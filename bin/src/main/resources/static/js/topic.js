const clusterName=sessionStorage.getItem("indexClusterName");

$(function () {
    to_topic_list_page(1);
});

function to_topic_list_page(pn){
    //console.log("clusterName="+clusterName);
    $.ajax({
        url:rootPath+"/topic/getAllTopics",
        data:"clusterName="+clusterName+"&pn="+pn+"&pattern="+$("#inpt_search_topic").val(),
        type:"GET",
        success(result){
            //console.log(result);
            if (successCode===result.code) {
                $("#topic_nums").empty().append(result.extend.kafkaResult.totalCount);
                build_topic_list_table(result);
                build_page_nav(result,"#page_nav_area");
                build_page_info(result,"#page_info_area");
            } else {
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,errorTitle,result.message);
            }
        }
    });
}

/**
 * 构建topic table
 * @param result
 */
function build_topic_list_table(result) {
    //清空table中tbody的内容
    $("#tbody_topic_list").empty();
    let topics=result.extend.kafkaResult.pageContent;
    $.each(topics,function (index,topic) {
        //console.log("topic="+topic);
        let checkBox=$("<td></td>").addClass("align-middle").append("<input class='check_item' type='checkbox'>").attr("box_name",topic);
        let topicName=$("<td></td>").addClass("align-middle").append(topic);
        let viewBtn=$("<button></button>").addClass("btn btn-outline-primary btn-sm btn_view").attr("view_name",topic).append("View");
        let editBtn=$("<button></button>").addClass("btn btn-outline-warning btn-sm btn_edit").attr("edit_name",topic).append("Edit");
        let dltBtn=$("<button></button>").addClass("btn btn-outline-danger btn-sm btn_dlt").attr("dlt_name",topic).append("Delete");
        let tdBtn=$("<td></td>").addClass("align-middle").append(viewBtn).append(" ").append(editBtn).append(" ").append(dltBtn);
        $("<tr></tr>").append(checkBox).append(topicName).append(tdBtn).appendTo("#tbody_topic_list");
    });
}

/**
 * 搜索按钮触发事件
 */
$(document).on("click","#btn_search_topic",function(){
    to_search($("#inpt_search_topic").val());
});

/**
 * 搜索框改变事件
 */
$(document).on("change","#inpt_search_topic",function () {
    to_search($(this).val());
});

/**
 * 像后台请求搜索事件
 */
function to_search(search_msg) {
    $.ajax({
        url: rootPath+"/topic/clickSearch",
        data: "clusterName="+clusterName+"&pattern="+search_msg,
        type: "GET",
        success(result) {
            //console.log(result);
            if (successCode===result.code) {
                build_topic_list_table(result);
                build_page_nav(result,"#page_nav_area");
                build_page_info(result,"#page_info_area");
            }else if (403===result.code) {
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,errorTitle,"Cannot match the input characters.");
            }else {
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,errorTitle,result.message);
            }
        }
    });
}

/**
 * 请求topic的详细信息，并跳转到页面
 */
$(document).on("click",".btn_view",function(){
    sessionStorage.setItem("view_name",$(this).attr("view_name"));
    window.location.replace(rootPath+"/topic/toTopicDescriptionPage");
});

/**
 * 点击table中删除按钮事件
 */
$(document).on("click",".btn_dlt",function () {
    staticModal("#modal_dlt");
    alert_message("#modal_dlt","#modal_dlt_title","#modal_dlt_msg", "",
        "Delete Alert","You will <span class=\"text-danger\">PERMANENTLY DELETE</span> ["+$(this).attr("dlt_name")+"] topic.<br/> " +
        "Please click <span class=\"text-danger\">[DELETE]</span> to delete the topic or. <br/>" +
        "Click <span class=\"text-secondary\">[Cancel]</span> to withdraw the operation.");
    $("#btn_dlt_tpc").attr("dlt_name",$(this).attr("dlt_name"));
});
/**
 * 点击取消删除按钮事件
 */
$(document).on("click","#btn_dlt_cnc",function () {
    $("#btn_dlt_tpc").attr("dlt_name","");
});
/**
 * 点击删除多个topic操作按钮事件
 */
$(document).on("click","#btn_dlt_tpc", function () {
    //alert($(this).attr("dlt_name"));
    $.ajax({
        url:rootPath+"/topic/deleteATopic",
        data:"topicName="+$(this).attr("dlt_name"),
        type:"DELETE",
        success(result){
            console.log(result);
            if(successCode===result.code){
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,successTitle,successMsg);
                to_search($("#inpt_search_topic").val());
            }else if(errorCode602===result.code){
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,errorTitle,result.message);
            }else {
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,errorTitle,result.message);
            }
        }
    });
});

/**
 * 跳转到添加一个topic的页面
 */
$(document).on("click","#btn_add_topic",function(){
    window.location.replace(rootPath+"/topic/toAddATopicPage");
});

/**
 * 跳转到添加多个topic的页面
 */
$(document).on("click","#btn_batch_add_topic",function(){
    window.location.replace(rootPath+"/topic/toAddMultipleTopicPage");
});

/**
 * 跳转到上一个页面
 */
$(document).on("click","#topic_list_page_back",function(){
    toClusterManagementPage();
});


/**
 * 点击删除多个topic的按钮事件
 */
let deleteTopicNames="";
$(document).on("click","#btn_dlt_checked_topic",function () {
    $.each($(".check_item:checked"), function () {
        deleteTopicNames+=$(this).parents("tr").find("td:eq(1)").text()+",";
    });
    deleteTopicNames=deleteTopicNames.substring(0,deleteTopicNames.length-1);
    //alert(deleteTopicNames);
    if(0===deleteTopicNames.length){
        alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content","",
            errorTitle,"Please choose some topics if you want to do <span class='text-danger'>DELETE</span> operation.");
        return false;
    }else {
        staticModal("#modal_dlt_mltp");
        alert_message("#modal_dlt_mltp","#modal_dlt_mltp_title","#modal_dlt_mltp_msg", "",
            "Delete Alert","You will <span class=\"text-danger\">PERMANENTLY DELETE</span> these topics.<br/> " +
            "Please click <span class='text-danger'>[DELETE]</span> to delete these topics or. <br/>" +
            "Click <span class='text-secondary'>[Cancel]</span> to withdraw the operation.");
        $("#btn_dlt_tpc").attr("dlt_multp",true);
    }
});

/**
 * 点击取消删除按钮事件
 */
$(document).on("click","#btn_dlt_mltp_cnc",function () {
    $("#btn_dlt_tpc").attr("dlt_multp",false);
    deleteTopicNames=""
});

/**
 * 点击删除多个topic操作按钮事件
 */
$(document).on("click","#btn_dlt_mltp_tpc", function () {
    if($("#btn_dlt_tpc").attr("dlt_multp")){
        $.ajax({
            url:rootPath+"/topic/deleteMultipleTopics",
            data:"deleteTopicNames="+deleteTopicNames,
            type:"DELETE",
            success(result){
                console.log(result);
                if(successCode===result.code){
                    alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,successTitle,successMsg);
                    to_search($("#inpt_search_topic").val());
                }else if(errorCode603===result.code){
                    alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,errorTitle,result.message);
                }else {
                    alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,errorTitle,result.message);
                }
            }
        });
    }
});

/**
 * 点击编辑按钮，转到Topic配置的信息
 */
$(document).on("click",".btn_edit",function () {
    sessionStorage.setItem("edit_name",$(this).attr("edit_name"));
    window.location.replace(rootPath+"/topic/toTopicEditPage");
});



