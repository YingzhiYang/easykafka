/**
 * 产生密码
 */
$(document).on("click","#btn_pwd_generate",function () {
    let nums=0, lowercase=0, capital=0, length;
    if($("#inpt_pwd_number").prop("checked")){
        nums=1;
    }
    if ($("#inpt_pwd_lowercase").prop("checked")){
        lowercase=1;
    }
    if($("#inpt_pwd_capital").prop("checked")){
        capital=1;
    }
    length=$("#slct_length option:selected").val();
    if(nums===0 && lowercase===0 && capital===0){
        alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",errorCode,errorTitle,
            "You should choose at least one pattern to generate password.");
        return;
    }
    $.ajax({
        url:rootPath+"/accountOperation/generatePassword",
        data:"nums="+nums+"&lowercase="+lowercase+"&capital="+capital+"&length="+length,
        type:"GET",
        success(result){
            console.log(result);
            if (successCode===result.code){
                $("#inpt_pwd_generator").val(result.extend.password);
                $("#inpt_account_pwd").val(result.extend.password);
            }else if(403===result.code){
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,errorTitle,
                    "You should choose at least one pattern to generate password.");
            }else{
                exception_alert("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result,exceptionTitle);
            }
        }
    });
});

/**
 * 创建账户
 */
$(document).on("click","#btn_account_add",function () {
    if(""===$("#inpt_account_name").val()){
        return false;
    }

    if(""===$("#inpt_account_pwd").val()){
        return false;
    }
    showAuthModal("#auth_modal","#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done");
    let formData={
        "account": $("#inpt_account_name").val(),
        "password": $("#inpt_account_pwd").val(),
        "clusterName": sessionStorage.getItem("indexClusterName")
    };
    $.ajax({
        url:rootPath+"/accountOperation/createAccount",
        data:formData,
        type:"POST",
        success(result) {
            console.log(result);
            if (successCode===result.code){
                changeAuthModalSuccess("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done");
            }else if(600===result.code){
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,errorTitle,result.message);
            }else {
                changeAuthModalFailed("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done",);
                //exception_alert("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result,exceptionTitle);
            }
        }
    });
});


/**
 * 删除账户提示
 */
$(document).on("click","#btn_account_delete",function () {
    if(""===$("#inpt_account_name").val()){
        return false;
    }
    warning_message("#alert_modal_warning","#alert_title_warning","#alert_msg_warning","#alert_model_content_warning",warningTitle,"You will DELETE an account <strong>["+$("#inpt_account_name").val()+"]</strong>, and will REMOVE all related authorizations !");
});

/**
 * 确定删除账户
 */
$(document).on("click","#btn_account_delete_confirmed",function () {
    if(""===$("#inpt_account_name").val()){
        return false;
    }
    showAuthModal("#auth_modal","#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done");
    let formData={
        "account": $("#inpt_account_name").val(),
        "clusterName": sessionStorage.getItem("indexClusterName").toString(),
        "password": "",
    };
    $.ajax({
        url:rootPath+"/accountOperation/deleteAccount",
        type:"DELETE",
        data:formData,
        success(result){
            if(successCode===result.code){
                changeAuthModalSuccess("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done");
            }else if(609===result.code){
                changeAuthModalFailed("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done",result.message);
            }else {
                changeAuthModalFailed("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done",result.message);
            }
        }
    });
});


