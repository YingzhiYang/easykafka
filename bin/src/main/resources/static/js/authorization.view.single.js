$(document).on("click","#btn_submit_single_acl",function () {
    to_single_acl_search($("#inpt_search_single_acl").val());
});


$(document).on("change","#inpt_search_single_acl",function () {
    to_single_acl_search($(this).val());
});


function to_single_acl_search(account) {
    $.ajax({
        url:rootPath+"/aclDescription/getSingleAcl",
        data:"account="+account,
        type:"GET",
        success(result){
            console.log(result);
            if (successCode===result.code){
                $("#auth_single_nums").empty().append(result.extend.kafkaResult.totalCount);
                build_auth_single_table(result);
                build_page_nav_single_acl(result,"#page_nav_single_area");
                build_page_info(result,"#page_info_single_area");
            }else if(202===result.code){
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,errorTitle,result.message);
            }else {
                exception_alert("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result,exceptionTitle);
            }
        }
    });
}

/**
 * 获取单一账户的的ACL表格数据
 */
function to_auth_single_list_page(pn) {
    $.ajax({
        url:rootPath+"/aclDescription/getSingleAclForPageNum",
        type:"GET",
        data: "pageNum="+pn,
        success(result){
            console.log(result);
            if (successCode===result.code){
                $("#auth_single_nums").empty().append(result.extend.kafkaResult.totalCount);
                build_auth_single_table(result);
                build_page_nav_single_acl(result,"#page_nav_single_area");
                build_page_info(result,"#page_info_single_area");
            }else if(202===result.code){
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,errorTitle,result.message);
            }else{
                exception_alert("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result,exceptionTitle);
            }
        }
    });
}

/**
 * 构建单个账户的acl的表格
 * @param result
 */
function build_auth_single_table(result) {
    $("#thead_single_auth_info").empty();
    $("#tbody_single_auth_info").empty();
    let authorizations=result.extend.kafkaResult.pageContent;
    let th_principal=$("<th></th>").attr("scope","col").append("Account Name");
    let th_operation=$("<th></th>").attr("scope","col").append("Authorization");
    let th_name=$("<th></th>").attr("scope","col").append("Object Name");
    let th_permission=$("<th></th>").attr("scope","col").append("Permission");
    let th_resourceType=$("<th></th>").attr("scope","col").append("Auth Object");
    let th_patternType=$("<th></th>").attr("scope","col").append("Auth Type");
    $("<tr></tr>").append(th_principal).append(th_operation).append(th_name).append(th_permission).append(th_resourceType).append(th_patternType).appendTo("#thead_single_auth_info");
    $.each(authorizations,function (index,authorization) {
        let principal=$("<td></td>").addClass("align-middle").append(authorization.principal);
        let operation=$("<td></td>").addClass("align-middle").append(authorization.operation);
        let name=$("<td></td>").addClass("align-middle").append(authorization.name);
        let permission=$("<td></td>").addClass("align-middle").append(authorization.permission);
        let resourceType=$("<td></td>").addClass("align-middle").append(authorization.resourceType);
        let patternType=$("<td></td>").addClass("align-middle").append(authorization.patternType);
        $("<tr></tr>").append(principal).append(operation).append(name).append(resourceType).append(permission).append(patternType).appendTo("#tbody_single_auth_info");
    });
}





/**
 * 创建单一的acl导航条
 * @param result
 * @param ele
 */
function build_page_nav_single_acl(result,ele) {
    $(ele).empty();
    let ul = $("<ul></ul>").addClass("pagination pagination-sm");
    let first = $("<li></li>").addClass("page-item").append(
        $("<a></a>").addClass("page-link").attr("href", "#")
            .append("First"));
    let previous = $("<li></li>").addClass("page-item").append(
        $("<a></a>").addClass("page-link").attr("href", "#").append(
            "&laquo;"));
    if (result.extend.kafkaResult.hasPreviousPage) {
        ul.append(first).append(previous);
        first.click(function () {
            console.log("go to first page");
            to_auth_single_list_page(result.extend.kafkaResult.firstPage);
        });

        previous.click(function () {
            if (result.extend.kafkaResult.currentPage - 1 <= 0) {
                to_auth_single_list_page(result.extend.kafkaResult.firstPage);
            } else {
                to_auth_single_list_page(result.extend.kafkaResult.currentPage - 1);
            }
        });
    } else {
        ul.append(first.addClass("disabled"));
    }

    $.each(result.extend.kafkaResult.currentNavigationPages,
        function (index, item) {
            let li = $("<li></li>").addClass("page-item").append(
                $("<a></a>").addClass("page-link").attr("href", "#")
                    .append(item));
            if (result.extend.kafkaResult.currentPage === item) {
                li.addClass("active");
            } else {
                li.click(function () {
                    to_auth_single_list_page(item);
                });
            }
            ul.append(li);
        });
    let next = $("<li></li>").addClass("page-item").append(
        $("<a></a>").addClass("page-link").attr("href", "#").append(
            "&raquo;"));
    let last = $("<li></li>").addClass("page-item")
        .append(
            $("<a></a>").addClass("page-link").attr("href", "#")
                .append("Last"));
    if (result.extend.kafkaResult.hasNextPage) {
        ul.append(next).append(last);
        last.click(function () {
            to_auth_single_list_page(result.extend.kafkaResult.lastPage);
        });
        next.click(function () {
            if (result.extend.kafkaResult.currentPage + 1 >= result.extend.kafkaResult.lastPage) {
                to_auth_single_list_page(result.extend.kafkaResult.lastPage);
            } else {
                to_auth_single_list_page(result.extend.kafkaResult.currentPage + 1);
            }
        });
    } else {
        ul.append(last.addClass("disabled"));
    }
    let nav = $("<nav></nav>").append(ul);
    nav.appendTo(ele);
}


$(document).on("click","#auth_single_page_back",function () {
    window.location.replace(rootPath+"/cluster/toAuthViewPage");
});