$(document).on("click","#btn_topic_add_cancel",function(){
    window.location.replace(rootPath+"/cluster/toTopicPage");
});

/**
 * 添加一个topic按钮事件
 */
$(document).on("click","#btn_topic_add",function () {
    let formData=$("#form_topic_add").serialize();
    //console.log(formData);
    $.ajax({
        url:rootPath+"/topic/addATopic",
        type:"POST",
        data:formData,
        success(result){
            //console.log(result);
            if(successCode===result.code){
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,successTitle,"Topic is created.");
            }else {
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,errorTitle,result.message);
            }
        }
    });
});

/**
 * 添加多个topic按钮事件
 */
$(document).on("click","#btn_multiple_topic_add",function () {
    let formData=$("#form_topic_multiple_add").serialize();
    //console.log(formData);
    $.ajax({
        url: rootPath+"/topic/addMultipleTopics",
        data: formData,
        type: "POST",
        success(result) {
            console.log(result);
            if(successCode===result.code){
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,successTitle,"Topics are created.");
            }else if(errorCode601===result.code){
                let text = "The following topics are exists: ";
                for (let i = 0; i < result.extend.existsTopics.length; i++) {
                    text += " ["+result.extend.existsTopics[i] + "] ";
                }
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,errorTitle,text+" Please double check and retype topics.");
            }else {
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,errorTitle,result.message);
            }
        }
    });
});


/**
 * 跳转到上一个页面
 */
$(document).on("click","#topic_mtp_add_back",function(){
    window.location.replace(rootPath+"/cluster/toTopicPage");
});

/**
 * 跳转到上一个页面
 */
$(document).on("click","#topic_add_back",function(){
    window.location.replace(rootPath+"/cluster/toTopicPage");
});