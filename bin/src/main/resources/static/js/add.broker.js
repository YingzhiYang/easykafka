$(document).on("click","#btn_add_cluster",function () {
    //alert("this is a function "+ $("#zk_ips").val()+"  "+rootPath);
    $.ajax({
        url:rootPath+"/clusters/addCluster",
        type:"POST",
        data:{
            zkIps:$("#zk_ips").val(),
            kfIps:$("#kaf_ips").val(),
            clusterName:$("#inpt_cluster_name").val(),
            supName:$("#inpt_supper_user").val(),
            supPwd:$("#inpt_supper_pwd").val(),
            securityProtocol:$("#inpt_security_protocol").val(),
            saslMechanism:$("#inpt_sasl_mechanism").val(),
            sslPwd:$("#inpt_ssl_pwd").val(),
            sslLocation:$("#inpt_jks_location").val(),
            zkNodePath:$("#inpt_zk_node_path").val(),
            zkConfigPath:$("#inpt_zk_config_path").val()
        },
        dataType: "json",
        success : function(result) {
            //console.log(result);
            if (successCode===result.code) {
                returnToHomePage();
            } else {
                alert(result.message);
            }
        }
    });
});

function returnToHomePage() {
    window.location.replace(rootPath+"/index");
}