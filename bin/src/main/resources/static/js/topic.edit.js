$(function () {
    $("#topic_name").val("");
    $("#topic_name").val(sessionStorage.getItem("edit_name"));
    getTopicPartitions();
    getTopicConfigurations();
});

/**
 * 拿到指定topic的partition数量
 */
function getTopicPartitions(){
    $.ajax({
        url:rootPath+"/partition/getTopicPartitions",
        type:"GET",
        data:"topicName="+$("#topic_name").val(),
        success(result){
            console.log(result);
            if(successCode===result.code){
                $("#topic_partitions").val("");
                $("#topic_partitions").val(result.extend.topicPartitions);
            }else {
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,errorTitle,result.message);
            }
        }
    });
}

/**
 * 获取Topic的配置
 */
function getTopicConfigurations(){
    $.ajax({
        url:rootPath+"/topicConfiguration/getTopicConfigurations",
        type:"GET",
        data:"topicName="+$("#topic_name").val(),
        success(result){
            //console.log(result);
            if(successCode===result.code){
                setTopicConfiguration(result.extend.topicConfig);
            }else {
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,errorTitle,result.message);
            }
        }
    });
}

/**
 * 创建
 * @param topicConfig
 */
function setTopicConfiguration(topicConfig){
    //第一行赋值
    let ipt_retentionMs=$("#ipt_retentionMs");
    ipt_retentionMs.val("");
    ipt_retentionMs.val(topicConfig.retentionMs);

    let ipt_maxMessageBytes=$("#ipt_maxMessageBytes");
    ipt_maxMessageBytes.val("");
    ipt_maxMessageBytes.val(topicConfig.maxMessageBytes);

    let ipt_segmentIndexBytes=$("#ipt_segmentIndexBytes");
    ipt_segmentIndexBytes.val("");
    ipt_segmentIndexBytes.val(topicConfig.segmentIndexBytes);

    //第二行赋值
    let ipt_segmentBytes=$("#ipt_segmentBytes");
    ipt_segmentBytes.val("");
    ipt_segmentBytes.val(topicConfig.segmentBytes);

    let ipt_minCleanableDirtyRatio=$("#ipt_minCleanableDirtyRatio");
    ipt_minCleanableDirtyRatio.val("");
    ipt_minCleanableDirtyRatio.val(topicConfig.minCleanableDirtyRatio);

    let ipt_minInsyncReplicas=$("#ipt_minInsyncReplicas");
    ipt_minInsyncReplicas.val("");
    ipt_minInsyncReplicas.val(topicConfig.minInsyncReplicas);

    //第三行赋值
    let ipt_deleteRetentionMs=$("#ipt_deleteRetentionMs");
    ipt_deleteRetentionMs.val("");
    ipt_deleteRetentionMs.val(topicConfig.deleteRetentionMs);

    let ipt_flushMessages=$("#ipt_flushMessages");
    ipt_flushMessages.val("");
    ipt_flushMessages.val(topicConfig.flushMessages);

    let ipt_preallocate=$("#ipt_preallocate");
    ipt_preallocate.val("");
    ipt_preallocate.val(topicConfig.preallocate);

    //第四行赋值
    let ipt_retentionBytes=$("#ipt_retentionBytes");
    ipt_retentionBytes.val("");
    ipt_retentionBytes.val(topicConfig.retentionBytes);

    let ipt_flushMs=$("#ipt_flushMs");
    ipt_flushMs.val("");
    ipt_flushMs.val(topicConfig.flushMs);

    let ipt_cleanupPolicy=$("#ipt_cleanupPolicy");
    ipt_cleanupPolicy.val("");
    ipt_cleanupPolicy.val(topicConfig.cleanupPolicy);

    //第五行赋值
    let ipt_fileDeleteDelayMs=$("#ipt_fileDeleteDelayMs");
    ipt_fileDeleteDelayMs.val("");
    ipt_fileDeleteDelayMs.val(topicConfig.fileDeleteDelayMs);

    let ipt_segmentJitterMs=$("#ipt_segmentJitterMs");
    ipt_segmentJitterMs.val("");
    ipt_segmentJitterMs.val(topicConfig.segmentJitterMs);

    let ipt_indexIntervalBytes=$("#ipt_indexIntervalBytes");
    ipt_indexIntervalBytes.val("");
    ipt_indexIntervalBytes.val(topicConfig.indexIntervalBytes);

    //第六行赋值
    let ipt_compressionType=$("#ipt_compressionType");
    ipt_compressionType.val("");
    ipt_compressionType.val(topicConfig.compressionType);

    let ipt_segmentMs=$("#ipt_segmentMs");
    ipt_segmentMs.val("");
    ipt_segmentMs.val(topicConfig.segmentMs);

    let ipt_uncleanLeaderElectionEnable=$("#ipt_uncleanLeaderElectionEnable");
    ipt_uncleanLeaderElectionEnable.val("");
    ipt_uncleanLeaderElectionEnable.val(topicConfig.uncleanLeaderElectionEnable);
}


/**
 * 增加partition的按钮
 * @returns
 */
$(document).on("click","#btn_change_partition",function(){
	$.ajax({
		url:rootPath+"/partition/increasePartitions",
		type:"POST",
        data:"topicName="+$("#topic_name").val()+"&partitions="+$("#topic_partitions").val(),
        success(result){
            //console.log(result);
            if(successCode===result.code){
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,successTitle,result.message);
            }else {
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,errorTitle,result.message);
            }
        }
	});
});

/**
 * 修改topic配置参数的按钮
 */
$(document).on("click","#btn_topic_edit",function(){
    let data_edit=$("#form_topic_conf_edit").serialize();
    //console.log(data_edit);
    $.ajax({
        url:rootPath+"/topicConfiguration/setTopicConfigurations",
        type:"POST",
        data:data_edit+"&topicName="+$("#topic_name").val(),
        success(result){
            //console.log(result);
            if(successCode===result.code){
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,successTitle,result.message);
            }else {
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,errorTitle,result.message);
            }
        }
    });
});



/**
 * cancel button跳转到上一个页面
 */
$(document).on("click","#btn_topic_edit_cancel",function(){
    window.location.replace(rootPath+"/cluster/toTopicPage");
});



/**
 * back button跳转到上一个页面
 */
$(document).on("click","#topic_edit_back",function(){
    window.location.replace(rootPath+"/cluster/toTopicPage");
});

/**
 * cancel button 跳转到上一个页面
 */
$(document).on("click","#btn_topic_edit_cancel",function(){
    window.location.replace(rootPath+"/cluster/toTopicPage");
});