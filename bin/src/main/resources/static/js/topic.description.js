$(function () {
    let topicName=sessionStorage.getItem("view_name");
    $("#topic_description_name").append(topicName);
    getTopicInfo(topicName);
    getTopicAuthorization(topicName);
});

/**
 * 根据topicName查询topic运行时信息
 * @param topicName
 */
function getTopicInfo(topicName) {
    if(!checkTopicName(topicName)){
        return false;
    }
    $.ajax({
        url:rootPath+"/topicDescription/getTopicDescription",
        type:"GET",
        data:"topicName="+topicName,
        success(result){
            console.log(result);
            if (successCode===result.code){
                $("#partition_nums").append(result.extend.topicPartitions.length);
                partition_table(result);
            }else if(414===result.code){
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,errorTitle,result.message);
            }else {
                exception_alert("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result,exceptionTitle);
            }
        }
    });
}

/**
 * 根据topic名字查询topic权限信息
 * @param topicName
 * @returns {boolean}
 */
function getTopicAuthorization(topicName) {
    if(!checkTopicName(topicName)){
        return false;
    }
    $.ajax({
        url: rootPath+"/topicDescription/getTopicAuthorizationDescription",
        type: "GET",
        data: "topicName="+topicName,
        success(result) {
            console.log(result);
            if (successCode===result.code){
                $("#authorization_nums").append(result.extend.topicAuthorization.length);
                authorization_table(result);
            }else if(414===result.code){
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,errorTitle,result.message);
            }else {
                exception_alert("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result,exceptionTitle);
            }
        }
    });

}

/**
 * 检测topic是否为null
 * @param topicName
 * @returns {boolean}
 */
function checkTopicName(topicName) {
    if (null===topicName||""===topicName){
        alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content","",
            errorTitle,"Cannot detect valid <span class='text-danger'>TOPIC NAME</span>, please turn back and re-try.");
        return false;
    }
    return true;
}


/**
 * 根据result创建partition表
 * @param result
 */
function partition_table(result) {
    $("#tbody_partition_info").empty();
    $.each(result.extend.topicPartitions,function (index,partition) {
        let partitionId=$("<td></td>").addClass("align-middle").append(partition.partitionId);
        let leaderId=$("<td></td>").addClass("align-middle").append(partition.leader.leaderId);
        let leaderHost=$("<td></td>").addClass("align-middle").append(partition.leader.leaderHost+":"+partition.leader.leaderPort);
        let replicasNum=$("<td></td>").addClass("align-middle").append(partition.leader.replicasNum);
        $("<tr></tr>").append(partitionId).append(leaderId).append(leaderHost).append(replicasNum).appendTo("#tbody_partition_info");
    });
}


/**
 * 根据result创建authorization表
 * @param result
 */
function authorization_table(result) {
    $("#tbody_topic_authorization_info").empty();
    $.each(result.extend.topicAuthorization,function (index,authorization) {
        let principal=$("<td></td>").addClass("align-middle").append(authorization.principal);
        let operation=$("<td></td>").addClass("align-middle").append(authorization.operation);
        let permission=$("<td></td>").addClass("align-middle").append(authorization.permission);
        let resourceType=$("<td></td>").addClass("align-middle").append(authorization.resourceType);
        let patternType=$("<td></td>").addClass("align-middle").append(authorization.patternType);
        $("<tr></tr>").append(principal).append(operation).append(permission).append(resourceType).append(patternType).appendTo("#tbody_topic_authorization_info");
    });
}

/**
 * 返回TopicListPage
 */
$(document).on("click","#topic_description_page_back",function () {
    window.location.replace(rootPath+"/cluster/toTopicPage");
});












