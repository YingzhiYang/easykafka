$(function () {

    welcomeTitle();
});

function welcomeTitle() {
    $("#h1_cluster_title").append("Cluster Name: "+sessionStorage.getItem("indexClusterName"));
}

$(document).on("click","#btn_topic_view", function () {
    window.location.replace(rootPath+"/cluster/toTopicPage");
});

$(document).on("click","#btn_to_auth_account_page", function () {
    window.location.replace(rootPath+"/cluster/toAuthAccountPage");
});

$(document).on("click","#btn_all_auth_view", function () {
    window.location.replace(rootPath+"/cluster/toAuthViewPage");
});


$(document).on("click","#btn_zk_view", function () {
    window.location.replace(rootPath+"/cluster/toZookeeperViewPage")
});
