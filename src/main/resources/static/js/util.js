let rootPath = getRootPath();
let currentpage, totalRecords;
let successTitle = "Success";
let errorTitle = "Oops......";
let warningTitle = "Warning";
let successMsg = "Operation is Successful.";
let exceptionTitle = "Exception Happened";
let browserName = navigator.appName;

const successCode = 200;
const errorCode = 400;
const errorCode601 = 601;
const errorCode602 = 602;
const errorCode603 = 603;



function getRootPath() {
    // 获取当前网址，如： http://localhost:8088/test/test.jsp
    let curPath = window.document.location.href;
    // 获取主机地址之后的目录，如： test/test.jsp
    let pathName = window.document.location.pathname;
    let pos = curPath.indexOf(pathName);
    // 获取主机地址，如： http://localhost:8088
    return curPath.substring(0, pos);// http://localhost:8088
}

function getRootProjectPath() {
    // 获取当前网址，如： http://localhost:8088/test/test.jsp
    let curPath = window.document.location.href;
    // 获取主机地址之后的目录，如： test/test.jsp
    let pathName = window.document.location.pathname;
    let pos = curPath.indexOf(pathName);
    // 获取主机地址，如： http://localhost:8088
    let localhostPath = curPath.substring(0, pos);
    // 获取带"/"的项目名，如：/test
    let projectName = pathName
        .substring(0, pathName.substr(1).indexOf('/') + 1);
    return (localhostPath + projectName);// http://localhost:8088/test
}

/**
 * 创建导航信息
 * @param result
 * @param ele
 */
function build_page_info(result,ele) {
    $(ele).empty().append(
        "Current page : " + result.extend.kafkaResult.currentPage + "; Total pages : "
        + result.extend.kafkaResult.pagesCount + "; Total records : "
        + result.extend.kafkaResult.totalCount + ".<br>");
    totalRecords = result.extend.kafkaResult.totalCount;
    currentpage = result.extend.kafkaResult.currentPage;
}

/**
 * 创建导航条
 * @param result
 * @param ele
 */
function build_page_nav(result,ele) {
    $(ele).empty();
    let ul = $("<ul></ul>").addClass("pagination pagination-sm");
    let first = $("<li></li>").addClass("page-item").append(
        $("<a></a>").addClass("page-link").attr("href", "#")
            .append("First"));
    let previous = $("<li></li>").addClass("page-item").append(
        $("<a></a>").addClass("page-link").attr("href", "#").append(
            "&laquo;"));
    if (result.extend.kafkaResult.hasPreviousPage) {
        ul.append(first).append(previous);
        first.click(function () {
            console.log("go to first page");
            to_topic_list_page(result.extend.kafkaResult.firstPage);
        });

        previous.click(function () {
            if (result.extend.kafkaResult.currentPage - 1 <= 0) {
                to_topic_list_page(result.extend.kafkaResult.firstPage);
            } else {
                to_topic_list_page(result.extend.kafkaResult.currentPage - 1);
            }
        });
    } else {
        ul.append(first.addClass("disabled"));
    }

    $.each(result.extend.kafkaResult.currentNavigationPages,
        function (index, item) {
            let li = $("<li></li>").addClass("page-item").append(
                $("<a></a>").addClass("page-link").attr("href", "#")
                    .append(item));
            if (result.extend.kafkaResult.currentPage === item) {
                li.addClass("active");
            } else {
                li.click(function () {
                    to_topic_list_page(item);
                });
            }
            ul.append(li);
        });
    let next = $("<li></li>").addClass("page-item").append(
        $("<a></a>").addClass("page-link").attr("href", "#").append(
            "&raquo;"));
    let last = $("<li></li>").addClass("page-item")
        .append(
            $("<a></a>").addClass("page-link").attr("href", "#")
                .append("Last"));
    if (result.extend.kafkaResult.hasNextPage) {
        ul.append(next).append(last);
        last.click(function () {
            to_topic_list_page(result.extend.kafkaResult.lastPage);
        });
        next.click(function () {
            if (result.extend.kafkaResult.currentPage + 1 >= result.extend.kafkaResult.lastPage) {
                to_topic_list_page(result.extend.kafkaResult.lastPage);
            } else {
                to_topic_list_page(result.extend.kafkaResult.currentPage + 1);
            }
        });
    } else {
        ul.append(last.addClass("disabled"));
    }
    let nav = $("<nav></nav>").append(ul);
    nav.appendTo(ele);
}


/**
 * modal框固定
 *
 * @param ele
 * @returns
 */
function staticModal(ele) {
    $(ele).modal({
        backdrop: 'static'
    });
}

/**
 * modal显示
 *
 * @param ele
 * @returns
 */
function showModal(ele) {
    $(ele).modal('show');
}


/**
 * 警告框内容填充，需要针对这一块的提示框进行修改
 * @param ele_modal
 * @param ele_title
 * @param ele_msg
 * @param ele_content
 * @param code
 * @param title
 * @param msg
 * @returns
 */
function alert_message(ele_modal, ele_title, ele_msg, ele_content, code, title, msg) {
    if (successCode === code) {
        $(ele_content).removeClass("alert-danger");
        $(ele_title).removeClass("text-danger");
        $(ele_content).addClass("alert-success");
        $("#alert_pic").attr("src", "../img/success.svg");
        $(ele_title).addClass("text-success");
    } else {
        $(ele_content).removeClass("alert-success");
        $(ele_title).removeClass("text-success");
        $(ele_content).addClass("alert-danger");
        $(ele_title).addClass("text-danger");
        $("#alert_pic").attr("src", "../img/delete.svg");
    }
    $(ele_title).empty();
    $(ele_title).append(title);
    $(ele_msg).empty();
    $(ele_msg).append(msg);
    $(ele_modal).modal('show');
}

/**
 * 异常框内容填充
 * @param ele_modal
 * @param ele_title
 * @param ele_msg
 * @param ele_content
 * @param result
 * @param exceptionTitle
 * @returns
 */
function exception_alert(ele_modal, ele_title, ele_msg,ele_content, result, exceptionTitle) {
    let cause = result.extend.exception.cause;
    let clazzName = result.extend.exception.clazzName;
    let methodName = result.extend.exception.methodName;
    let lineNum = result.extend.exception.lineNum;
    let exception_message = "Cause: " + cause + "<br>" + "Class: " + clazzName + "<br>" + "Method: " + methodName + "<br>" + "Line: " + lineNum;
    $(ele_content).removeClass("alert-success");
    $(ele_title).removeClass("text-success");
    $(ele_content).addClass("alert-danger");
    $(ele_title).addClass("text-danger");
    $("#alert_pic").attr("src", "../img/delete.svg");
    $(ele_title).empty();
    $(ele_title).append(exceptionTitle);
    $(ele_msg).empty();
    $(ele_msg).append(exception_message);
    $(ele_modal).modal('show');
}

function warning_message(ele_modal, ele_title, ele_msg,ele_content, title, msg) {
    $("#alert_pic_warning").attr("src", "../img/delete.svg");
    $(ele_content).removeClass("alert-success");
    $(ele_title).removeClass("text-success");
    $(ele_content).addClass("alert-danger");
    $(ele_title).addClass("text-danger");
    $(ele_title).empty();
    $(ele_title).append(title);
    $(ele_msg).empty();
    $(ele_msg).append(msg);
    $(ele_modal).modal('show');
}



/**
 * 重置表单
 *
 * @param element
 * @returns
 */
function reset_form(element) {
    $(element)[0].reset();
    $(element).find("*").removeClass("is-valid is-invalid");
    $(element).find(".help-block").text("");
}

/**
 * 重置元素
 *
 * @param element
 * @returns
 */
function reset_element(element) {
    $(element).removeClass("is-valid is-invalid");
    $(element).find(".help-block").text("");
}

/**
 * 关闭当前页面
 * @returns
 */
function close_window() {
    if (browserName === "Netscape") {
        window.location.href = "about:blank";
        window.close();
    } else if (browserName === "Microsoft Internet Explorer") {
        window.opener = null;
        window.close();
    }
}


/**
 * 延时一秒执行，并显示在控件上
 * @param ele
 * @param time
 * @returns
 */
function clickDownAndShow(ele, time) {
    time--;
    $(ele).text(time);
    if (time === 0) {
        close_window();
    } else {
        setTimeout(function () {
            clickDownAndShow(ele, time);
        }, 1000);
    }
}

/**
 * 控制全选的checkbox
 * @returns
 */
$(document).on("click", "#checkbox_all", function () {
    $(".check_item").prop("checked", $(this).prop("checked"));
});

/**
 * 控制单条小的checkbox，并影响全选的checkbox
 * @returns
 */
$(document).on("click", ".check_item", function () {
    let flag = $(".check_item:checked").length === $(".check_item").length;
    $("#checkbox_all").prop("checked", flag);
});

/**
 * 跳转到单一授权界面
 */
function to_single_authorization_page() {
    window.location.replace(rootPath+"/aclDescription/toSingleAuthorizationPage");
}

/**
 * 跳转到Cluster管理界面
 */
function toClusterManagementPage() {
    window.location.replace(rootPath+"/cluster/toClusterManagementPage");
}


/**
 * 授权开始初次提示
 * @param ele_modal
 * @param ele_pic
 * @param ele_title
 * @param ele_spinner
 * @param ele_text
 * @param ele_btn_done
 */
function showAuthModal(ele_modal,ele_pic,ele_title,ele_spinner,ele_text,ele_btn_done) {
    $(ele_pic).attr("src","../img/yellow.svg");
    $(ele_title).empty().removeClass("text-success").removeClass("text-danger").addClass("text-warning").append("The Authorizations are processing......");
    $(ele_spinner).addClass("text-warning");
    $(ele_text).empty().append("Please do NOT close this window. <br/> And wait a minute until the [Done] is <span class=\"text-success\">GREEN</span>.");
    $(ele_btn_done).removeClass("btn-success").removeClass("btn-danger").addClass("btn-warning").attr("disabled");
    staticModal(ele_modal);
    showModal(ele_modal);
}

/**
 * 授权完成成功提示
 * @param ele_pic
 * @param ele_title
 * @param ele_spinner
 * @param ele_text
 * @param ele_btn_done
 */
function changeAuthModalSuccess(ele_pic,ele_title,ele_spinner,ele_text,ele_btn_done) {
    $(ele_pic).attr("src","../img/success.svg");
    $(ele_title).empty().removeClass("text-danger").removeClass("text-warning").addClass("text-success").append("Operation Successful");
    $(ele_spinner).removeClass("text-warning").removeClass("spinner-border");
    $(ele_text).empty().append("All authorizations have been processed.");
    $(ele_btn_done).removeClass("btn-warning").removeClass("btn-danger").addClass("btn-success").removeAttr("disabled");
}

/**
 * 授权失败提示
 * @param ele_pic
 * @param ele_title
 * @param ele_spinner
 * @param ele_text
 * @param ele_btn_done
 * @param err_msg
 */
function changeAuthModalFailed(ele_pic,ele_title,ele_spinner,ele_text,ele_btn_done,err_msg) {
    $(ele_pic).attr("src","../img/delete.svg");
    $(ele_title).empty().removeClass("text-success").removeClass("text-warning").addClass("text-danger").append("Operation Failed");
    $(ele_spinner).removeClass("text-warning").removeClass("spinner-border");
    $(ele_text).empty().append(err_msg);
    $(ele_btn_done).removeClass("btn-success").removeClass("btn-warning").addClass("btn-danger").removeAttr("disabled");
}

/**
 * 文档加载完毕后调用自动生成alert标签
 */
$(document).ready(function () {
    autoFillAlertModal();
});

/**
 * 自动生成全局控件到div global_alert_modal里面
 */
function autoFillAlertModal(){
    //alert内容
    let alertModal=document.getElementById("global_alert_modal");
    alertModal.innerHTML="<div class=\"modal fade\" id=\"alert_modal\" tabindex=\"-1\" role=\"dialog\"\n" +
        "         aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">\n" +
        "        <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n" +
        "            <div class=\"modal-content\">\n" +
        "                <div class=\"modal-header\">\n" +
        "                    <img src=\"\" width=\"30\" height=\"30\" id=\"alert_pic\"\n" +
        "                         class=\"d-inline-block align-top\" alt=\"\">&nbsp;\n" +
        "                    <h5 class=\"modal-title\" id=\"alert_title\"></h5>\n" +
        "                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\"\n" +
        "                            aria-label=\"Close\">\n" +
        "                        <span aria-hidden=\"true\">&times;</span>\n" +
        "                    </button>\n" +
        "                </div>\n" +
        "                <div class=\"modal-body\">\n" +
        "                    <div class=\"alert alert-danger\" id=\"alert_model_content\"><p id=\"alert_msg\"></p></div>\n" +
        "                </div>\n" +
        "                <div class=\"modal-footer\">\n" +
        "                    <button type=\"button\" class=\"btn btn-secondary\"\n" +
        "                            data-dismiss=\"modal\">Close</button>\n" +
        "                </div>\n" +
        "            </div>\n" +
        "        </div>\n" +
        "    </div>";
}


