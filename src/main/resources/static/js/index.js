$(function() {
    findAllCluster();
});

function findAllCluster() {
    //console.log("findAllCluster");
    $.ajax({
        url:rootPath+"/clusters/findAllClusterName",
        type:"GET",
        success(result){
            //console.log(result);
            if(successCode===result.code){
                if (!(0===result.extend.queryResult.length)){
                    generateClusterBlock(result.extend.queryResult);
                }
            }else {
                alert(result.message);
            }
        }
    });
}

function generateClusterBlock(clusters) {
    //console.log(clusters);
    $("#ul_cluster_list").find("li").remove();
    $.each(clusters,function(index, cluster){
        let li = $("<li></li>").addClass("list-group-item").css({
            "overflow": "hidden",
            "text-overflow": "ellipsis",
            "white-space": "nowrap"
        });
        let a = $("<a></a>").attr("href", "#").addClass("a_cluster_name").append(cluster.clusterName);
        li.append(a).appendTo("#ul_cluster_list");
    });
}

$(document).on("click",".a_cluster_name",function () {
    toBroker($(this).text());
});

function toBroker(clusterName) {
    sessionStorage.setItem("indexClusterName",clusterName);
    $.ajax({
        url: rootPath+"/cluster/initClusterConnect",
        data: "clusterName="+clusterName,
        type: "GET",
        success(result) {
            //console.log(result);
            if(successCode===result.code){
                toClusterManagementPage();
            }else {
                alert("所选Kafka集群连接失败，请联系管理员Elvis Wu、yangyz9处理。");
            }
        }
    });
}