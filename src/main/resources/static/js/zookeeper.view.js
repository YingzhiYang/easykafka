const clusterName=sessionStorage.getItem("indexClusterName");

$(function () {
    getZookeeperListInfo();
});


function getZookeeperListInfo(){
    $.ajax({
        url:rootPath+"/zookeeperOp/getZookeeperListInfo",
        data:"clusterName="+clusterName,
        type:"GET",
        success(result){
            //console.log(result);
            if(successCode===result.code){
                build_zookeeper_list_table(result.extend.zookeeper);
            }else {
                alert_message("#alert_modal","#alert_title","#alert_msg",result.code,errorTitle,result.message);
            }
        }
    });
}


function build_zookeeper_list_table(zookeepers){
    $("#topic_nums").empty().append(zookeepers.length);
    $("#tbody_zookeeper_list").empty();
    $.each(zookeepers,function (index, zookeeper) {
        let ip=$("<td></td>").addClass("align-middle").append(zookeeper.ip);
        let port=$("<td></td>").addClass("align-middle").append(zookeeper.port);
        let role=$("<td></td>").addClass("align-middle").append(zookeeper.role);
        let status;
        if(1===zookeeper.status){
            status=$("<td></td>").addClass("align-middle").addClass("text-success").addClass("font-weight-bold").append("Normal");
        }else {
            status=$("<td></td>").addClass("align-middle").addClass("text-danger").addClass("font-weight-bold").append("Disconnected");
        }
        let btn_zk=$("<button></button>").addClass("align-middle").addClass("btn btn-outline-primary btn-sm btn_zk_op").attr("zkIp",zookeeper.ip).attr("zkPort",zookeeper.port).append("Ping");
        let operation=$("<td></td>").addClass("align-middle").append(" ").append(btn_zk).append(" ");
        $("<tr></tr>").append(ip).append(port).append(role).append(status).append(operation).appendTo("#tbody_zookeeper_list");
    });
}


$(document).on("click","#zookeeper_view_page_back", function () {
    toClusterManagementPage();
});

$(document).on("click",".btn_zk_op",function () {
    $.ajax({
        url:rootPath+"/zookeeperOp/getZookeeperPing",
        data:"ip="+$(this).attr("zkIp")+"&port="+$(this).attr("zkPort"),
        type: "GET",
        success(result){
            //console.log(result);
            if(successCode===result.code){
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,successTitle,"Zookeeper is normal.");
            }else {
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,errorTitle,result.message);
            }
        }
    });
});























