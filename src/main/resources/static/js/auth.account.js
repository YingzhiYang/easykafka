$(document).on("click", "#topic_auth_back", function () {
    toClusterManagementPage();
});


/**
 * 产生错误的topic提示信息
 * @param result
 * @returns {string}
 */
function authErrorMessage(result) {
    let errorMessage = "";
    if (null != result.extend.authorizationResult) {
        $.each(result.extend.authorizationResult, function (index, item) {
            errorMessage += "[" + item + "]<br/>";
        });
    }
    return errorMessage;
}

/**
 * btn_add_auth
 * 添加权限的按钮功能
 */
$(document).on("click", "#btn_add_auth", function () {
    let formData = $("#form_auth_account").serialize();
    showAuthModal("#auth_modal","#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done");
    $.ajax({
        url:rootPath+"/authAccount/addAuthorizationAccount",
        data:formData,
        type:"POST",
        success(result){
            //console.log(result);
            if (successCode===result.code) {
                changeAuthModalSuccess("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done");
            } else if (414===result.code){
                changeAuthModalFailed("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done",result.message);
            } else if(415===result.code){
                changeAuthModalFailed("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done",result.message);
            }else if(605===result.code){
                let errorMessage = authErrorMessage(result);
                changeAuthModalFailed("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done","Topics : <br/>"+errorMessage+" give authorization failed.");
            }else if (600===result.code){
                let errorMessage = authErrorMessage(result);
                changeAuthModalFailed("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done",errorMessage);
            }else {
                changeAuthModalFailed("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done",result.message);
            }
        }
    });
});

/**
 * btn_remove_auth
 * 移除权限的按钮功能
 */
$(document).on("click","#btn_remove_auth",function () {
    let formData = $("#form_auth_account").serialize();
    showAuthModal("#auth_modal","#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done");
    $.ajax({
        url:rootPath+"/authAccount/removeAuthorizationAccount",
        data:formData,
        type:"POST",
        success(result){
            //console.log(result);
            if (successCode===result.code) {
                changeAuthModalSuccess("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done");
            } else if (414===result.code){
                changeAuthModalFailed("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done",result.message);
            } else if(415===result.code){
                changeAuthModalFailed("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done",result.message);
            }else if(608===result.code){
                let errorMessage = authErrorMessage(result);
                changeAuthModalFailed("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done","Topics : <br/>"+errorMessage+" remove authorization failed.");
            }else if (600===result.code){
                let errorMessage = authErrorMessage(result);
                changeAuthModalFailed("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done",errorMessage);
            }else {
                changeAuthModalFailed("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done",result.message);
            }
        }
    });
});


/**
 * 添加group id的按钮功能
 */
$(document).on("click","#btn_groupId_add",function () {
    //alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content"200,"Topics are created.");
    let formData = $("#form_group_id").serialize();
    showAuthModal("#auth_modal","#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done");
    $.ajax({
       url:rootPath+"/authAccount/addGroupId",
       data:formData,
       type:"POST",
       success(result){
           //console.log(result);
           if (successCode===result.code) {
               changeAuthModalSuccess("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done");
           } else if (414===result.code){
               changeAuthModalFailed("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done",result.message);
           } else if(416===result.code){
               changeAuthModalFailed("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done",result.message);
           }else if(606===result.code){
               let errorMessage = authErrorMessage(result);
               changeAuthModalFailed("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done","Topics : <br/>"+errorMessage+" give group id failed.");
           }else if (600===result.code){
               let errorMessage = authErrorMessage(result);
               changeAuthModalFailed("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done",errorMessage);
           }else {
               changeAuthModalFailed("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done",result.message);
           }
       }
    });
});

$(document).on("click","#btn_groupId_remove",function () {
    let formData = $("#form_group_id").serialize();
    showAuthModal("#auth_modal","#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done");
    $.ajax({
        url:rootPath+"/authAccount/removeGroupId",
        type:"POST",
        data:formData,
        success(result){
            console.log(result);
            if (successCode===result.code) {
                changeAuthModalSuccess("#auth_pic","#auth_title","#spn_show","#spn_show","#auth_msg","#btn_auth_done");
            } else if (414===result.code){
                changeAuthModalFailed("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done",result.message);
            } else if(416===result.code){
                changeAuthModalFailed("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done",result.message);
            }else if(607===result.code){
                let errorMessage = authErrorMessage(result);
                changeAuthModalFailed("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done","Topics : <br/>"+errorMessage+" remove group id failed.");
            }else if (600===result.code){
                let errorMessage = authErrorMessage(result);
                changeAuthModalFailed("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done",errorMessage);
            }else {
                changeAuthModalFailed("#auth_pic","#auth_title","#spn_show","#auth_msg","#btn_auth_done",result.message);
            }
        }
    });
});



