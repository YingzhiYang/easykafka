$(function () {
    to_auth_list_page(1);
});

/**
 * 获取所有的ACL表格数据
 */
function to_auth_list_page(pn) {
    $.ajax({
        url:rootPath+"/aclDescription/getAllAclDescription",
        type:"GET",
        data: "pageNum="+pn+"&pattern="+$("#inpt_search_acl").val(),
        success(result){
            console.log(result);
            if (successCode===result.code){
                $("#auth_all_nums").empty().append(result.extend.kafkaResult.totalCount);
                build_auth_list_table(result);
                build_page_nav_acl(result,"#page_nav_area");
                build_page_info(result,"#page_info_area");
            }else {
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,errorTitle,result.message);
            }
        }
    });
}


$(document).on("click","#btn_search_acl",function () {
    to_acl_search($("#inpt_search_acl").val());
});

$(document).on("change","#inpt_search_acl",function () {
    to_acl_search($(this).val());
});

/**
 * 根据输入模糊搜索acl内容
 * @param pattern
 */
function to_acl_search(pattern) {
    $.ajax({
        url:rootPath+"/aclDescription/clickSearchAcl",
        data:"pattern="+pattern,
        type:"GET",
        success(result){
            console.log(result);
            if (successCode===result.code){
                $("#auth_all_nums").empty().append(result.extend.kafkaResult.totalCount);
                build_auth_list_table(result);
                build_page_nav_acl(result,"#page_nav_area");
                build_page_info(result,"#page_info_area");
            }else {
                alert_message("#alert_modal","#alert_title","#alert_msg","#alert_model_content",result.code,errorTitle,result.message);
            }
        }
    });
}

/**
 * 为所有的acl构建表格
 * @param result
 */
function build_auth_list_table(result) {
    $("#tbody_all_auth_info").empty();
    let authorizations=result.extend.kafkaResult.pageContent;
    $.each(authorizations,function (index,authorization) {
        let principal=$("<td></td>").addClass("align-middle").append(authorization.principal);
        let operation=$("<td></td>").addClass("align-middle").append(authorization.operation);
        let name=$("<td></td>").addClass("align-middle").append(authorization.name);
        let permission=$("<td></td>").addClass("align-middle").append(authorization.permission);
        let resourceType=$("<td></td>").addClass("align-middle").append(authorization.resourceType);
        let patternType=$("<td></td>").addClass("align-middle").append(authorization.patternType);
        $("<tr></tr>").append(principal).append(operation).append(name).append(resourceType).append(permission).append(patternType).appendTo("#tbody_all_auth_info");
    });
}
/**
 * 创建acl导航条
 * @param result
 * @param ele
 */
function build_page_nav_acl(result,ele) {
    $(ele).empty();
    let ul = $("<ul></ul>").addClass("pagination pagination-sm");
    let first = $("<li></li>").addClass("page-item").append(
        $("<a></a>").addClass("page-link").attr("href", "#")
            .append("First"));
    let previous = $("<li></li>").addClass("page-item").append(
        $("<a></a>").addClass("page-link").attr("href", "#").append(
            "&laquo;"));
    if (result.extend.kafkaResult.hasPreviousPage) {
        ul.append(first).append(previous);
        first.click(function () {
            console.log("go to first page");
            to_auth_list_page(result.extend.kafkaResult.firstPage);
        });

        previous.click(function () {
            if (result.extend.kafkaResult.currentPage - 1 <= 0) {
                to_auth_list_page(result.extend.kafkaResult.firstPage);
            } else {
                to_auth_list_page(result.extend.kafkaResult.currentPage - 1);
            }
        });
    } else {
        ul.append(first.addClass("disabled"));
    }

    $.each(result.extend.kafkaResult.currentNavigationPages,
        function (index, item) {
            let li = $("<li></li>").addClass("page-item").append(
                $("<a></a>").addClass("page-link").attr("href", "#")
                    .append(item));
            if (result.extend.kafkaResult.currentPage === item) {
                li.addClass("active");
            } else {
                li.click(function () {
                    to_auth_list_page(item);
                });
            }
            ul.append(li);
        });
    let next = $("<li></li>").addClass("page-item").append(
        $("<a></a>").addClass("page-link").attr("href", "#").append(
            "&raquo;"));
    let last = $("<li></li>").addClass("page-item")
        .append(
            $("<a></a>").addClass("page-link").attr("href", "#")
                .append("Last"));
    if (result.extend.kafkaResult.hasNextPage) {
        ul.append(next).append(last);
        last.click(function () {
            to_auth_list_page(result.extend.kafkaResult.lastPage);
        });
        next.click(function () {
            if (result.extend.kafkaResult.currentPage + 1 >= result.extend.kafkaResult.lastPage) {
                to_auth_list_page(result.extend.kafkaResult.lastPage);
            } else {
                to_auth_list_page(result.extend.kafkaResult.currentPage + 1);
            }
        });
    } else {
        ul.append(last.addClass("disabled"));
    }
    let nav = $("<nav></nav>").append(ul);
    nav.appendTo(ele);
}

$(document).on("click","#auth_page_back",function () {
    toClusterManagementPage();
});





