package com.kafka.easykafka.model;

public class ZookeeperInfo {
    private String clusterName;
    private String zkIpsNotPort;
    private Integer zkPort;

    @Override
    public String toString() {
        return "ZookeeperInfo{" +
                "clusterName='" + clusterName + '\'' +
                ", zkIpsNotPort='" + zkIpsNotPort + '\'' +
                ", zkPort=" + zkPort +
                '}';
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getZkIpsNotPort() {
        return zkIpsNotPort;
    }

    public void setZkIpsNotPort(String zkIpsNotPort) {
        this.zkIpsNotPort = zkIpsNotPort;
    }

    public Integer getZkPort() {
        return zkPort;
    }

    public void setZkPort(Integer zkPort) {
        this.zkPort = zkPort;
    }
}
