package com.kafka.easykafka.model;

public class EKPartitionLeader {

    private int leaderId;
    private String leaderHost;
    private int leaderPort;
    private int replicasNum;

    public EKPartitionLeader(int leaderId, String leaderHost, int leaderPort, int replicasNum) {
        this.leaderId = leaderId;
        this.leaderHost = leaderHost;
        this.leaderPort = leaderPort;
        this.replicasNum = replicasNum;
    }

    @Override
    public String toString() {
        return "PartitionLeader{" +
                "leaderId=" + leaderId +
                ", leaderHost='" + leaderHost + '\'' +
                ", leaderPort='" + leaderPort + '\'' +
                ", replicasNum=" + replicasNum +
                '}';
    }

    public int getLeaderId() {
        return leaderId;
    }

    public void setLeaderId(int leaderId) {
        this.leaderId = leaderId;
    }

    public String getLeaderHost() {
        return leaderHost;
    }

    public void setLeaderHost(String leaderHost) {
        this.leaderHost = leaderHost;
    }

    public int getLeaderPort() {
        return leaderPort;
    }

    public void setLeaderPort(int leaderPort) {
        this.leaderPort = leaderPort;
    }

    public int getReplicasNum() {
        return replicasNum;
    }

    public void setReplicasNum(int replicasNum) {
        this.replicasNum = replicasNum;
    }
}
