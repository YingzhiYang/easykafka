package com.kafka.easykafka.model;

import com.kafka.easykafka.util.Util;

import java.util.HashMap;
import java.util.Map;

public class Message {
    //200-成功，400-失败
    private Integer code=0;
    //提示信息
    private String message="";
    //返回给浏览器的数据单一数据，这里影响的是ProjManday里面的内容，重构时可删除
    private Object content=null;
    //返回给浏览器的复合数据
    private Map<String, Object> extend= new HashMap<>();

    public static Message success() {
        Message result=new Message();
        result.setCode(Util.SUC_OPERATION_200);
        result.setMessage(Util.SUC_MSG_200);
        return result;
    }

    public static Message fail(Integer code, String message) {
        Message result=new Message();
        result.setCode(code);
        result.setMessage(message);
        return result;
    }

    public static Message status(Integer code, String message) {
        Message result=new Message();
        result.setCode(code);
        result.setMessage(message);
        return result;
    }

    public Message addContent(String key, Object value) {
        this.getExtend().put(key, value);
        return this;
    }

    public Map<String, Object> getExtend() {
        return extend;
    }
    public void setExtend(Map<String, Object> extend) {
        this.extend = extend;
    }
    public Object getContent() {
        return content;
    }
    public void setContent(Object object) {
        this.content = object;
    }
    public Integer getCode() {
        return code;
    }
    public void setCode(Integer code) {
        this.code = code;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    @Override
    public String toString() {
        return "Message [code=" + code + ", message=" + message + ", msgcontent=" + content + "]";
    }



}
