package com.kafka.easykafka.model;

public class EKTopicPartition {
    private int partitionId;
    private EKPartitionLeader leader;

    public EKPartitionLeader getLeader() {
        return leader;
    }

    public void setLeader(EKPartitionLeader leader) {
        this.leader = leader;
    }

    public int getPartitionId() {
        return partitionId;
    }

    public void setPartitionId(int partitionId) {
        this.partitionId = partitionId;
    }


    @Override
    public String toString() {
        return "EKTopicPartition{" +
                "partitionId=" + partitionId +
                ", leader=" + leader +
                '}';
    }

}
