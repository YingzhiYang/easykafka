package com.kafka.easykafka.model;

public class TopicConfiguration {
    private Long retentionMs;
    private Integer maxMessageBytes;
    private Integer segmentIndexBytes;
    private Integer segmentBytes;
    private Double minCleanableDirtyRatio;
    private Integer minInsyncReplicas;
    private Long deleteRetentionMs;
    private Long flushMessages;
    private Boolean preallocate;
    private Long retentionBytes;
    private Long flushMs;
    private String cleanupPolicy;
    private Long fileDeleteDelayMs;
    private Long segmentJitterMs;
    private Integer indexIntervalBytes;
    private String compressionType;
    private Long segmentMs;
    private Boolean uncleanLeaderElectionEnable;

    public Long getRetentionMs() {
        return retentionMs;
    }

    public void setRetentionMs(Long retentionMs) {
        this.retentionMs = retentionMs;
    }

    public Integer getMaxMessageBytes() {
        return maxMessageBytes;
    }

    public void setMaxMessageBytes(Integer maxMessageBytes) {
        this.maxMessageBytes = maxMessageBytes;
    }

    public Integer getSegmentIndexBytes() {
        return segmentIndexBytes;
    }

    public void setSegmentIndexBytes(Integer segmentIndexBytes) {
        this.segmentIndexBytes = segmentIndexBytes;
    }

    public Integer getSegmentBytes() {
        return segmentBytes;
    }

    public void setSegmentBytes(Integer segmentBytes) {
        this.segmentBytes = segmentBytes;
    }

    public Double getMinCleanableDirtyRatio() {
        return minCleanableDirtyRatio;
    }

    public void setMinCleanableDirtyRatio(Double minCleanableDirtyRatio) {
        this.minCleanableDirtyRatio = minCleanableDirtyRatio;
    }

    public Integer getMinInsyncReplicas() {
        return minInsyncReplicas;
    }

    public void setMinInsyncReplicas(Integer minInsyncReplicas) {
        this.minInsyncReplicas = minInsyncReplicas;
    }

    public Long getDeleteRetentionMs() {
        return deleteRetentionMs;
    }

    public void setDeleteRetentionMs(Long deleteRetentionMs) {
        this.deleteRetentionMs = deleteRetentionMs;
    }

    public Long getFlushMessages() {
        return flushMessages;
    }

    public void setFlushMessages(Long flushMessages) {
        this.flushMessages = flushMessages;
    }

    public Boolean getPreallocate() {
        return preallocate;
    }

    public void setPreallocate(Boolean preallocate) {
        this.preallocate = preallocate;
    }

    public Long getRetentionBytes() {
        return retentionBytes;
    }

    public void setRetentionBytes(Long retentionBytes) {
        this.retentionBytes = retentionBytes;
    }

    public Long getFlushMs() {
        return flushMs;
    }

    public void setFlushMs(Long flushMs) {
        this.flushMs = flushMs;
    }

    public String getCleanupPolicy() {
        return cleanupPolicy;
    }

    public void setCleanupPolicy(String cleanupPolicy) {
        this.cleanupPolicy = cleanupPolicy;
    }

    public Long getFileDeleteDelayMs() {
        return fileDeleteDelayMs;
    }

    public void setFileDeleteDelayMs(Long fileDeleteDelayMs) {
        this.fileDeleteDelayMs = fileDeleteDelayMs;
    }

    public Long getSegmentJitterMs() {
        return segmentJitterMs;
    }

    public void setSegmentJitterMs(Long segmentJitterMs) {
        this.segmentJitterMs = segmentJitterMs;
    }

    public Integer getIndexIntervalBytes() {
        return indexIntervalBytes;
    }

    public void setIndexIntervalBytes(Integer indexIntervalBytes) {
        this.indexIntervalBytes = indexIntervalBytes;
    }

    public String getCompressionType() {
        return compressionType;
    }

    public void setCompressionType(String compressionType) {
        this.compressionType = compressionType;
    }

    public Long getSegmentMs() {
        return segmentMs;
    }

    public void setSegmentMs(Long segmentMs) {
        this.segmentMs = segmentMs;
    }

    public Boolean getUncleanLeaderElectionEnable() {
        return uncleanLeaderElectionEnable;
    }

    public void setUncleanLeaderElectionEnable(Boolean uncleanLeaderElectionEnable) {
        this.uncleanLeaderElectionEnable = uncleanLeaderElectionEnable;
    }

    @Override
    public String toString() {
        return "TopicConfiguration{" +
                "retentionMs=" + retentionMs +
                ", maxMessageBytes=" + maxMessageBytes +
                ", segmentIndexBytes=" + segmentIndexBytes +
                ", segmentBytes=" + segmentBytes +
                ", minCleanableDirtyRatio=" + minCleanableDirtyRatio +
                ", minInsyncReplicas=" + minInsyncReplicas +
                ", deleteRetentionMs=" + deleteRetentionMs +
                ", flushMessages=" + flushMessages +
                ", preallocate=" + preallocate +
                ", retentionBytes=" + retentionBytes +
                ", flushMs=" + flushMs +
                ", cleanupPolicy='" + cleanupPolicy + '\'' +
                ", fileDeleteDelayMs=" + fileDeleteDelayMs +
                ", segmentJitterMs=" + segmentJitterMs +
                ", indexIntervalBytes=" + indexIntervalBytes +
                ", compressionType='" + compressionType + '\'' +
                ", segmentMs=" + segmentMs +
                ", uncleanLeaderElectionEnable=" + uncleanLeaderElectionEnable +
                '}';
    }
}
