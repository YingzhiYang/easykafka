package com.kafka.easykafka.model;

public class EKTopicDescription {
    private String topicName;
    private int partitionNums;
    private EKPartitionLeader leader;

    public int getPartitionNums() {
        return partitionNums;
    }

    public void setPartitionNums(int partitionNums) {
        this.partitionNums = partitionNums;
    }


    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public EKPartitionLeader getLeader() {
        return leader;
    }

    public void setLeader(EKPartitionLeader leader) {
        this.leader = leader;
    }

    @Override
    public String toString() {
        return "EKTopicDescription{" +
                "topicName='" + topicName + '\'' +
                ", partitionNums=" + partitionNums +
                ", leader=" + leader +
                '}';
    }
}
