package com.kafka.easykafka.model;

public class ScramAccount {
    private String account;
    private String password;
    private String clusterName;
    private final String SCRAM_MECHANISM="SCRAM_SHA_512";


    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSCRAM_MECHANISM() {
        return SCRAM_MECHANISM;
    }

    @Override
    public String toString() {
        return "ScramAccount{" +
                "account='" + account + '\'' +
                ", password='" + password + '\'' +
                ", clusterName='" + clusterName + '\'' +
                ", SCRAM_MECHANISM='" + SCRAM_MECHANISM + '\'' +
                '}';
    }
}
