package com.kafka.easykafka.model;

@Deprecated
public class LinuxConnect {
    private static String host="10.122.49.164";// 服务器地址
    private static String userName="appadm";// 用户名
    private static String password="yBH9T8iYAmLR";// 密码
    private static int port=22;// 端口号

    public static int getPort() {
        return port;
    }

    public void setPort(int port) {
        LinuxConnect.port = port;
    }

    public static String getHost() {
        return host;
    }

    public void setHost(String host) {
        LinuxConnect.host = host;
    }

    public static String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        LinuxConnect.userName = userName;
    }

    public static String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        LinuxConnect.password = password;
    }

    @Override
    public String toString() {
        return "LinuxConnect{" +
                "host='" + host + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", port=" + port +
                '}';
    }
}
