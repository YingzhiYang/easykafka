package com.kafka.easykafka.model;


public class Cluster {
    private Integer clusterId;
    private String clusterName;
    private String zkIps;
    private String kfkIps;
    private String supUser;
    private String supPwd;
    private String securityProtocol;
    private String saslMechanism;
    private String sslPws;
    private String jksPath;
    //Zookeeper Chroot Path
    private String zkNodePath;
    private String zkConfigPath;
    private String kfkConfigPath;
    private String zkIpsNotPort;
    private String kfkIpsNotPort;
    private Integer zkPort;
    private Integer kfkPort;

    @Override
    public String toString() {
        return "Cluster{" +
                "clusterId=" + clusterId +
                ", clusterName='" + clusterName + '\'' +
                ", zkIps='" + zkIps + '\'' +
                ", kfkIps='" + kfkIps + '\'' +
                ", supUser='" + supUser + '\'' +
                ", supPwd='" + supPwd + '\'' +
                ", securityProtocol='" + securityProtocol + '\'' +
                ", saslMechanism='" + saslMechanism + '\'' +
                ", sslPws='" + sslPws + '\'' +
                ", jksPath='" + jksPath + '\'' +
                ", zkNodePath='" + zkNodePath + '\'' +
                ", zkConfigPath='" + zkConfigPath + '\'' +
                ", kfkConfigPath='" + kfkConfigPath + '\'' +
                ", zkIpsNotPort='" + zkIpsNotPort + '\'' +
                ", kfkIpsNotPort='" + kfkIpsNotPort + '\'' +
                ", zkPort=" + zkPort +
                ", kfkPort=" + kfkPort +
                '}';
    }

    public String getKfkConfigPath() {
        return kfkConfigPath;
    }

    public void setKfkConfigPath(String kfkConfigPath) {
        this.kfkConfigPath = kfkConfigPath;
    }

    public String getKfkIpsNotPort() {
        return kfkIpsNotPort;
    }

    public void setKfkIpsNotPort(String kfkIpsNotPort) {
        this.kfkIpsNotPort = kfkIpsNotPort;
    }

    public Integer getKfkPort() {
        return kfkPort;
    }

    public void setKfkPort(Integer kfkPort) {
        this.kfkPort = kfkPort;
    }

    public String getZkIpsNotPort() {
        return zkIpsNotPort;
    }

    public void setZkIpsNotPort(String zkIpsNotPort) {
        this.zkIpsNotPort = zkIpsNotPort;
    }

    public Integer getZkPort() {
        return zkPort;
    }

    public void setZkPort(Integer zkPort) {
        this.zkPort = zkPort;
    }


    public String getZkNodePath() {
        return zkNodePath;
    }

    public void setZkNodePath(String zkNodePath) {
        this.zkNodePath = zkNodePath;
    }

    public String getZkConfigPath() {
        return zkConfigPath;
    }

    public void setZkConfigPath(String zkConfigPath) {
        this.zkConfigPath = zkConfigPath;
    }

    public Integer getClusterId() {
        return clusterId;
    }

    public void setClusterId(Integer clusterId) {
        this.clusterId = clusterId;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getZkIps() {
        return zkIps;
    }

    public void setZkIps(String zkIps) {
        this.zkIps = zkIps;
    }

    public String getKfkIps() {
        return kfkIps;
    }

    public void setKfkIps(String kfkIps) {
        this.kfkIps = kfkIps;
    }

    public String getSupUser() {
        return supUser;
    }

    public void setSupUser(String supUser) {
        this.supUser = supUser;
    }

    public String getSupPwd() {
        return supPwd;
    }

    public void setSupPwd(String supPwd) {
        this.supPwd = supPwd;
    }

    public String getSecurityProtocol() {
        return securityProtocol;
    }

    public void setSecurityProtocol(String securityProtocol) {
        this.securityProtocol = securityProtocol;
    }

    public String getSaslMechanism() {
        return saslMechanism;
    }

    public void setSaslMechanism(String saslMechanism) {
        this.saslMechanism = saslMechanism;
    }

    public String getSslPws() {
        return sslPws;
    }

    public void setSslPws(String sslPws) {
        this.sslPws = sslPws;
    }

    public String getJksPath() {
        return jksPath;
    }

    public void setJksPath(String jksPath) {
        this.jksPath = jksPath;
    }
}
