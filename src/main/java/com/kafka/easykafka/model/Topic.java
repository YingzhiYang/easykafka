package com.kafka.easykafka.model;

/**
 * 描述一个Topic
 * offsets.topic.replication.factor short
 * num.partitions int
 * default.replication.factor int
 * retention.ms long
 * max.message.bytes int
 * segment.index.bytes int
 * segment.bytes int
 * min.cleanable.dirty.ratio  double
 * min.insync.replicas int
 * delete.retention.ms  long
 * flush.messages long
 * preallocate boolean
 * retention.bytes long
 * flush.ms long
 * cleanup.policy list
 * file.delete.delay.ms long
 * segment.jitter.ms long
 * index.interval.bytes int
 * compression.type string
 * segment.ms long
 * unclean.leader.election.enable boolean
 */
public class Topic {
    private String topicName;
    private Integer topicPartition;
    private Short topicReplicationFactor;
    private Long retentionMs;
    private Integer maxMessageBytes;
    private Integer segmentIndexBytes;
    private Integer segmentBytes;
    private Double minCleanableDirtyRatio;
    private Integer minInsyncReplicas;
    private Long deleteRetentionMs;
    private Long flushMessages;
    private Boolean preallocate;
    private Long retentionBytes;
    private Long flushMs;
    private String cleanupPolicy;
    private Long fileDeleteDelayMs;
    private Long segmentJitterMs;
    private Integer indexIntervalBytes;
    private String compressionType;
    private Long segmentMs;
    private Boolean uncleanLeaderElectionEnable;

    public Topic(String topicName, Integer topicPartition, Short topicReplicationFactor, Long retentionMs, Integer maxMessageBytes, Integer segmentIndexBytes, Integer segmentBytes, Double minCleanableDirtyRatio, Integer minInsyncReplicas, Long deleteRetentionMs, Long flushMessages, Boolean preallocate, Long retentionBytes, Long flushMs, String cleanupPolicy, Long fileDeleteDelayMs, Long segmentJitterMs, Integer indexIntervalBytes, String compressionType, Long segmentMs, Boolean uncleanLeaderElectionEnable) {
        this.topicName = topicName;
        this.topicPartition = topicPartition;
        this.topicReplicationFactor = topicReplicationFactor;
        this.retentionMs = retentionMs;
        this.maxMessageBytes = maxMessageBytes;
        this.segmentIndexBytes = segmentIndexBytes;
        this.segmentBytes = segmentBytes;
        this.minCleanableDirtyRatio = minCleanableDirtyRatio;
        this.minInsyncReplicas = minInsyncReplicas;
        this.deleteRetentionMs = deleteRetentionMs;
        this.flushMessages = flushMessages;
        this.preallocate = preallocate;
        this.retentionBytes = retentionBytes;
        this.flushMs = flushMs;
        this.cleanupPolicy = cleanupPolicy;
        this.fileDeleteDelayMs = fileDeleteDelayMs;
        this.segmentJitterMs = segmentJitterMs;
        this.indexIntervalBytes = indexIntervalBytes;
        this.compressionType = compressionType;
        this.segmentMs = segmentMs;
        this.uncleanLeaderElectionEnable = uncleanLeaderElectionEnable;
    }

    @Override
    public String toString() {
        return "Topic{" +
                "topicName='" + topicName + '\'' +
                ", topicPartition=" + topicPartition +
                ", topicReplicationFactor=" + topicReplicationFactor +
                ", retentionMs=" + retentionMs +
                ", maxMessageBytes=" + maxMessageBytes +
                ", segmentIndexBytes=" + segmentIndexBytes +
                ", segmentBytes=" + segmentBytes +
                ", minCleanableDirtyRatio=" + minCleanableDirtyRatio +
                ", minInsyncReplicas=" + minInsyncReplicas +
                ", deleteRetentionMs=" + deleteRetentionMs +
                ", flushMessages=" + flushMessages +
                ", preallocate=" + preallocate +
                ", retentionBytes=" + retentionBytes +
                ", flushMs=" + flushMs +
                ", cleanupPolicy='" + cleanupPolicy + '\'' +
                ", fileDeleteDelayMs=" + fileDeleteDelayMs +
                ", segmentJitterMs=" + segmentJitterMs +
                ", indexIntervalBytes=" + indexIntervalBytes +
                ", compressionType='" + compressionType + '\'' +
                ", segmentMs=" + segmentMs +
                ", uncleanLeaderElectionEnable=" + uncleanLeaderElectionEnable +
                '}';
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public Integer getTopicPartition() {
        return topicPartition;
    }

    public void setTopicPartition(Integer topicPartition) {
        this.topicPartition = topicPartition;
    }

    public Short getTopicReplicationFactor() {
        return topicReplicationFactor;
    }

    public void setTopicReplicationFactor(Short topicReplicationFactor) {
        this.topicReplicationFactor = topicReplicationFactor;
    }

    public Long getRetentionMs() {
        return retentionMs;
    }

    public void setRetentionMs(Long retentionMs) {
        this.retentionMs = retentionMs;
    }

    public Integer getMaxMessageBytes() {
        return maxMessageBytes;
    }

    public void setMaxMessageBytes(Integer maxMessageBytes) {
        this.maxMessageBytes = maxMessageBytes;
    }

    public Integer getSegmentIndexBytes() {
        return segmentIndexBytes;
    }

    public void setSegmentIndexBytes(Integer segmentIndexBytes) {
        this.segmentIndexBytes = segmentIndexBytes;
    }

    public Integer getSegmentBytes() {
        return segmentBytes;
    }

    public void setSegmentBytes(Integer segmentBytes) {
        this.segmentBytes = segmentBytes;
    }

    public Double getMinCleanableDirtyRatio() {
        return minCleanableDirtyRatio;
    }

    public void setMinCleanableDirtyRatio(Double minCleanableDirtyRatio) {
        this.minCleanableDirtyRatio = minCleanableDirtyRatio;
    }

    public Integer getMinInsyncReplicas() {
        return minInsyncReplicas;
    }

    public void setMinInsyncReplicas(Integer minInsyncReplicas) {
        this.minInsyncReplicas = minInsyncReplicas;
    }

    public Long getDeleteRetentionMs() {
        return deleteRetentionMs;
    }

    public void setDeleteRetentionMs(Long deleteRetentionMs) {
        this.deleteRetentionMs = deleteRetentionMs;
    }

    public Long getFlushMessages() {
        return flushMessages;
    }

    public void setFlushMessages(Long flushMessages) {
        this.flushMessages = flushMessages;
    }

    public Boolean getPreallocate() {
        return preallocate;
    }

    public void setPreallocate(Boolean preallocate) {
        this.preallocate = preallocate;
    }

    public Long getRetentionBytes() {
        return retentionBytes;
    }

    public void setRetentionBytes(Long retentionBytes) {
        this.retentionBytes = retentionBytes;
    }

    public Long getFlushMs() {
        return flushMs;
    }

    public void setFlushMs(Long flushMs) {
        this.flushMs = flushMs;
    }

    public String getCleanupPolicy() {
        return cleanupPolicy;
    }

    public void setCleanupPolicy(String cleanupPolicy) {
        this.cleanupPolicy = cleanupPolicy;
    }

    public Long getFileDeleteDelayMs() {
        return fileDeleteDelayMs;
    }

    public void setFileDeleteDelayMs(Long fileDeleteDelayMs) {
        this.fileDeleteDelayMs = fileDeleteDelayMs;
    }

    public Long getSegmentJitterMs() {
        return segmentJitterMs;
    }

    public void setSegmentJitterMs(Long segmentJitterMs) {
        this.segmentJitterMs = segmentJitterMs;
    }

    public Integer getIndexIntervalBytes() {
        return indexIntervalBytes;
    }

    public void setIndexIntervalBytes(Integer indexIntervalBytes) {
        this.indexIntervalBytes = indexIntervalBytes;
    }

    public String getCompressionType() {
        return compressionType;
    }

    public void setCompressionType(String compressionType) {
        this.compressionType = compressionType;
    }

    public Long getSegmentMs() {
        return segmentMs;
    }

    public void setSegmentMs(Long segmentMs) {
        this.segmentMs = segmentMs;
    }

    public Boolean getUncleanLeaderElectionEnable() {
        return uncleanLeaderElectionEnable;
    }

    public void setUncleanLeaderElectionEnable(Boolean uncleanLeaderElectionEnable) {
        this.uncleanLeaderElectionEnable = uncleanLeaderElectionEnable;
    }

}
