package com.kafka.easykafka.model;

public class Group {

    private String groupId;
    private String accountName;

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public void setAccountName(String account) {
        this.accountName = account;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getAccountName() {
        return accountName;
    }

    @Override
    public String toString() {
        return "Group{" +
                "GroupId='" + groupId + '\'' +
                ", account='" + accountName + '\'' +
                '}';
    }
}
