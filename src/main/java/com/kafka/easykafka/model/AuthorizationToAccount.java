package com.kafka.easykafka.model;

public class AuthorizationToAccount {
    private String accountName;
    private String topics;
    private String authorization;

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getTopics() {
        return topics;
    }

    public void setTopics(String topics) {
        this.topics = topics;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    @Override
    public String toString() {
        return "AuthorizationToAccount{" +
                "accountName='" + accountName + '\'' +
                ", topics='" + topics + '\'' +
                ", authorization='" + authorization + '\'' +
                '}';
    }
}
