package com.kafka.easykafka.model;

import java.util.ArrayList;

/**
 * 用来描述一个Zookeeper的集群ip和Host群的model
 */
public class ZookeeperHostInfo {
    private ArrayList<String> ipsList;
    private Integer clientPort;


    @Override
    public String toString() {
        return "ZookeeperHostInfo{" +
                "ipsList=" + ipsList +
                ", clientPort=" + clientPort +
                '}';
    }

    public ArrayList<String> getIpsList() {
        return ipsList;
    }

    public void setIpsList(ArrayList<String> ipsList) {
        this.ipsList = ipsList;
    }

    public Integer getClientPort() {
        return clientPort;
    }

    public void setClientPort(Integer clientPort) {
        this.clientPort = clientPort;
    }
}
