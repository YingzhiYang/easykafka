package com.kafka.easykafka.model;

public class EKTopicAuthorization {
    /**
     * 当前topic 名字
     */
    private String name;
    /**
     * 授权方式，是显示的LITERAL，还是前缀的Prefix的
     */
    private String patternType;
    /**
     * 授权对象，是Topic，还是Group等等
     */
    private String resourceType;
    /**
     * 用户的账户名字
     */
    private String principal;
    /**
     * 是否允许操作这个权限Allow，Deny
     */
    private String permission;
    /**
     * 操作的权限是什么
     */
    private String operation;

    public EKTopicAuthorization(String name, String patternType, String resourceType, String principal, String permission, String operation) {
        this.name = name;
        this.patternType = patternType;
        this.resourceType = resourceType;
        this.principal = principal;
        this.permission = permission;
        this.operation = operation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatternType() {
        return patternType;
    }

    public void setPatternType(String patternType) {
        this.patternType = patternType;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    @Override
    public String toString() {
        return "EKTopicAuthorization{" +
                "name='" + name + '\'' +
                ", patternType='" + patternType + '\'' +
                ", resourceType='" + resourceType + '\'' +
                ", principal='" + principal + '\'' +
                ", permission='" + permission + '\'' +
                ", operation='" + operation + '\'' +
                '}';
    }
}
