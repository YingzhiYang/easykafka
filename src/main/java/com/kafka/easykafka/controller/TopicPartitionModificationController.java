package com.kafka.easykafka.controller;

import java.util.concurrent.ExecutionException;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kafka.easykafka.model.Message;
import com.kafka.easykafka.service.PartitionOperationService;
import com.kafka.easykafka.util.GlobalPropoties;
import com.kafka.easykafka.util.Util;

@Controller
@RequestMapping("/partition")
public class TopicPartitionModificationController {

    @Resource
    private PartitionOperationService partitionOperationService;

    @GetMapping("/getTopicPartitions")
    @ResponseBody
    public Message getTopicPartitions(String topicName) throws ExecutionException, InterruptedException {
        Integer partitions=partitionOperationService.getTopicPartitions(topicName);
        if (Util.ZERO < partitions){
            return Message.success().addContent(GlobalPropoties.TOPIC_PARTITIONS,partitions);
        }else {
            return Message.fail(Util.ERR_CODE_400,Util.ERR_MSG_400);
        }
    }
    
    @PostMapping("/increasePartitions")
    @ResponseBody
    public Message increasePartitions(@RequestParam("topicName")String topicName, @RequestParam("partitions")Integer partitions) throws InterruptedException, ExecutionException {
    	//System.out.println(topicName+":"+partitions);
		if (partitionOperationService.setTopicPartitions(topicName, partitions)) {
			return Message.success();
		}else {
			return Message.fail(Util.ERR_CODE_400,Util.ERR_MSG_400);
		}
    	
	}
    
}
