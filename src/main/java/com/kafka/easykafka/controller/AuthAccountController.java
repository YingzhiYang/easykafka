package com.kafka.easykafka.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kafka.easykafka.model.AuthorizationToAccount;
import com.kafka.easykafka.model.Group;
import com.kafka.easykafka.model.Message;
import com.kafka.easykafka.service.AuthAccountService;
import com.kafka.easykafka.util.Util;

@Controller
@RequestMapping("/authAccount")
public class AuthAccountController {

    private AuthAccountService authAccountService;

    @Autowired
    public void setAuthAccountService(AuthAccountService authAccountService) {
        this.authAccountService = authAccountService;
    }

    @RequestMapping("/addAuthorizationAccount")
    @ResponseBody
    @ExceptionHandler(RuntimeException.class)
    public Message addAuthorizationAccount(AuthorizationToAccount authorizationToAccount) {
        System.out.println(authorizationToAccount.toString());
        if (authorizationToAccount.getAccountName().isEmpty()) {
            return Message.fail(Util.ERR_ACCOUNT_NAME_415, Util.ERR_MSG_415);
        }
        if (authorizationToAccount.getTopics().isEmpty()) {
            return Message.fail(Util.ERR_TOPIC_NAME_414, Util.ERR_MSG_414);
        }
        Message message=authAccountService.addAuthorizationAccount(authorizationToAccount);
        if(Util.SUC_OPERATION_200.equals(message.getCode())){
            return message;
        }else {
            Util.unknowErrorAlert(this.getClass().getSimpleName());
            return Message.fail(Util.ERR_CODE_400, Util.ERR_MSG_400);
        }
    }

    @RequestMapping("/removeAuthorizationAccount")
    @ResponseBody
    public Message removeAuthorizationAccount(AuthorizationToAccount authorizationToAccount) {
        //System.out.println(authorizationToAccount.toString());
        if (authorizationToAccount.getAccountName().isEmpty()) {
            return Message.fail(Util.ERR_ACCOUNT_NAME_415, Util.ERR_MSG_415);
        }
        if (authorizationToAccount.getTopics().isEmpty()) {
            return Message.fail(Util.ERR_TOPIC_NAME_414, Util.ERR_MSG_414);
        }
        try {
            return authAccountService.removeAuthorizationAccount(authorizationToAccount);
        } catch (Exception e) {
            Util.unknowErrorAlert(this.getClass().getSimpleName());
            return Message.fail(Util.ERR_CODE_400, Util.ERR_MSG_400);
        }
    }

    @RequestMapping("/addGroupId")
    @ResponseBody
    public Message addGroupId(Group group){
        //System.out.println(group.toString());
        if (group.getAccountName().isEmpty()) {
            return Message.fail(Util.ERR_ACCOUNT_NAME_415, Util.ERR_MSG_415);
        }
        if (group.getGroupId().isEmpty()) {
            return Message.fail(Util.ERR_GROUP_ID_416, Util.ERR_MSG_416);
        }
        try {
            return authAccountService.addGroupId(group);
        }catch (Exception e){
            Util.unknowErrorAlert(this.getClass().getSimpleName());
            return Message.fail(Util.ERR_CODE_400, Util.ERR_MSG_400);
        }
    }

    @RequestMapping("/removeGroupId")
    @ResponseBody
    public Message removeGroupId(Group group){
        System.out.println(group.toString());
        try{
            return authAccountService.removeGroupId(group);
        }catch (Exception e){
            Util.unknowErrorAlert(this.getClass().getSimpleName());
            return Message.fail(Util.ERR_CODE_400, Util.ERR_MSG_400);
        }
    }


}
