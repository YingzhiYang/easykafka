package com.kafka.easykafka.controller;

import com.kafka.easykafka.model.Message;
import com.kafka.easykafka.model.ScramAccount;
import com.kafka.easykafka.service.AccountOperationService;
import com.kafka.easykafka.util.GlobalPropoties;
import com.kafka.easykafka.util.Util;
import org.apache.zookeeper.KeeperException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Controller
@RequestMapping("/accountOperation")
public class AccountController {

    @Resource
    AccountOperationService accountService;


    @PostMapping("/createAccount")
    @ResponseBody
    public Message createAccount(ScramAccount account) throws NoSuchAlgorithmException {
        if(accountService.createScramAccount(account))
            return Message.fail(Util.ERR_CODE_600, Util.ERR_MSG_600);
        return Message.success();
    }


    @DeleteMapping("/deleteAccount")
    @ResponseBody
    public Message deleteAccount(ScramAccount account) throws NoSuchAlgorithmException, InterruptedException, ExecutionException, KeeperException {
        System.out.println(account.toString());
        List<String> errorList=accountService.deleteScramAccount(account);
        if(errorList.isEmpty())
            return Message.success();
        else if (errorList.get(0).equals(GlobalPropoties.ZK_DEL_REPORT)){
            return Message.fail(Util.ERR_CODE_609,Util.ERR_MSG_609);
        }
        return Message.fail(Util.ERR_CODE_600, Util.ERR_MSG_600);
    }

    /*@PostMapping("/createAccount")
    @ResponseBody
    public Message createAccount(Account account) throws IOException, JSchException {

        if(accountService.createAccount(account)){
            return Message.success();
        }
        return Message.fail(Util.ERR_CODE_600,Util.ERR_MSG_600);
    }*/

    @GetMapping("/generatePassword")
    @ResponseBody
    public Message generatePassword(@RequestParam("nums") Integer nums,
                                    @RequestParam("lowercase") Integer lowercase,
                                    @RequestParam("capital") Integer capital,
                                    @RequestParam("length") Integer length) {
        if (nums.equals(Util.ZERO) & lowercase.equals(Util.ZERO) & capital.equals(Util.ZERO)) {
            return Message.fail(Util.ERR_DATA_EMPTY_403, Util.ERR_MSG_403);
        }
        return Message.success().addContent("password", accountService.generatePassword(nums, lowercase, capital, length));
    }
}
