package com.kafka.easykafka.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kafka.easykafka.exception.GlobalExceptionMessage;
import com.kafka.easykafka.model.EKTopicAuthorization;
import com.kafka.easykafka.model.Message;
import com.kafka.easykafka.service.AuthorizationDescriptionService;
import com.kafka.easykafka.util.GlobalPropoties;
import com.kafka.easykafka.util.Util;
import com.kafka.easykafka.util.UtilPages;

@Controller
@RequestMapping("/aclDescription")
public class AuthorizationDescriptionController {

    @Resource
    AuthorizationDescriptionService aclService;
    private ArrayList<EKTopicAuthorization> allAuthorizations = new ArrayList<>();
    ArrayList<EKTopicAuthorization> aclSingleList = new ArrayList<>();

    @GetMapping("/getAllAclDescription")
    @ResponseBody
    public Message getAllAclDescription(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                        @RequestParam(value = "pattern", defaultValue = "") String pattern) throws ExecutionException, InterruptedException {
        if (tryToGetAllACLInfo()) {
            return Message.fail(Util.ERR_DATA_EMPTY_403, Util.ERR_MSG_403);
        }
        UtilPages pageInfo = new UtilPages();
        if (StringUtils.isEmpty(pattern)){
            pageInfo.getInitialPages(pageInfo, (List) allAuthorizations, pageNum);
        }else {
            ArrayList<EKTopicAuthorization> aclPartList = aclService.getClickSearchedACL(allAuthorizations, pattern);
            pageInfo.getInitialPages(pageInfo, (List) aclPartList, pageNum);
        }
        return Message.success().addContent(Util.KAFKA_RESULT, pageInfo);

    }

    private boolean tryToGetAllACLInfo() throws ExecutionException, InterruptedException{
        allAuthorizations=aclService.getAllAclDescription();
        return CollectionUtils.isEmpty(allAuthorizations);
    }


    @GetMapping("/clickSearchAcl")
    @ResponseBody
    public Message clickSearchAcl(@RequestParam("pattern") String pattern) {
        UtilPages pageInfo = new UtilPages();
        if (StringUtils.isEmpty(pattern)){
            pageInfo.getInitialPages(pageInfo, (List) allAuthorizations, Util.FIRST_PAGE);
        }
        ArrayList<EKTopicAuthorization> aclPartList = aclService.getClickSearchedACL(allAuthorizations, pattern);
        if (CollectionUtils.isEmpty(aclPartList)){
            pageInfo.getInitialPages(pageInfo, (List) allAuthorizations, Util.FIRST_PAGE);
        }else {
            pageInfo.getInitialPages(pageInfo, (List) aclPartList, Util.FIRST_PAGE);
        }
        return Message.success().addContent(Util.KAFKA_RESULT, pageInfo);
    }


    @GetMapping("/getSingleAcl")
    @ResponseBody
    public Message getSingleAcl(@RequestParam("account")String account) throws ExecutionException, InterruptedException {
        if (StringUtils.isEmpty(account)){
            return Message.fail(Util.SUC_EMPTY_202,Util.SUC_MEG_202);
        }

        aclSingleList = aclService.getSingleAclDescription(account);
        if(CollectionUtils.isEmpty(aclSingleList)){
            return Message.fail(Util.SUC_EMPTY_202,Util.SUC_MEG_202);
        }else {
            UtilPages pageInfo = new UtilPages();
            pageInfo.getInitialPages(pageInfo, (List) aclSingleList, Util.FIRST_PAGE);
            return Message.success().addContent(Util.KAFKA_RESULT, pageInfo);
        }
    }

    @GetMapping("/getSingleAclForPageNum")
    @ResponseBody
    public Message getSingleAclForPageNum(@RequestParam("pageNum")Integer pageNum){
        UtilPages pageInfo = new UtilPages();
        pageInfo.getInitialPages(pageInfo, (List) aclSingleList, pageNum);
        return Message.success().addContent(Util.KAFKA_RESULT, pageInfo);
    }



    @RequestMapping("/toSingleAuthorizationPage")
    public String toSingleAuthorizationPage(){
        return GlobalPropoties.AUTH_VIEW_SINGLE_PAGE;
    }



    @ExceptionHandler({Exception.class, ExecutionException.class, InterruptedException.class})
    @ResponseBody
    public Message AuthorizationDescriptionControllerExceptionHandler(Exception e) {
        return Message.fail(Util.ERR_CODE_400, Util.ERR_MSG_400).addContent(GlobalPropoties.EXCEPTION_FLAG,
                new GlobalExceptionMessage(
                        e.getStackTrace()[0].getClassName(),
                        e.getStackTrace()[0].getMethodName(),
                        e.getStackTrace()[0].getLineNumber(),
                        e.getMessage()));
    }
}
