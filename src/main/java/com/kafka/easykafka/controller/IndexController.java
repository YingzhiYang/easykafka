package com.kafka.easykafka.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kafka.easykafka.util.GlobalPropoties;

@Controller
@RequestMapping("/index")
public class IndexController {

    @RequestMapping("/")
    public String indexWithSlash() {
        return GlobalPropoties.INDEX_PAGE;
    }

    @RequestMapping("")
    public String indexWithEmpty() {
        return GlobalPropoties.INDEX_PAGE;
    }

    @RequestMapping("/toAddCluster")
    public String addClusterPage() {
        return GlobalPropoties.ADD_CLUSTER_PAGE;
    }


}
