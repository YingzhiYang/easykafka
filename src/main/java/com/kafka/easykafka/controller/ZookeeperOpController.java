package com.kafka.easykafka.controller;

import com.kafka.easykafka.exception.GlobalExceptionMessage;
import com.kafka.easykafka.model.Message;
import com.kafka.easykafka.service.ZookeeperOpService;
import com.kafka.easykafka.util.GlobalPropoties;
import com.kafka.easykafka.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Controller
@RequestMapping("/zookeeperOp")
public class ZookeeperOpController {

    private ZookeeperOpService zookeeperOpService;

    @Autowired
    public ZookeeperOpController(ZookeeperOpService zookeeperOpService) {
        this.zookeeperOpService = zookeeperOpService;
    }

    @GetMapping("/getZookeeperListInfo")
    @ResponseBody
    public Message getZookeeperListInfo(@RequestParam("clusterName") String clusterName) throws IOException {
        return Message.success().addContent(GlobalPropoties.ZOOKEEPER_INFO,zookeeperOpService.getZookeeperListInfo(clusterName));
    }

    @GetMapping("/getZookeeperPing")
    @ResponseBody
    public Message getZookeeperPing(@RequestParam("ip") String ip,@RequestParam("port") Integer port) throws IOException {
        if (zookeeperOpService.getZookeeperPing(ip,port)){
            return Message.success();
        }else {
            return Message.fail(Util.ERR_CODE_400,Util.ERR_MSG_400);
        }
    }


    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Message ZookeeperOpControllerExceptionHandler(Exception e){
        return Message.fail(Util.ERR_CODE_400, Util.ERR_MSG_400).addContent(GlobalPropoties.EXCEPTION_FLAG,
                new GlobalExceptionMessage(
                        e.getStackTrace()[0].getClassName(),
                        e.getStackTrace()[0].getMethodName(),
                        e.getStackTrace()[0].getLineNumber(),
                        e.getMessage()));
    }

}
