package com.kafka.easykafka.controller;

import com.kafka.easykafka.model.Cluster;
import com.kafka.easykafka.model.Message;
import com.kafka.easykafka.service.ClustersService;
import com.kafka.easykafka.util.GlobalPropoties;
import com.kafka.easykafka.util.KafkaConnection;
import com.kafka.easykafka.util.Util;
import com.kafka.easykafka.util.ZookeeperConnection;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Objects;

@Controller
@RequestMapping("/cluster")
public class ClusterController {

    @Resource
    ClustersService service;

    @RequestMapping("/initClusterConnect")
    @ResponseBody
    public Message initClusterConnect(@RequestParam("clusterName") String clusterName) {
        System.out.println("clusterManagement:" + clusterName);
        //数据库获取cluster连接信息
        Cluster cluster = service.findCluster(clusterName);
        //设置/刷新KafkaConnection连接信息
        KafkaConnection.setAdminClient(
                KafkaConnection.buildAdminClientConnection(
                        cluster.getKfkIps(),
                        cluster.getSecurityProtocol(),
                        cluster.getJksPath(),
                        cluster.getSslPws(),
                        cluster.getSaslMechanism(),
                        Util.generateSaslJaasConfig(cluster.getSupUser(), cluster.getSupPwd())));
        ZookeeperConnection.refreshZookeeperConnection(clusterName);
        
        if (Objects.nonNull(KafkaConnection.getAdminClient())) {
            return Message.success();
        } else {
            return Message.fail(Util.ERR_DISCONNECT_KAFKA_CLUSTER_413, Util.ERR_MSG_413);
        }

    }

    @RequestMapping("/toClusterManagementPage")
    public String toClusterManagementPage() {
        return GlobalPropoties.CLUSTER_MANAGE_PAGE;
    }

    @RequestMapping("/toTopicPage")
    public String toTopicPage() {
        return GlobalPropoties.TOPIC_PAGE;
    }


    @RequestMapping("/toAuthAccountPage")
    public String toAuthAccountPage() {
        return GlobalPropoties.AUTH_ACCOUNT_PAGE;
    }


    @RequestMapping("/toAuthViewPage")
    public String toAuthViewPage() {
        return GlobalPropoties.AUTH_VIEW_PAGE;
    }


    @RequestMapping("/toZookeeperViewPage")
    public String toZookeeperViewPage() {
        return GlobalPropoties.ZOOKEEPER_VIEW_PAGE;
    }


}
