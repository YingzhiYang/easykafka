package com.kafka.easykafka.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kafka.easykafka.mapper.ClustersMapper;
import com.kafka.easykafka.model.Message;
import com.kafka.easykafka.model.Topic;
import com.kafka.easykafka.service.TopicService;
import com.kafka.easykafka.util.GlobalPropoties;
import com.kafka.easykafka.util.Util;
import com.kafka.easykafka.util.UtilPages;

@Controller
@RequestMapping("/topic")
public class TopicController {

    @Resource
    private ClustersMapper clustersMapper;

    @Resource
    private TopicService topicService;
    private Set<String> topicSet = new HashSet<>();
    private List<String> topicList = new LinkedList<>();
    private String clusterID = "";

    /**
     * 更新页面
     *
     * @param clusterName 传入集群名称
     * @param pn          传入的页面号码
     * @param pattern     模糊匹配的topic名字字符
     * @return 返回Message对象
     */
    @RequestMapping("/getAllTopics")
    @ResponseBody
    public Message getAllTopics(@RequestParam("clusterName") String clusterName,
                                @RequestParam(value = "pn", defaultValue = "1") Integer pn,
                                @RequestParam(value = "pattern", defaultValue = "") String pattern) {
        clusterID = clusterName;
        if (tryToGetTopic(clusterName))
            return Message.fail(Util.ERR_DISCONNECT_KAFKA_CLUSTER_413, Util.ERR_MSG_413);
        if (StringUtils.isEmpty(pattern)) {
            UtilPages pageInfo = new UtilPages();
            pageInfo.getInitialPages(pageInfo, (List) topicList, pn);
            //System.out.println(topicList.toString());
            return Message.success().addContent(Util.KAFKA_RESULT, pageInfo);
        } else {
            //System.out.println("pattern:"+pattern);
            List<String> topicPartList = topicService.getClickSearchedTopic(topicList, pattern);
            UtilPages pagePartInfo = new UtilPages();
            pagePartInfo.getInitialPages(pagePartInfo, (List) topicPartList, pn);
            return Message.success().addContent(Util.KAFKA_RESULT, pagePartInfo);
        }

    }

    /**
     * 搜索框查询逻辑
     *
     * @param clusterName 传入集群名称
     * @param pattern     模糊匹配的topic名字字符
     * @return 返回Message对象
     */
    @RequestMapping("/clickSearch")
    @ResponseBody
    public Message clickSearch(@RequestParam("clusterName") String clusterName, @RequestParam("pattern") String pattern) {
        clusterID = clusterName;
        if (tryToGetTopic(clusterName))
            return Message.fail(Util.ERR_DISCONNECT_KAFKA_CLUSTER_413, Util.ERR_MSG_413);
        List<String> searchedList = topicService.getClickSearchedTopic(topicList, pattern);
        if (CollectionUtils.isEmpty(searchedList)) {
            return Message.fail(Util.ERR_DATA_EMPTY_403, Util.ERR_MSG_403);
        }
        UtilPages pageInfo = new UtilPages();
        pageInfo.getInitialPages(pageInfo, (List) searchedList, Util.FIRST_PAGE);
        return Message.success().addContent(Util.KAFKA_RESULT, pageInfo);
    }

    /**
     * 检查是否能连接上kafka，并拿出topic列表
     *
     * @param clusterName 传入集群名称
     * @return 返回Message对象
     */
    private boolean tryToGetTopic(@RequestParam("clusterName") String clusterName) {
        topicSet = topicService.getAllTopics(clustersMapper.queryCluster(clusterName));
        if (CollectionUtils.isEmpty(topicSet)) {
            System.out.println("[" + this.getClass().getName() + "]" + Message.fail(Util.ERR_DISCONNECT_KAFKA_CLUSTER_413, Util.ERR_MSG_413).getMessage());
            return true;
        }
        topicList = topicSet.stream().sorted(Comparator.naturalOrder()).collect(Collectors.toList());
        return false;
    }


    /**
     * 查询指定topic然后跳转到该topic的详细信息页面
     *
     * @return 返回Message对象
     */
    @GetMapping("/toTopicDescriptionPage")
    public String toTopicDescriptionPage() {
        return GlobalPropoties.TOPIC_DESCRIPTION_PAGE;
    }

    /**
     * 跳转到添加topic的页面
     *
     * @return 返回Message对象
     */
    @RequestMapping("/toAddATopicPage")
    public String toAddATopicPage() {
        return GlobalPropoties.TOPIC_ADD_PAGE;
    }


    /**
     * 添加一个topic
     *
     * @param topic 传入的topic对象
     * @return 返回Message对象
     */
    @RequestMapping("/addATopic")
    @ResponseBody
    public Message addATopic(Topic topic) {
        System.out.println(topic.toString());
        Message message = topicService.addATopic(topic);
        if (Util.SUC_OPERATION_200.equals(message.getCode())) {
            message = refreshTopicList(message);
            topicList.add(topic.getTopicName());
            return message;
        }
        return message;
    }


    @RequestMapping("/addMultipleTopics")
    @ResponseBody
    public Message addMultipleTopics(Topic topic) {
        System.out.println(topic.toString());
        if (topic.getTopicName().isEmpty()) {
            return Message.fail(Util.ERR_DATA_EMPTY_403, Util.ERR_MSG_403);
        }
        if (Objects.isNull(topic.getTopicPartition())) {
            return Message.fail(Util.ERR_DATA_EMPTY_403, Util.ERR_MSG_403);
        }
        if (Objects.isNull(topic.getTopicReplicationFactor())) {
            return Message.fail(Util.ERR_DATA_EMPTY_403, Util.ERR_MSG_403);
        }
        Message message = topicService.addMultipleTopics(topic, topicList);
        if (Util.SUC_OPERATION_200.equals(message.getCode())) {
            message = refreshTopicList(message);
            topicList.addAll((ArrayList<String>) message.getExtend().get(Util.TOPIC_RESULT));
            return message;
        } else if (Util.ERR_CODE_601.equals(message.getCode())) {
            return message;
        } else if (Util.ERR_CODE_600.equals(message.getCode())) {
            return message;
        } else {
            Util.unknowErrorAlert(this.getClass().getSimpleName());
            return Message.fail(Util.ERR_CODE_400, Util.ERR_MSG_400);
        }
    }

    private Message refreshTopicList(Message message) {
        if (tryToGetTopic(clusterID))
            return Message.fail(Util.ERR_DISCONNECT_KAFKA_CLUSTER_413, Util.ERR_MSG_413);
        return message;
    }

    /**
     * 删除单个topic
     *
     * @param topicName 传入的topic名称
     * @return 返回Message对象
     */
    @RequestMapping(value = "/deleteATopic", method = RequestMethod.DELETE)
    @ResponseBody
    public Message deleteATopic(@RequestParam("topicName") String topicName) throws ExecutionException, InterruptedException {
        //System.out.println(topicName);
        Message message = topicService.deleteATopic(topicName, topicList);
        if (Util.SUC_OPERATION_200.equals(message.getCode())) {
            message = refreshTopicList(message);
            topicList.remove(topicName);
            return message.addContent(GlobalPropoties.TOPIC_COUNT, topicList.size());
        } else if (Util.ERR_CODE_600.equals(message.getCode())) {
            return message;
        } else if (Util.ERR_CODE_602.equals(message.getCode())) {
            return message;
        } else {
            Util.unknowErrorAlert(this.getClass().getSimpleName());
            return Message.fail(Util.ERR_CODE_400, Util.ERR_MSG_400);
        }
    }

    /**
     * 删除多个topics
     *
     * @param deleteTopicNames 传入的多个topic名称
     * @return 返回Message对象
     */
    @ResponseBody
    @RequestMapping("/deleteMultipleTopics")
    public Message deleteMultipleTopics(@RequestParam("deleteTopicNames") String deleteTopicNames) throws ExecutionException, InterruptedException {
        System.out.println(deleteTopicNames);
        if (deleteTopicNames.isEmpty()) {
            return Message.status(Util.ERR_CODE_603, Util.ERR_MSG_603);
        }
        Message message = topicService.deleteMultipleTopic(deleteTopicNames, topicList);
        if (Util.SUC_OPERATION_200.equals(message.getCode())) {
            message = refreshTopicList(message);
            topicList.removeAll(Arrays.asList(Util.getStringList(deleteTopicNames)));
            System.out.println("After Multiple Delete Operation"+topicList.size());
            return message.addContent(GlobalPropoties.TOPIC_COUNT, topicList.size());
        } else if (Util.ERR_CODE_604.equals(message.getCode())) {
            return Message.fail(Util.ERR_CODE_604, Util.ERR_MSG_604).addContent(Util.ERR_EXD_604, message.getExtend().get(Util.ERR_EXD_604));
        } else {
            return Message.fail(Util.ERR_CODE_600, Util.ERR_MSG_600);
        }
    }


    /**
     * 跳转到添加topic的页面
     *
     * @return 返回String对象
     */
    @RequestMapping("/toAddMultipleTopicPage")
    public String toAddMultipleTopicPage() {
        return GlobalPropoties.TOPIC_MULTIPLE_ADD_PAGE;
    }

    /**
     * 跳转到更改Topic配置的页面
     *
     * @return 返回String对象
     */
    @GetMapping("/toTopicEditPage")
    public String toTopicEditPage() {
        return GlobalPropoties.TOPIC_EDIT_PAGE;
    }


}


