package com.kafka.easykafka.controller;

import java.util.Objects;
import java.util.concurrent.ExecutionException;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kafka.easykafka.exception.GlobalExceptionMessage;
import com.kafka.easykafka.model.Message;
import com.kafka.easykafka.service.TopicService;
import com.kafka.easykafka.util.GlobalPropoties;
import com.kafka.easykafka.util.Util;

@Controller
@RequestMapping("/topicDescription")
public class TopicDescriptionController {

    @Resource
    TopicService topicService;

    @GetMapping("/getTopicDescription")
    @ResponseBody
    public Message getTopicDescription(@RequestParam("topicName")String topicName) throws ExecutionException, InterruptedException {

        if (Objects.nonNull(topicName) | topicName.equals("")){
            return topicService.getTopicDescription(topicName);
        }else {
            return Message.fail(Util.ERR_TOPIC_NAME_414, Util.ERR_MSG_414);
        }
    }


    @GetMapping("/getTopicAuthorizationDescription")
    @ResponseBody
    public Message getTopicAuthorizationDescription(@RequestParam("topicName")String topicName) throws ExecutionException, InterruptedException {
        if (Objects.nonNull(topicName) | topicName.equals("")){
            return topicService.getTopicAuthorizationDescription(topicName);
        }else {
            return Message.fail(Util.ERR_TOPIC_NAME_414, Util.ERR_MSG_414);
        }
    }

    @ExceptionHandler({Exception.class,ExecutionException.class,InterruptedException.class})
    @ResponseBody
    public Message TopicDescriptionControllerExceptionHandler(Exception e){
        return Message.fail(Util.ERR_CODE_400, Util.ERR_MSG_400).addContent(GlobalPropoties.EXCEPTION_FLAG,
                new GlobalExceptionMessage(
                        e.getStackTrace()[0].getClassName(),
                        e.getStackTrace()[0].getMethodName(),
                        e.getStackTrace()[0].getLineNumber(),
                        e.getMessage()));
    }
}
