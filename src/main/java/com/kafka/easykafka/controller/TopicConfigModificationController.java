package com.kafka.easykafka.controller;


import java.util.Objects;
import java.util.concurrent.ExecutionException;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kafka.easykafka.model.Message;
import com.kafka.easykafka.model.TopicConfiguration;
import com.kafka.easykafka.service.TopicConfigOperationService;
import com.kafka.easykafka.util.GlobalPropoties;
import com.kafka.easykafka.util.Util;

@Controller
@RequestMapping("/topicConfiguration")
public class TopicConfigModificationController {

    @Resource
    private TopicConfigOperationService topicConfigOperationService;

    @GetMapping("/getTopicConfigurations")
    @ResponseBody
    public Message getTopicConfigurations(String topicName) throws ExecutionException, InterruptedException {
        TopicConfiguration config = topicConfigOperationService.getTopicConfigurations(topicName);
        System.out.println(config.toString());
        if (Objects.isNull(config)){
            return Message.fail(Util.ERR_CODE_400,Util.ERR_MSG_400);
        }
        
        return Message.success().addContent(GlobalPropoties.TOPIC_CONFIG,config);
    }

    
    @PostMapping("/setTopicConfigurations")
    @ResponseBody
    public Message setTopicConfigurations(TopicConfiguration topicConfiguration, @RequestParam("topicName")String topicName) throws InterruptedException, ExecutionException {
//    	System.out.println(topicConfiguration.toString());
//    	System.out.println(topicName);
    	if(topicConfigOperationService.setTopicConfigurations(topicName,topicConfiguration)) {
    		return Message.success();
    	}
    	return Message.fail(Util.ERR_CODE_400,Util.ERR_MSG_400);
	}

}
