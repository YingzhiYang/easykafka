package com.kafka.easykafka.controller;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kafka.easykafka.model.Cluster;
import com.kafka.easykafka.model.Message;
import com.kafka.easykafka.model.ZookeeperHostInfo;
import com.kafka.easykafka.service.ClustersService;
import com.kafka.easykafka.util.Util;
import com.kafka.easykafka.util.ZkServerOperation;

@Controller
@RequestMapping("/clusters")
public class ClustersController {

    private final ClustersService clustersService;

    @Autowired
    public ClustersController(ClustersService clustersService){
        this.clustersService = clustersService;
    }

    /**
     * 添加一个集群
     * @param zkIps Zookeeper Url/连接地址
     * @param kfIps Zookeeper Url/连接地址
     * @param clusterName   kafka集群名称
     * @param supName   用户名字
     * @param supPwd    用户密码
     * @param securityProtocol  安全协议
     * @param sslLocation   安全地址 jks文件地址
     * @param saslMechanism 安全机制 SCRAM-SHA-512
     * @param sslPwd    安全机制密码
     * @param zkNodePath    zookeeper节点名称
     * @param zkConfigPath  zookeeper配置地点
     * @return  Message类对象
     */
    @RequestMapping("/addCluster")
    @ResponseBody
    public Message addBroker(@RequestParam("zkIps")String zkIps, @RequestParam("kfIps")String kfIps,
                             @RequestParam("clusterName")String clusterName, @RequestParam("supName")String supName,
                             @RequestParam("supPwd")String supPwd, @RequestParam("securityProtocol")String securityProtocol,
                             @RequestParam("sslLocation")String sslLocation, @RequestParam("saslMechanism")String saslMechanism,
                             @RequestParam("sslPwd")String sslPwd,@RequestParam("zkNodePath")String zkNodePath,
                             @RequestParam("zkConfigPath")String zkConfigPath){

        System.out.println(zkIps+"|"+kfIps+"|"+clusterName+"|"+supName+"|"+supPwd+"|"+securityProtocol+"|"+sslLocation+"|"+sslPwd+"|"+saslMechanism+"|");

        if(!clustersService.getClusterInstance(kfIps,supName,supPwd,securityProtocol,sslLocation,sslPwd,saslMechanism)){
            return Message.fail(Util.ERR_DISCONNECT_KAFKA_CLUSTER_413,Util.ERR_MSG_413);
        }
        Cluster cluster=new Cluster();
        cluster.setClusterName(clusterName);
        cluster.setZkIps(zkIps);
        cluster.setKfkIps(kfIps);
        cluster.setSupUser(supName);
        cluster.setSupPwd(supPwd);
        cluster.setSecurityProtocol(securityProtocol);
        cluster.setJksPath(sslLocation);
        cluster.setSslPws(sslPwd);
        cluster.setSaslMechanism(saslMechanism);
        cluster.setZkNodePath(zkNodePath);
        cluster.setZkConfigPath(zkConfigPath);
        ZookeeperHostInfo zookeeperHostInfo= ZkServerOperation.analyzingIps(zkConfigPath);
        if (Objects.nonNull(zookeeperHostInfo)){
            cluster.setZkPort(zookeeperHostInfo.getClientPort());
        }else {
            return Message.fail(Util.ERR_DISCONNECT_ZK_CLUSTER_417,Util.ERR_MSG_417);
        }
        cluster.setZkIpsNotPort(Util.generateStringFromList(zookeeperHostInfo.getIpsList()));
        //cluster.setKfkIpsNotPort(Util.generateStringFromList(clustersService.findAllClustersFromKafkaServer()));
        if (1== clustersService.addCluster(cluster)){
            System.out.println(Message.success().getMessage());
            return Message.success();
        }else{
            System.out.println("["+this.getClass().getName()+"]"+Message.fail(Util.FAIL_INSERT_301,Util.FAIL_MSG_301).getMessage());
            return Message.fail(Util.FAIL_INSERT_301,Util.FAIL_MSG_301);
        }
    }

    /**
     * 查找到所有的集群名称
     * @return 返回Message对象
     */
    @RequestMapping("/findAllClusterName")
    @ResponseBody
    public Message findAllClusterName(){
        try {
            List<Map<String,Object>> result= clustersService.findAllClusterName();
            if (!Objects.nonNull(result)){
                System.out.println("["+this.getClass().getName()+"]"+Message.fail(Util.FAIL_QUERY_303,Util.FAIL_MSG_303).getMessage());
                return Message.fail(Util.FAIL_QUERY_303,Util.FAIL_MSG_303);
            }
            return Message.success().addContent(Util.QUERY_RESULT,result);
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("["+this.getClass().getName()+"]"+Message.fail(Util.ERR_CODE_400,Util.ERR_MSG_400).getMessage());
            return Message.fail(Util.ERR_CODE_400,Util.ERR_MSG_400);
        }
    }
}
