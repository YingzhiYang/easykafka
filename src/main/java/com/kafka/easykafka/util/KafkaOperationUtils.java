package com.kafka.easykafka.util;


import com.kafka.easykafka.model.AuthorizationToAccount;
import com.kafka.easykafka.model.EKTopicAuthorization;
import com.kafka.easykafka.model.Group;
import com.kafka.easykafka.model.TopicConfiguration;
import com.kafka.easykafka.sysEnum.AuthorizationEnum;
import org.apache.kafka.clients.admin.*;
import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.acl.*;
import org.apache.kafka.common.config.ConfigResource;
import org.apache.kafka.common.resource.PatternType;
import org.apache.kafka.common.resource.ResourcePattern;
import org.apache.kafka.common.resource.ResourcePatternFilter;
import org.apache.kafka.common.resource.ResourceType;

import java.util.*;
import java.util.concurrent.ExecutionException;

public class KafkaOperationUtils {
    /**
     * 给多个topic添加账户权限
     * @param authorizationToAccount 授权列表
     * @param topics topic列表
     * @return CreateAclsResult
     */
    public static CreateAclsResult addAuthorizationToAccount(AuthorizationToAccount authorizationToAccount, String[] topics){
        Collection<AclBinding> aclBindingCollection = new ArrayList<>();
        for (String topic : topics) {
            ResourcePattern resourcePattern = new ResourcePattern(ResourceType.TOPIC, topic, PatternType.LITERAL);
            AccessControlEntry accessControlEntry = chooseAuth(authorizationToAccount);
            AclBinding aclBinding = new AclBinding(resourcePattern, accessControlEntry);
            aclBindingCollection.add(aclBinding);
        }
        return KafkaConnection.getAdminClient().createAcls(aclBindingCollection);
    }

    /**
     * 选择要添加的权限
     * @param authorizationToAccount 授权列表
     * @return AccessControlEntry
     */
    private static AccessControlEntry chooseAuth(AuthorizationToAccount authorizationToAccount) {
        AccessControlEntry accessControlEntry=null;
        switch (AuthorizationEnum.valueOf(authorizationToAccount.getAuthorization())) {
            case All:
                accessControlEntry = new AccessControlEntry("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.ALL, AclPermissionType.ALLOW);
                break;
            case Read:
                accessControlEntry = new AccessControlEntry("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.READ, AclPermissionType.ALLOW);
                break;
            case Write:
                accessControlEntry = new AccessControlEntry("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.WRITE, AclPermissionType.ALLOW);
                break;
            case Create:
                accessControlEntry = new AccessControlEntry("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.CREATE, AclPermissionType.ALLOW);
                break;
            case Delete:
                accessControlEntry = new AccessControlEntry("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.DELETE, AclPermissionType.ALLOW);
                break;
            case Alter:
                accessControlEntry = new AccessControlEntry("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.ALTER, AclPermissionType.ALLOW);
                break;
            case Describe:
                accessControlEntry = new AccessControlEntry("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.DESCRIBE, AclPermissionType.ALLOW);
                break;
            case Clusteraction:
                accessControlEntry = new AccessControlEntry("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.CLUSTER_ACTION, AclPermissionType.ALLOW);
                break;
        }
        return accessControlEntry;
    }

    /**
     * 给多个topic移除账户权限
     * @param authorizationToAccount  authorizationToAccount 授权列表
     * @param topics topic列表
     * @return DeleteAclsResult
     */
    public static DeleteAclsResult removeAuthorizationFromAccount(AuthorizationToAccount authorizationToAccount, String[] topics){
        Collection<AclBindingFilter> aclBindingFilterCollection = new ArrayList<>();
        for (String topic : topics) {
            ResourcePatternFilter resourcePatternFilter = new ResourcePatternFilter(ResourceType.TOPIC, topic, PatternType.LITERAL);
            AccessControlEntryFilter accessControlEntryFilter = chooseAuthFilter(authorizationToAccount);
            AclBindingFilter aclBinding = new AclBindingFilter(resourcePatternFilter, accessControlEntryFilter);
            aclBindingFilterCollection.add(aclBinding);
        }
        return KafkaConnection.getAdminClient().deleteAcls(aclBindingFilterCollection);
    }

    /**
     * 选择要移除的权限
     * @param authorizationToAccount 授权列表
     * @return AccessControlEntry
     */
    private static AccessControlEntryFilter chooseAuthFilter(AuthorizationToAccount authorizationToAccount) {
        AccessControlEntryFilter accessControlEntryFilter = null;
        switch (AuthorizationEnum.valueOf(authorizationToAccount.getAuthorization())) {
            case All:
                accessControlEntryFilter = new AccessControlEntryFilter("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.ALL, AclPermissionType.ALLOW);
                break;
            case Read:
                accessControlEntryFilter = new AccessControlEntryFilter("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.READ, AclPermissionType.ALLOW);
                break;
            case Write:
                accessControlEntryFilter = new AccessControlEntryFilter("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.WRITE, AclPermissionType.ALLOW);
                break;
            case Create:
                accessControlEntryFilter = new AccessControlEntryFilter("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.CREATE, AclPermissionType.ALLOW);
                break;
            case Delete:
                accessControlEntryFilter = new AccessControlEntryFilter("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.DELETE, AclPermissionType.ALLOW);
                break;
            case Alter:
                accessControlEntryFilter = new AccessControlEntryFilter("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.ALTER, AclPermissionType.ALLOW);
                break;
            case Describe:
                accessControlEntryFilter = new AccessControlEntryFilter("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.DESCRIBE, AclPermissionType.ALLOW);
                break;
            case Clusteraction:
                accessControlEntryFilter = new AccessControlEntryFilter("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.CLUSTER_ACTION, AclPermissionType.ALLOW);
                break;
        }
        return accessControlEntryFilter;
    }

    /**
     * 给传入的account赋予一个group
     * @param group Group#groupId,#accountName
     * @return CreateAclsResult
     */
    public static CreateAclsResult addGroupIdToAccount(Group group){
        ResourcePattern resourcePattern = new ResourcePattern(ResourceType.GROUP, group.getGroupId(), PatternType.LITERAL);
        AccessControlEntry accessControlEntry = new AccessControlEntry("User:" + group.getAccountName(), "*", AclOperation.ALL, AclPermissionType.ALLOW);
        AclBinding aclBinding = new AclBinding(resourcePattern, accessControlEntry);
        Collection<AclBinding> aclBindingCollection = new ArrayList<>();
        aclBindingCollection.add(aclBinding);
        return KafkaConnection.getAdminClient().createAcls(aclBindingCollection);
    }


    /**
     * 给传入的account移除一个group
     * @param group Group#groupId,#accountName
     * @return CreateAclsResult
     */
    public static DeleteAclsResult removeGroupIdFromAccount(Group group){
        ResourcePatternFilter resourcePatternFilter = new ResourcePatternFilter(ResourceType.GROUP, group.getGroupId(), PatternType.LITERAL);
        AccessControlEntryFilter accessControlEntry = new AccessControlEntryFilter("User:" + group.getAccountName(), "*", AclOperation.ALL, AclPermissionType.ALLOW);
        AclBindingFilter aclBindingFilter = new AclBindingFilter(resourcePatternFilter, accessControlEntry);
        Collection<AclBindingFilter> aclBindingCollection = new ArrayList<>();
        aclBindingCollection.add(aclBindingFilter);
        return KafkaConnection.getAdminClient().deleteAcls(aclBindingCollection);
    }

    public static ArrayList<EKTopicAuthorization> getAllAclDescription() throws ExecutionException, InterruptedException {
       return getEkTopicAuthorizations(KafkaConnection.getAdminClient().describeAcls(AclBindingFilter.ANY));
    }


    /**
     * 根据账户名称获取该账户的所有权限信息
     * @param account 输入账户
     * @return ArrayList<EKTopicAuthorization>
     * @throws ExecutionException ExecutionException
     * @throws InterruptedException ExecutionException
     */
    public static ArrayList<EKTopicAuthorization> getSingleAclDescription(String account) throws ExecutionException, InterruptedException {
        AccessControlEntryFilter accessControlEntryFilter=new AccessControlEntryFilter("User:"+account,"*", AclOperation.ANY, AclPermissionType.ANY);
        AclBindingFilter aclBindingFilter=new AclBindingFilter(ResourcePatternFilter.ANY,accessControlEntryFilter);
        return getEkTopicAuthorizations(KafkaConnection.getAdminClient().describeAcls(aclBindingFilter));
    }


    /**
     * 获取Kafka中查询的信息结果
     * @param result 返回查询结果
     * @return ArrayList<EKTopicAuthorization>
     * @throws InterruptedException InterruptedException
     * @throws ExecutionException ExecutionException
     */
    private static ArrayList<EKTopicAuthorization> getEkTopicAuthorizations(DescribeAclsResult result) throws InterruptedException, ExecutionException {
        Collection<AclBinding> gets = result.values().get();
        ArrayList<EKTopicAuthorization> topicAuthorization=new ArrayList<>();
        for (AclBinding get : gets) {
            EKTopicAuthorization authorization=new EKTopicAuthorization(
                    get.pattern().name(),
                    get.pattern().patternType().toString(),
                    get.pattern().resourceType().toString(),
                    get.entry().principal(),
                    get.entry().permissionType().toString(),
                    get.entry().operation().toString()
            );
            topicAuthorization.add(authorization);
        }
        return topicAuthorization;
    }

    /**
     * 删除所有目标账户accountName的权限
     * @param accountName 目标账户
     * @param targetName 任何权限，可以是topic name，或者group name
     * @return boolean，返回是否成功
     * @throws ExecutionException ExecutionException
     * @throws InterruptedException ExecutionException
     */
    public static boolean removeAllAuthorizationFromAccount(String accountName, String targetName) throws ExecutionException, InterruptedException {
        Collection<AclBindingFilter> aclBindingFilterCollection = new ArrayList<>();
        ResourcePatternFilter resourcePatternFilter = new ResourcePatternFilter(ResourceType.ANY, targetName, PatternType.ANY);
        AccessControlEntryFilter accessControlEntryFilter = new AccessControlEntryFilter("User:" + accountName, "*", AclOperation.ANY, AclPermissionType.ANY);;
        AclBindingFilter aclBindingFilter = new AclBindingFilter(resourcePatternFilter, accessControlEntryFilter);
        aclBindingFilterCollection.add(aclBindingFilter);
        KafkaFuture<Collection<AclBinding>> result = KafkaConnection.getAdminClient().deleteAcls(aclBindingFilterCollection).all();
        result.get();
        return result.isDone();
    }

    /**
     * 修改当前topic的属性信息
     * @param topicName 当前topicName
     * @param configMap 属性信息
     * @return boolean
     * @throws ExecutionException ExecutionException
     * @throws InterruptedException InterruptedException
     */
    public static boolean alterTopicConfiguration(String topicName, Map<String,String> configMap) throws ExecutionException, InterruptedException {
        Map<ConfigResource, Collection<AlterConfigOp>> configs =new HashMap<>();
        ConfigResource configResource=new ConfigResource(ConfigResource.Type.TOPIC,topicName);
        Collection<AlterConfigOp> configOps=new ArrayList<>();
        for (String config :configMap.keySet()) {
            configOps.add(new AlterConfigOp(new ConfigEntry(config, configMap.get(config)),AlterConfigOp.OpType.SET));
        }
        configs.put(configResource,configOps);
        AlterConfigsResult result = KafkaConnection.getAdminClient().incrementalAlterConfigs(configs);
        result.all().get();
        return result.all().isDone();
    }

    /**
     * 获取指定topic的分区数
     * @param topicName topicName
     * @return Integer
     */
    public static Integer getTopicPartitions(String topicName) throws ExecutionException, InterruptedException {
        List<String> list = new ArrayList<String>();
        list.add(topicName);
        DescribeTopicsResult topic = KafkaConnection.getAdminClient().describeTopics(list);
        return topic.all().get().get(topicName).partitions().size();
    }

    /**
     * 获取指定topic的配置参数
     * @param topicName topicName
     * @return 返回对应的参数信息map
     * @throws ExecutionException ExecutionException
     * @throws InterruptedException InterruptedException
     */
    public static Map<String, String> getTopicConfiguration(String topicName) throws ExecutionException, InterruptedException {

        Collection<ConfigResource> configResources=new ArrayList<>();
        configResources.add(new ConfigResource(ConfigResource.Type.TOPIC,topicName));
        DescribeConfigsResult result = KafkaConnection.getAdminClient().describeConfigs(configResources);
        result.all().get();
        Collection<KafkaFuture<Config>> list = result.values().values();
        Map<String, String> returnMap=new HashMap<>();
        for (KafkaFuture<Config> future: list) {
            for (ConfigEntry ce:future.get().entries()){
                //System.out.println(ce.name()+":"+ce.value()+"; isDefault:"+ce.isDefault());
            	//这里只收集非默认的参数，如果是默认的，就不再显示
            	if(!ce.isDefault()) {
            		returnMap.put(ce.name(),ce.value());
            	}
            }
        }
        if (result.all().isDone()){
            return returnMap;
        }else
            return null;
    }
    
    /**
     * 增加指定topic的partition
     * @param topicName topicName
     * @param partitions 要修改的partition
     * @return boolean
     * @throws InterruptedException InterruptedException
     * @throws ExecutionException ExecutionException
     */
    public static boolean increasePartitions(String topicName, Integer partitions) throws InterruptedException, ExecutionException {
    	Map<String, NewPartitions> partMap= new HashMap<>();
    	partMap.put(topicName, NewPartitions.increaseTo(partitions));
    	CreatePartitionsResult result = KafkaConnection.getAdminClient().createPartitions(partMap);
    	result.all().get();
    	return result.all().isDone();
    }
    
    
    /**
     * 设置指定Topic的配置参数
     * @param topicName
     * @param topicConfiguration
     * @return
     * @throws ExecutionException 
     * @throws InterruptedException 
     */
    public static boolean setTopicConfigurations(String topicName,TopicConfiguration topicConfiguration) throws InterruptedException, ExecutionException {
    	Map<ConfigResource, Collection<AlterConfigOp>> configs=new HashMap<>();
    	ConfigResource configResource=new ConfigResource(ConfigResource.Type.TOPIC, topicName);
    	Collection<AlterConfigOp> configOps = generateTopicConfigurations(topicConfiguration);
    	configs.put(configResource, configOps);
    	AlterConfigsResult result = KafkaConnection.getAdminClient().incrementalAlterConfigs(configs);
    	result.all().get();
    	return result.all().isDone();
	}
    
    
    /**
     * 生成必要的配置参数列表
     * @param configs
     * @return
     */
    private static Collection<AlterConfigOp> generateTopicConfigurations(TopicConfiguration configs) {
    	Collection<AlterConfigOp> configOps = new ArrayList<>();
    	checkNullParameter(configs.getRetentionMs(),configOps,GlobalPropoties.RETENTION_MS,configs.getRetentionMs());
    	checkNullParameter(configs.getMaxMessageBytes(),configOps,GlobalPropoties.MAX_MESSAGE_BYTES,configs.getMaxMessageBytes());
    	checkNullParameter(configs.getSegmentIndexBytes(),configOps,GlobalPropoties.SEGMENT_INDEX_BYTES,configs.getSegmentIndexBytes());
    	checkNullParameter(configs.getSegmentBytes(),configOps,GlobalPropoties.SEGMENT_BYTES,configs.getSegmentBytes());
    	checkNullParameter(configs.getMinCleanableDirtyRatio(),configOps,GlobalPropoties.MIN_CLEANABLE_DIRTY_RATIO,configs.getMinCleanableDirtyRatio());
    	checkNullParameter(configs.getMinInsyncReplicas(),configOps,GlobalPropoties.MIN_INSYNC_REPLICAS,configs.getMinInsyncReplicas());
    	checkNullParameter(configs.getDeleteRetentionMs(),configOps,GlobalPropoties.DELETE_RETENTION_MS,configs.getDeleteRetentionMs());
    	checkNullParameter(configs.getFlushMessages(),configOps,GlobalPropoties.FLUSH_MESSAGES,configs.getFlushMessages());
    	checkNullParameter(configs.getPreallocate(),configOps,GlobalPropoties.PREALLOCATE,configs.getPreallocate());
    	checkNullParameter(configs.getRetentionBytes(),configOps,GlobalPropoties.RETENTION_BYTES,configs.getRetentionBytes());
    	checkNullParameter(configs.getFlushMs(),configOps,GlobalPropoties.FLUSH_MS,configs.getFlushMs());
    	checkNullParameter(configs.getCleanupPolicy(),configOps,GlobalPropoties.CLEANUP_POLICY,configs.getCleanupPolicy());
    	checkNullParameter(configs.getFileDeleteDelayMs(),configOps,GlobalPropoties.FILE_DELETE_DELAY_MS,configs.getFileDeleteDelayMs());
    	checkNullParameter(configs.getSegmentJitterMs(),configOps,GlobalPropoties.SEGMENT_JITTER_MS,configs.getSegmentJitterMs());
    	checkNullParameter(configs.getIndexIntervalBytes(),configOps,GlobalPropoties.INDEX_INTERVAL_BYTES,configs.getIndexIntervalBytes());
    	checkNullParameter(configs.getCompressionType(),configOps,GlobalPropoties.COMPRESSION_TYPE,configs.getCompressionType());
    	checkNullParameter(configs.getSegmentMs(),configOps,GlobalPropoties.SEGMENT_MS,configs.getSegmentMs());
    	checkNullParameter(configs.getUncleanLeaderElectionEnable(),configOps,GlobalPropoties.UNCLEAN_LEADER_ELECTION_ENABLE,configs.getUncleanLeaderElectionEnable());
		return configOps;
		
		/*if (Util.checkNotNullAndEmpty(configs.getRetentionMs())) {
		setParameter(configOps,GlobalPropoties.RETENTION_MS,configs.getRetentionMs().toString());
	}
	if (Util.checkNotNullAndEmpty(configs.getMaxMessageBytes())) {
		setParameter(configOps,GlobalPropoties.MAX_MESSAGE_BYTES,configs.getMaxMessageBytes().toString());
	}
	if (Util.checkNotNullAndEmpty(configs.getSegmentIndexBytes())) {
		setParameter(configOps,GlobalPropoties.SEGMENT_INDEX_BYTES,configs.getSegmentIndexBytes().toString());
	}
	if (Util.checkNotNullAndEmpty(configs.getSegmentBytes())) {
		setParameter(configOps,GlobalPropoties.SEGMENT_BYTES,configs.getSegmentBytes().toString());
	}
	if (Util.checkNotNullAndEmpty(configs.getMinCleanableDirtyRatio())) {
		setParameter(configOps,GlobalPropoties.MIN_CLEANABLE_DIRTY_RATIO,configs.getMinCleanableDirtyRatio().toString());
	}
	if (Util.checkNotNullAndEmpty(configs.getMinInsyncReplicas())) {
		setParameter(configOps,GlobalPropoties.MIN_INSYNC_REPLICAS,configs.getMinInsyncReplicas().toString());
	}
	if (Util.checkNotNullAndEmpty(configs.getDeleteRetentionMs())) {
		setParameter(configOps,GlobalPropoties.DELETE_RETENTION_MS,configs.getDeleteRetentionMs().toString());
	}
	if (Util.checkNotNullAndEmpty(configs.getFlushMessages())) {
		setParameter(configOps,GlobalPropoties.FLUSH_MESSAGES,configs.getFlushMessages().toString());
	}
	if (Util.checkNotNullAndEmpty(configs.getPreallocate())) {
		setParameter(configOps,GlobalPropoties.PREALLOCATE,configs.getPreallocate().toString());
	}
	if (Util.checkNotNullAndEmpty(configs.getRetentionBytes())) {
		setParameter(configOps,GlobalPropoties.RETENTION_BYTES,configs.getRetentionBytes().toString());
	}
	if (Util.checkNotNullAndEmpty(configs.getFlushMs())) {
		setParameter(configOps,GlobalPropoties.FLUSH_MS,configs.getFlushMs().toString());
	}
	if (Util.checkNotNullAndEmpty(configs.getCleanupPolicy())) {
		setParameter(configOps,GlobalPropoties.CLEANUP_POLICY,configs.getCleanupPolicy().toString());
	}
	if (Util.checkNotNullAndEmpty(configs.getFileDeleteDelayMs())) {
		setParameter(configOps,GlobalPropoties.FILE_DELETE_DELAY_MS,configs.getFileDeleteDelayMs().toString());
	}
	if (Util.checkNotNullAndEmpty(configs.getSegmentJitterMs())) {
		setParameter(configOps,GlobalPropoties.SEGMENT_JITTER_MS,configs.getSegmentJitterMs().toString());
	}
	if (Util.checkNotNullAndEmpty(configs.getIndexIntervalBytes())) {
		setParameter(configOps,GlobalPropoties.INDEX_INTERVAL_BYTES,configs.getIndexIntervalBytes().toString());
	}
	if (Util.checkNotNullAndEmpty(configs.getCompressionType())) {
		setParameter(configOps,GlobalPropoties.COMPRESSION_TYPE,configs.getCompressionType().toString());
	}
	if (Util.checkNotNullAndEmpty(configs.getSegmentMs())) {
		setParameter(configOps,GlobalPropoties.SEGMENT_MS,configs.getSegmentMs().toString());
	}
	if (Util.checkNotNullAndEmpty(configs.getUncleanLeaderElectionEnable())) {
		setParameter(configOps,GlobalPropoties.UNCLEAN_LEADER_ELECTION_ENABLE,configs.getUncleanLeaderElectionEnable().toString());
	}*/
	}
    
    /**
     * 设置具体的参数
     * @param configOps
     * @param param
     * @param value
     */
    private static void setParameter(Collection<AlterConfigOp> configOps, String param, String value) {
    	ConfigEntry configEntry=new ConfigEntry(param, value);
    	AlterConfigOp alterConfigOp = new AlterConfigOp(configEntry, AlterConfigOp.OpType.SET);
		configOps.add(alterConfigOp);
	}

    
    private static void checkNullParameter(Object checkContent, Collection<AlterConfigOp> configOps,  String param, Object value) {
    	if (Util.checkNotNullAndEmpty(checkContent)) {
    		setParameter(configOps,param,value.toString());
    	}
	}
    

}
