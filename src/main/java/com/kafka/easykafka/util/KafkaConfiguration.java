package com.kafka.easykafka.util;

import java.util.Objects;
import java.util.Properties;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.springframework.stereotype.Repository;

@Repository
@Deprecated
public class KafkaConfiguration {


    private static KafkaConfiguration kafkaConfiguration = new KafkaConfiguration();
    private AdminClient adminClient;

    public KafkaConfiguration() {
    }

    /**
     * 单例模式获取kafka管理类实例adminClient
     *
     * @param kafkaUrl broker ip
     * @param securityProtocol securityProtocol
     * @param sslLocation sslLocation
     * @param sslPwd sslPwd
     * @param saslMechanism saslMechanism
     * @param saslJaasConfig saslJaasConfig
     * @return KafkaConfiguration
     */
    public KafkaConfiguration getKafkaConfiguration(String kafkaUrl, String securityProtocol, String sslLocation,
                                                    String sslPwd, String saslMechanism, String saslJaasConfig) {
        try {
            if (!Objects.nonNull(kafkaConfiguration)) {
                kafkaConfiguration = new KafkaConfiguration();
            }
            kafkaConfiguration.setAdminClient(getAdminClient(kafkaUrl, securityProtocol, sslLocation, sslPwd, saslMechanism, saslJaasConfig));
            return kafkaConfiguration;
        } catch (Exception e) {
            return null;
        }

    }

    /**
     * 获取一个AdminClient的实例
     *
     * @param kafkaUrl         Broker的连接地址。eg. localhost:9092
     * @param securityProtocol 安全协议
     * @param sslLocation      ssl的**.jks文件地址
     * @param sslPwd           ssl文件密码
     * @param saslMechanism    sasl认证机制
     * @return
     */
    public static AdminClient getAdminClient(String kafkaUrl, String securityProtocol, String sslLocation,
                                             String sslPwd, String saslMechanism, String saslJaasConfig) {
        Properties props = new Properties();
        props.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaUrl);
        props.put(AdminClientConfig.SECURITY_PROTOCOL_CONFIG, securityProtocol);
        props.put(GlobalPropoties.SSL_LOCATION, sslLocation);
        props.put(GlobalPropoties.SSL_PASSWORD, sslPwd);
        props.put(GlobalPropoties.SASL_MECHANISM, saslMechanism);
        props.put(GlobalPropoties.SASL_JAAS_CONFIG, saslJaasConfig);
        return AdminClient.create(props);
    }

    public AdminClient getAdminClient() {
        return adminClient;
    }

    public void setAdminClient(AdminClient adminClient) {
        this.adminClient = adminClient;
    }
}
