package com.kafka.easykafka.util;

import java.util.List;
import java.util.Objects;

import com.kafka.easykafka.model.Account;
import com.kafka.easykafka.model.Message;

public class Util {

    /**
     * statue code 200:成功, 操作执行成功
     */
    public static Integer SUC_OPERATION_200 = 200;
    public static String SUC_MSG_200 = "[SUCCESS CODE 200] : Operation Successful.";

    /**
     * statue code 201:成功, 查询成功, 但是表里没有数据
     */
    public static Integer SUC_SQL_CODE_201 = 201;
    public static String SUC_MEG_201 = "[SUCCESS CODE 201] : Table is empty.";


    /**
     * statue code 201:成功, 查询成功但是结果为空
     */
    public static Integer SUC_EMPTY_202 = 202;
    public static String SUC_MEG_202 = "Cannot find any data for this search.";

    /**
     * 300:数据库端有问题
     */
    public static Integer FAIL_CODE = 300;

    /**
     * 301插入失败
     */
    public static Integer FAIL_INSERT_301 = 301;
    public static String FAIL_MSG_301 = "[FAIL CODE 301] : Insert Transaction Failed.";
    /**
     * 302更新失败
     */
    public static Integer FAIL_UPDATE_302 = 302;
    /**
     * 303查询失败
     */
    public static Integer FAIL_QUERY_303 = 303;
    public static String FAIL_MSG_303 = "[FAIL CODE 303] : Query Transaction Failed.";
    /**
     * 304删除失败
     */
    public static Integer FAIL_DELETE_304 = 304;

    /**
     * 400:服务器端有问题问题未知
     */
    public static Integer ERR_CODE_400 = 400;
    public static String ERR_MSG_400 = "[ERROR CODE 400] : System has an unknown error.";
    /**
     * 401字段验证问题
     */
    public static Integer ERR_VALIDATION_401 = 401;
    /**
     * 402字段重复
     */
    public static Integer ERR_VALIDATION_REDUPLICATION_402 = 402;
    /**
     * 403数据为空
     */
    public static Integer ERR_DATA_EMPTY_403 = 403;
    public static String ERR_MSG_403 = "Data Cannot be found.";
    /**
     * 405日期格式不正确
     */
    public static Integer ERR_DATE_FORMATE_405 = 405;
    /**
     * 406字段应该为数字格式
     */
    public static Integer ERR_DIGITAL_FORMATE_406 = 406;
    /**
     * 407文件尺寸有问题
     */
    public static Integer ERR_FILE_SIZE_407 = 407;
    /**
     * 408文件为空
     */
    public static Integer ERR_FILE_EMPTY_408 = 408;
    /**
     * 409文件上传失败
     */
    public static Integer ERR_FILE_UPLOAD_409 = 409;
    /**
     * 410文件删除错误
     */
    public static Integer ERR_FILE_DELETE_410 = 410;
    /**
     * 411文件类型错误
     */
    public static Integer ERR_FILE_TYPE_411 = 411;
    /**
     * 412登陆失败
     */
    public static Integer ERR_LOGIN_412 = 412;

    /**
     * 413 Kakfa Cluster连接失败
     */
    public static Integer ERR_DISCONNECT_KAFKA_CLUSTER_413 = 413;
    public static String ERR_MSG_413 = "[ERROR CODE 413] : Cannot to connect Kafka Cluster.";

    /**
     * 414 topic name 为空
     */
    public static Integer ERR_TOPIC_NAME_414 = 414;
    public static String ERR_MSG_414 = "[ERROR CODE 414] : Topic(s) cannot be empty.";

    /**
     * 415 account name 为空
     */
    public static Integer ERR_ACCOUNT_NAME_415 = 415;
    public static String ERR_MSG_415 = "[ERROR CODE 415] : Account Name cannot be empty.";

    /**
     * 416 Group ID 为空
     */
    public static Integer ERR_GROUP_ID_416 = 416;
    public static String ERR_MSG_416 = "[ERROR CODE 416] : Group ID cannot be empty.";


    /**
     * 413 Kakfa Cluster连接失败
     */
    public static Integer ERR_DISCONNECT_ZK_CLUSTER_417 = 417;
    public static String ERR_MSG_417 = "[ERROR CODE 417] : Cannot to connect Kafka Cluster.";

    /**
     * 600:Kafka操作有问题
     */
    public static Integer ERR_CODE_600 = 600;
    public static String ERR_MSG_HEAD_600 = "[ERROR CODE 600] : ";
    public static String ERR_MSG_600 = "[ERROR CODE 600] : There is an Unknown Error on Kafka Server.";

    /**
     * 601 Kafka topic 名字重复
     */
    public static Integer ERR_CODE_601 = 601;
    public static String ERR_MSG_601 = "[ERROR CODE 601] : Topic(s) already exists.";
    public static String ERR_EXD_601 = "existsTopics";

    /**
     * 602 Kafka topic 不存在
     */
    public static Integer ERR_CODE_602 = 602;
    public static String ERR_MSG_602 = "[ERROR CODE 602] : Topic(s) does NOT exists.";
    public static String ERR_EXD_602 = "missingTopics";

    /**
     * 603 没有选中Kafka topic
     */
    public static Integer ERR_CODE_603 = 603;
    public static String ERR_MSG_603 = "[ERROR CODE 603] : It looks you does NOT choose any topic.";

    /**
     * 604 选中Kafka topics无法删除
     */
    public static Integer ERR_CODE_604 = 604;
    public static String ERR_MSG_604 = "[ERROR CODE 604] : Some topics delete failed.";
    public static String ERR_EXD_604 = "failedTopics";

    /**
     * 605 给账户赋权操作失败
     */
    public static Integer ERR_CODE_605 = 605;
    public static String ERR_MSG_605 = "[ERROR CODE 605] : There are some errors when giving an authorization.";
    public static String ERR_EXD_605 = "failedTopics";

    /**
     * 606 给账户消费组操作失败
     */
    public static Integer ERR_CODE_606 = 606;
    public static String ERR_MSG_606 = "[ERROR CODE 606] : There are some errors when giving a group ID authorization.";
    public static String ERR_EXD_606 = "failedGroup";

    /**
     * 607 移除账户消费组操作失败
     */
    public static Integer ERR_CODE_607 = 607;
    public static String ERR_MSG_607 = "[ERROR CODE 607] : There are some errors when removing a group ID authorization.";
    public static String ERR_EXD_607 = "failedGroup";

    /**
     * 608 给账户赋权操作失败
     */
    public static Integer ERR_CODE_608 = 608;
    public static String ERR_MSG_608 = "[ERROR CODE 608] : There are some errors when removing an authorization.";
    public static String ERR_EXD_608 = "failedTopics";

    /**
     * 609 给账户赋权操作失败
     */
    public static Integer ERR_CODE_609 = 609;
    public static String ERR_MSG_609 = "[ERROR CODE 609] : There are some errors when removing an authorization.";
    public static String ERR_EXD_609 = "failedZookeeper";

    /**
     * 700 linux执行语句发生未知Error
     */
    public static Integer ERR_CODE_700 = 700;
    public static String ERR_MSG_700 = "[ERROR CODE 700] : There are some errors when execute linux shall.";

    /**
     * 700 linux执行复权语句发生Error
     */
    public static Integer ERR_CODE_701 = 701;
    public static String ERR_MSG_701 = "[ERROR CODE 701] : There are some errors when giving an authorization.";
    public static String ERR_EXD_701 = "authorizationResult";


    /**
     * -1 sql执行失败返回数据无法拿到时，用try-catch捕获，然后返回此常量
     */
    public final static int SQL_FAIL_FLAG = -1;

    /**
     * 从数据库查询出来的结果
     */
    public static String QUERY_RESULT = "queryResult";

    /**
     * 从kafka查询出来的结果
     */
    public static String KAFKA_RESULT = "kafkaResult";

    /**
     * topic操作的一般结果
     */
    public static String TOPIC_RESULT = "topicResult";

    /**
     * 数字常量初始化
     */
    public final static Integer ZERO = 0;
    public final static Integer ONE = 1;
    public final static Integer NINE = 9;
    public final static Integer TEN = 10;

    public final static String passwordAllPool = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    public final static String passwordLowercaseNumPool = "abcdefghijklmnopqrstuvwxyz0123456789";
    public final static String passwordCapitalNumPool = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    public final static String passwordLowercaseCapitalPool = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public final static String passwordNumPool = "0123456789";
    public final static String passwordLowercasePool = "abcdefghijklmnopqrstuvwxyz";
    public final static String passwordCapitalPool = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    /**
     * pageInfo常量 首页
     */
    public static Integer FIRST_PAGE = 1;
    /**
     * Topic单页大小，每页10条
     */
    public static Integer SINGLE_PAGE_SIZE_TOPIC = 10;

    /**
     * 连续显示的页数，如果算出来的总页数小于这个连续显示的页数数组会出现显示问题(已修复)
     */
    public static Integer VIEW_PAGES = 5;

    /**
     * 把带有","的字符串转换为字符串数组
     *
     * @param str 传入字符串
     * @return 返回转换后的字符数组
     */
    public static String[] getStringList(String str) {
        return str.split(",");
    }

    /**
     * 将list里面的String元素转换为字符串
     * @param list 传入list
     * @return  返回转换后的字符串
     */
    public static String generateStringFromList(List<String> list){
        StringBuilder stringBuilder=new StringBuilder();
        for (String str:list) {
            stringBuilder.append(str).append(",");
        }
        return stringBuilder.substring(0,stringBuilder.length()-1);
    }

    /**
     * 判定topic是不是存在于当前列表里
     *
     * @param topic 传入topic
     * @param topicFullList 传入topic列表
     * @return  返回判断结果
     */
    public static boolean existsTopics(String topic, List<String> topicFullList) {
        return topicFullList.contains(topic);
    }

    /**
     * 输出未知error的信息
     * @param clazzName 传入报错类名
     */
    public static void unknowErrorAlert(String clazzName) {
        System.out.println("[" + clazzName + "]" + Message.fail(Util.ERR_CODE_400, Util.ERR_MSG_400).getMessage());
    }



    public static String generateAddAccount(String shallPath,Account account) {
        return shallPath+"kafka-configs.sh --zookeeper n3.ikt.lenovo.com:2181/kafka210 --alter --add-config 'SCRAM-SHA-512=[password="+account.getPassword()+"]' --entity-type users --entity-name "+account.getAccount();
    }

    public static String generateSaslJaasConfig(String account,String pwd) {
        return "org.apache.kafka.common.security.scram.ScramLoginModule required username='"+account+"' password='"+pwd+"';";
    }
    
    /**
     * 判断一个对象，既不是null，也不是空
     * @param str
     * @return
     */
    public static boolean checkNotNullAndEmpty(Object object) {
		return Objects.nonNull(object) && !"".equals(object.toString());
	}



}


//    /**
//     * 生成add的kafka命令
//     *
//     * @param shallPath
//     * @param account
//     * @param topic
//     * @param operation
//     * @param auth
//     * @param zkHost
//     * @return
//     */
//    public static String generateAuthCmd(String shallPath, String account, String topic, String operation, String auth, String zkHost) {
//        return shallPath + "/kafka-acls.sh --authorizer kafka.security.auth.SimpleAclAuthorizer --authorizer-properties zookeeper.connect="
//                + zkHost + "  --" + operation + " --allow-principal User:" + account + " --operation " + auth + " --topic " + topic;
//    }

//    /**
//     * 生成remove的kafka命令
//     *
//     * @param shallPath
//     * @param account
//     * @param topic
//     * @param operation
//     * @param auth
//     * @param zkHost
//     * @return
//     */
//    public static String generateAuthCmdRemove(String shallPath, String account, String topic, String operation, String auth, String zkHost) {
//        return "echo y | " + shallPath + "/kafka-acls.sh --authorizer kafka.security.auth.SimpleAclAuthorizer --authorizer-properties zookeeper.connect="
//                + zkHost + "  --" + operation + " --allow-principal User:" + account + " --operation " + auth + " --topic " + topic;
//    }
//
//    public static String generateAuthFlag(String account, String auth) {
//        return "User:" + account + " has Allow permission for operations: " + auth + "";
//    }
