package com.kafka.easykafka.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

import com.kafka.easykafka.model.ZookeeperHostInfo;

public class ZkServerOperation {

    /**
     * 用来从zookeeper的配置信息中获取所有zk的服务器信息
     * @param config config 文件路径
     * @return ZookeeperHostInfo
     */
    public static ZookeeperHostInfo analyzingIps(String config){
        ZookeeperHostInfo zkHosts=new ZookeeperHostInfo();
        ArrayList<String> ips=new ArrayList<>();
        try {
            //"D:/tmp/zoo.cfg"
            FileInputStream in = new FileInputStream(new File(config));
            Properties cfg = new Properties();
            try {
                cfg.load(in);
            } finally {
                in.close();
            }
            for (Map.Entry<Object, Object> entry : cfg.entrySet()) {
                if(entry.getKey().toString().contains("server.") & !entry.getKey().toString().contains(".server.")){
                    ips.add((entry.getValue().toString().trim().split(":"))[0]);
                }
                if(entry.getKey().toString().contains("clientPort")){
                    //String a=entry.getValue().toString().replace(" ", "");
                    String a=entry.getValue().toString().trim();
                    Integer i=Integer.valueOf(a);
                    zkHosts.setClientPort(i);
                }
            }
            zkHosts.setIpsList(ips);
        } catch (Exception e) {
            return null;
        }
        return zkHosts;
    }

    /**
     * 通过Zookeeper的四字命令获取服务器的状态,检测zkIp对应的zk服务器状态是否正常
     * @param host zkIP
     * @param port port
     * @return 验证结果
     */
    public static Integer zkServerStatusCheck(String host, Integer port) throws IOException {
        return zkServerStatusCheck(new Socket(host, port), null, GlobalPropoties.ZK_RUOK);
    }

    private static Integer zkServerStatusCheck(Socket sock, BufferedReader reader, String cmd) throws IOException {
        try {
            OutputStream outputStream= sock.getOutputStream();
            outputStream.write(cmd.getBytes());
            outputStream.flush();
            sock.shutdownOutput();
            reader=new BufferedReader(new InputStreamReader(sock.getInputStream()));
            String line;
            while(Objects.nonNull(line = reader.readLine())){
                if (Objects.equals(line, GlobalPropoties.ZK_IMOK)){
                    return Util.ONE;
                }
            }
            return Util.ZERO;
        }finally {
            close(sock, reader);
        }
    }

    private static void close(Socket sock, BufferedReader reader) throws IOException {
        sock.close();
        if (reader != null) {
            reader.close();
        }
    }


    /**
     * 通过Zookeeper的四字命令获取服务器的状态,检测zkIp对应的zk服务器角色是什么
     * @param host zkIP
     * @param port port
     * @return 验证结果
     */
    public static String zkServerRoleCheck(String host, Integer port) throws IOException {
        return zkServerRoleCheck(new Socket(host, port), null, GlobalPropoties.ZK_STAT);
    }

    private static String zkServerRoleCheck(Socket sock, BufferedReader reader, String cmd) throws IOException {
        try {
            OutputStream outputStream = sock.getOutputStream();
            outputStream.write(cmd.getBytes());
            outputStream.flush();
            sock.shutdownOutput();
            reader = new BufferedReader(new InputStreamReader(sock.getInputStream()));
            String line;
            while (Objects.nonNull(line = reader.readLine())) {
                if (line.contains("Mode: ")) {
                    return line.replaceAll("Mode: ", "").trim();
                }
            }
        } finally {
            close(sock, reader);
        }
        return "";
    }


//    public static ArrayList<String> analyzingIps(String config){
//        ArrayList<String> ips=new ArrayList<>();
//        ZookeeperHostInfo zkHosts=new ZookeeperHostInfo();
//        try {
//            //"D:/tmp/zoo.cfg"
//            FileInputStream in = new FileInputStream(new File(config));
//            Properties cfg = new Properties();
//            try {
//                cfg.load(in);
//            } finally {
//                in.close();
//            }
//            for (Map.Entry<Object, Object> entry : cfg.entrySet()) {
//                if(entry.getKey().toString().contains("server.") & !entry.getKey().toString().contains(".server.")){
//                    ips.add((entry.getValue().toString().split(":"))[0]);
//                }
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return ips;
//    }



}
