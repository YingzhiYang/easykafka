package com.kafka.easykafka.util;

import java.util.ArrayList;
import java.util.List;

public class UtilPages {

    private static UtilPages utilPages;

    private int currentPage = 1;// 当前页面
    private int theFirstPage = 1;
    private int theLastPage = 0;
    private int pageSize = 10;// 页面大小
    private int totalCount = 0;// 总记录数
    private int pagesCount = 0;// 总页面数
    private List<Object> pageContent;// 当前页面的内容，即查询的分页的数据
    private List<ArrayList<Object>> pageContentDouble;// 当前页面的内容，即查询的分页的数据
    private List<Object> totalContent;// 当前所有的内容，即查询的所有数据
    private Integer[] currentNavigationPages; // 当前页面上要显示的分页页码数目

    private int navigationPages = 0;
    private boolean isHasNextPage = false;
    private boolean isHasPreviousPage = false;
    private boolean isFirstPage = false;
    private boolean isLastPage = false;

    public UtilPages() {

    }

    public void getInitialPages(UtilPages utilPages, List<Object> list, Integer pn) {
        utilPages.setNavigationPages(Util.VIEW_PAGES);
        utilPages.setTotalCount(list.size());
        utilPages.setTheFirstPage(Util.FIRST_PAGE);
        utilPages.setPageSize(Util.SINGLE_PAGE_SIZE_TOPIC);
        utilPages.setPagesCount(utilPages.getPagesCount());
        if (pn >= utilPages.getPagesCount()) {
            pn = utilPages.getPagesCount();
        }
        utilPages.setCurrentPage(pn);
        utilPages.setTheLastPage(utilPages.getLastPage());
        if(pn*10>utilPages.getTotalCount()){
            utilPages.setPageContent(list.subList((pn-1)*10,utilPages.getTotalCount()));
        }else {
            utilPages.setPageContent(list.subList((pn-1)*10,pn*10));
        }
        utilPages.setCurrentNavigationPages(utilPages.getCurrentNavigationPages());
        utilPages.setHasNextPage(utilPages.isHasNextPage());
        utilPages.setHasPreviousPage(utilPages.isHasPreviousPage());
        utilPages.setFirstPage(utilPages.isFirstPage());
        utilPages.setLastPage(utilPages.isLastPage());
    }



    /**
     * 拿到首页
     * @return
     */
    public int getFirstPage() {
        return theFirstPage;
    }

    /**
     * 设置首页
     * @param firstPage
     */
    public void setFirstPage(int firstPage) {
        this.theFirstPage = firstPage;
    }

    /**
     * 设置末页
      * @param lastPage
     */
    public void setLastPage(int lastPage) {
        this.theLastPage = lastPage;
    }

    /**
     * 拿到最后一页
     * @return
     */
    public int getLastPage() {
        this.theLastPage = pagesCount;
        return theLastPage;
    }

    /**
     * 设置连续显示的页码数组
     * @param currentNavigationPages
     */
    public void setCurrentNavigationPages(Integer[] currentNavigationPages) {
        this.currentNavigationPages = currentNavigationPages;
    }

    /**
     * 拿到连续现实的页码数组
     * @return
     */
    public Integer[] getCurrentNavigationPages() {
        int innerNavPag = navigationPages;
        int tempCurrentPage = currentPage;
        int half = innerNavPag / 2;
        int innerPages = pagesCount;
        if (innerNavPag > innerPages) {
            innerNavPag = innerPages;
        }
        Integer[] innerCNP = new Integer[innerNavPag];
        if (tempCurrentPage + half > innerPages) {
            tempCurrentPage = innerPages - innerNavPag + 1;
            return generateNavitionPages(tempCurrentPage, innerNavPag, innerCNP);
        }
        tempCurrentPage -= half;
        if (tempCurrentPage <= 0) {
            tempCurrentPage = 1;
            return generateNavitionPages(tempCurrentPage, innerNavPag, innerCNP);
        }
        return generateNavitionPages(tempCurrentPage, innerNavPag, innerCNP);
    }

    private Integer[] generateNavitionPages(Integer tempCurrentPage, Integer innerNavPag, Integer[] innerCNP) {
        int i = 0;
        while (innerNavPag > 0) {
            innerCNP[i] = tempCurrentPage;
            innerNavPag--;
            tempCurrentPage++;
            i++;
        }
        return innerCNP;
    }

    /**
     * 拿到当前的页码
     * @return
     */
    public int getCurrentPage() {
        return currentPage;
    }

    /**
     * 设置当前的页码
     * @param currentPage
     */
    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTheFirstPage() {
        return theFirstPage;
    }

    public void setTheFirstPage(int theFirstPage) {
        this.theFirstPage = theFirstPage;
    }

    public int getTheLastPage() {
        return theLastPage;
    }

    public void setTheLastPage(int theLastPage) {
        this.theLastPage = theLastPage;
    }

    /**
     * 拿到当前的单页大小
     * @return
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * 设置当前的单页大小
     * @param pageSize
     */
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * 拿到当前的总记录数
     * @return
     */
    public int getTotalCount() {
        return totalCount;
    }

    /**
     * 设置当前的总记录数
     * @param totalCount
     */
    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * 拿到当前的总页码
     * @return
     */
    public int getPagesCount() {
        // 如果不等0则取整数+1，比如10%3=3.3，就是4页，所以要加一
        if (totalCount % pageSize == 0) {
            pagesCount = totalCount / pageSize;
        } else {
            pagesCount = totalCount / pageSize + 1;
        }
        return pagesCount;
    }
    /**
     * 设置当前的总页码
     * @return
     */
    public void setPagesCount(int pages) {
        this.pagesCount = pages;
    }

    /**
     * 拿到当前页面
     * @return
     */
    public List<Object> getPageContent() {
        return pageContent;
    }

    /**
     * 设置当前页面
     * @param pageContent
     */
    public void setPageContent(List<Object> pageContent) {
        this.pageContent = pageContent;
    }

    public List<ArrayList<Object>> getPageContentDouble() {
        return pageContentDouble;
    }

    public void setPageContentDouble(List<ArrayList<Object>> pageContentDouble) {
        this.pageContentDouble = pageContentDouble;
    }

    public List<Object> getTotalContent() {
        return totalContent;
    }

    public void setTotalContent(List<Object> totalContent) {
        this.totalContent = totalContent;
    }

    public int getNavigationPages() {
        return navigationPages;
    }

    public void setNavigationPages(int navigationPages) {
        this.navigationPages = navigationPages;
    }

    /**
     * 是否有下一页，如果有下一页则返回true，如果没有则返回false
     * @return
     */
    public boolean isHasNextPage() {
        if (currentPage < pagesCount) {
            isHasNextPage = true;
        } else {
            isHasNextPage = false;
        }
        return isHasNextPage;
    }

    /**
     * 设置是否有下一页
     * @param hasNextPage
     */
    public void setHasNextPage(boolean hasNextPage) {
        isHasNextPage = hasNextPage;
    }

    public boolean isHasPreviousPage() {
        if (currentPage > theFirstPage) {
            isHasPreviousPage = true;
        } else {
            isHasPreviousPage = false;
        }
        return isHasPreviousPage;
    }

    public void setHasPreviousPage(boolean hasPreviousPage) {
        isHasPreviousPage = hasPreviousPage;
    }

    public boolean isFirstPage() {
        if (currentPage == theFirstPage) {
            isFirstPage = true;
        } else {
            isFirstPage = false;
        }
        return isFirstPage;
    }

    public void setFirstPage(boolean firstPage) {
        isFirstPage = firstPage;
    }

    public boolean isLastPage() {
        if (currentPage == pagesCount) {
            isLastPage = true;
        } else {
            isLastPage = false;
        }
        return isLastPage;
    }

    public void setLastPage(boolean lastPage) {
        isLastPage = lastPage;
    }
}
