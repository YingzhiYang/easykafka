package com.kafka.easykafka.util;

public class GlobalPropoties {

	
	final public static String EMPTY_STRING="";
	
    final public static String INDEX_PAGE = "index";

//    public static String getIndexPage() {
//        return INDEX_PAGE;
//    }

    final public static String ADD_CLUSTER_PAGE = "add_cluster";

//    public static String getAddClusterPage() {
//        return ADD_CLUSTER_PAGE;
//    }

    final public static String CLUSTER_MANAGE_PAGE = "cluster_manage";

//    public static String getClusterManagePage() {
//        return CLUSTER_MANAGE_PAGE;
//    }

    final public static String TOPIC_PAGE = "topic_list";

//    public static String getTopicPage() {
//        return TOPIC_PAGE;
//    }

    final public static String TOPIC_ADD_PAGE = "topic_add";

//    public static String getTopicAddPage() {
//        return TOPIC_ADD_PAGE;
//    }

    final public static String TOPIC_DESCRIPTION_PAGE = "topic_description";


    final public static String AUTH_VIEW_PAGE = "authorization_list";

//    public static String getAuthViewPage() {
//        return AUTH_VIEW_PAGE;
//    }

    final public static String AUTH_VIEW_SINGLE_PAGE = "authorization_list_single";

//    public static String getAuthViewSinglePage() {
//        return AUTH_VIEW_SINGLE_PAGE;
//    }

    final public static String AUTH_ACCOUNT_PAGE = "authorization_page";
//    public static String getAuthAccountPage() {
//        return AUTH_ACCOUNT_PAGE;
//    }

    final public static String TOPIC_MULTIPLE_ADD_PAGE = "topic_multiple_add";

//    public static String getTopicMultipleAddPage() {
//        return TOPIC_MULTIPLE_ADD_PAGE;
//    }

    final public static String TOPIC_EDIT_PAGE = "topic_edit";

//    public static String getTopicEdit() {
//        return TOPIC_EDIT;
//    }

    final public static String ZOOKEEPER_VIEW_PAGE = "zookeeper_view";

//    public static String getZookeeperViewPage() {
//        return ZOOKEEPER_VIEW_PAGE;
//    }

    final public static String SSL_LOCATION = "ssl.truststore.location";

//    public static String getSslLocation() {
//        return SSL_LOCATION;
//    }

    final public static String SSL_PASSWORD = "ssl.truststore.password";

    final public static String SASL_MECHANISM = "sasl.mechanism";

    final public static String SASL_JAAS_CONFIG = "sasl.jaas.config";
    
    final public static String SASL_ENDPOINT_IDENTIFICATION_ALGORITHM = "ssl.endpoint.identification.algorithm";

    public static String SASL_JAAS_CONFIG_INFO = "org.apache.kafka.common.security.scram.ScramLoginModule required username='";

//    public static String getSslPassword() {
//        return SSL_PASSWORD;
//    }

//    public static String getSaslMechanism() {
//        return SASL_MECHANISM;
//    }

//    public static String getSaslJaasConfig() {
//        return SASL_JAAS_CONFIG;
//    }

    public static String getSaslJaasConfigInfo(String account, String pwd) {
        SASL_JAAS_CONFIG_INFO = SASL_JAAS_CONFIG_INFO + account + "' password='" + pwd + "';";
        return SASL_JAAS_CONFIG_INFO;
    }





    //kafka topic 各项常用参数
    final public static String NUM_PARTITIONS = "num.partitions";
    final public static String OFFSETS_TOPIC_REPLICATION_FACTOR = "offsets.topic.replication.factor";
    final public static String DEFAULT_REPLICATION_FACTOR = "default.replication.factor";
    final public static String RETENTION_MS = "retention.ms";
    final public static String MAX_MESSAGE_BYTES = "max.message.bytes";
    final public static String SEGMENT_INDEX_BYTES = "segment.index.bytes";
    final public static String SEGMENT_BYTES = "segment.bytes";
    final public static String MIN_CLEANABLE_DIRTY_RATIO = "min.cleanable.dirty.ratio";
    final public static String MIN_INSYNC_REPLICAS = "min.insync.replicas";
    final public static String DELETE_RETENTION_MS = "delete.retention.ms";
    final public static String FLUSH_MESSAGES = "flush.messages";
    final public static String PREALLOCATE = "preallocate";
    final public static String RETENTION_BYTES = "retention.bytes";
    final public static String FLUSH_MS = "flush.ms";
    final public static String CLEANUP_POLICY = "cleanup.policy";
    final public static String FILE_DELETE_DELAY_MS = "file.delete.delay.ms";
    final public static String SEGMENT_JITTER_MS = "segment.jitter.ms";
    final public static String INDEX_INTERVAL_BYTES = "index.interval.bytes";
    final public static String COMPRESSION_TYPE = "compression.type";
    final public static String DEFAULT_COMPRESSION_TYPE = "uncompressed";
    final public static String SEGMENT_MS = "segment.ms";
    final public static String UNCLEAN_LEADER_ELECTION_ENABLE = "unclean.leader.election.enable";

//    public static String getDefaultCompressionType() {
//        return DEFAULT_COMPRESSION_TYPE;
//    }

//    public static String getPreallocate() {
//        return PREALLOCATE;
//    }

//    public static String getSaslJaasConfigInfo() {
//        return SASL_JAAS_CONFIG_INFO;
//    }

//    public static String getOffsetsTopicReplicationFactor() {
//        return OFFSETS_TOPIC_REPLICATION_FACTOR;
//    }

//    public static String getNumPartitions() {
//        return NUM_PARTITIONS;
//    }

//    public static String getDefaultReplicationFactor() {
//        return DEFAULT_REPLICATION_FACTOR;
//    }

//    public static String getRetentionMs() {
//        return RETENTION_MS;
//    }

//    public static String getMaxMessageBytes() {
//        return MAX_MESSAGE_BYTES;
//    }

//    public static String getSegmentIndexBytes() {
//        return SEGMENT_INDEX_BYTES;
//    }

//    public static String getSegmentBytes() {
//        return SEGMENT_BYTES;
//    }

//    public static String getMinCleanableDirtyRatio() {
//        return MIN_CLEANABLE_DIRTY_RATIO;
//    }

//    public static String getMinInsyncReplicas() {
//        return MIN_INSYNC_REPLICAS;
//    }

//    public static String getDeleteRetentionMs() {
//        return DELETE_RETENTION_MS;
//    }

//    public static String getFlushMessages() {
//        return FLUSH_MESSAGES;
//    }

//    public static String getRetentionBytes() {
//        return RETENTION_BYTES;
//    }

//    public static String getFlushMs() {
//        return FLUSH_MS;
//    }

//    public static String getCleanupPolicy() {
//        return CLEANUP_POLICY;
//    }

//    public static String getFileDeleteDelayMs() {
//        return FILE_DELETE_DELAY_MS;
//    }

//    public static String getSegmentJitterMs() {
//        return SEGMENT_JITTER_MS;
//    }

//    public static String getIndexIntervalBytes() {
//        return INDEX_INTERVAL_BYTES;
//    }

//    public static String getCompressionType() {
//        return COMPRESSION_TYPE;
//    }

//    public static String getSegmentMs() {
//        return SEGMENT_MS;
//    }

//    public static String getUncleanLeaderElectionEnable() {
//        return UNCLEAN_LEADER_ELECTION_ENABLE;
//    }


    final public static String KAFKA_BIN_PATH="/usr/local/kafka_2.12-2.1.0/bin/";

    final public static String ZK_HOST="n5.ikt.lenovo.com:2181/kafka210";



    final public static String EXCEPTION_FLAG="exception";

//    public static String getExceptionFlag() {
//        return EXCEPTION_FLAG;
//    }
    
    final public static String TOPIC_COUNT="topicCount";

    final public static String TOPIC_PARTITIONS="topicPartitions";

//    public static String getTopicPartitions() {
//        return TOPIC_PARTITIONS;
//    }

    final public static String TOPIC_AUTHORIZATION="topicAuthorization";

//    public static String getTopicAuthorization() {
//        return TOPIC_AUTHORIZATION;
//    }

    final public static String ZOOKEEPER_INFO="zookeeper";

//    public static String getZookeeperInfo() {
//        return ZOOKEEPER_INFO;
//    }

    final public static String ZK_IMOK="imok";

//    public static String getZkImok() {
//        return ZK_IMOK;
//    }
    final public static String ZK_RUOK="ruok";
//    public static String getZkRuok() {
//        return ZK_RUOK;
//    }

    final public static String ZK_STAT="stat";

//    public static String getZkStat() {
//        return ZK_STAT;
//    }

    final public static String TOPIC_CONFIG="topicConfig";

    final public static String ZK_DEL_REPORT="zookeeper node delete failed";

    
    final public static String CLUSTER_NAME="clusterName";
    

}
