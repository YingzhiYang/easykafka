package com.kafka.easykafka.util;

import java.util.Properties;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.springframework.stereotype.Repository;

@Repository
public class KafkaConnection {

    private static volatile AdminClient adminClient;

    /*@PostConstruct
    private void initAdminClient(){
        if (!Objects.nonNull(adminClient)) {
            //测试
            adminClient = buildAdminClientConnection("10.122.49.166:9092", "SASL_SSL",
                    "D:/Working/Guidance/Kafka/client_truststore.jks", "WSO2_sp440",
                    "SCRAM-SHA-512",
                    "org.apache.kafka.common.security.scram.ScramLoginModule required username='admin' password='BROKER_admin';");
            //生产
            this.adminClient = buildAdminClientConnection("10.122.42.51:9092", "SASL_SSL",
                    "D:/Working/Guidance/Kafka/client_truststore.jks", "WSO2_sp440",
                    "SCRAM-SHA-512",
                    "org.apache.kafka.common.security.scram.ScramLoginModule required username='admin' password='BROKER_admin!1234';");
        }
    }*/


    public static AdminClient getAdminClient() {
        return adminClient;
    }

    public static void setAdminClient(AdminClient adminClient) {
        KafkaConnection.adminClient = adminClient;
        System.out.println("Refresh AdminClient Information:"+adminClient.toString());
    }

    /**
     * 获取一个AdminClient的实例
     *
     * @param kafkaUrl         Broker的连接地址。eg. localhost:9092
     * @param securityProtocol 安全协议
     * @param sslLocation      ssl的**.jks文件地址
     * @param sslPwd           ssl文件密码
     * @param saslMechanism    sasl认证机制
     * @return  返回AdminClient实例对象
     */
    public static AdminClient buildAdminClientConnection(String kafkaUrl, String securityProtocol, String sslLocation,
                                             String sslPwd, String saslMechanism, String saslJaasConfig) {
        Properties props = new Properties();
        props.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaUrl);
        props.put(AdminClientConfig.SECURITY_PROTOCOL_CONFIG, securityProtocol);
        props.put(GlobalPropoties.SSL_LOCATION, sslLocation);
        props.put(GlobalPropoties.SSL_PASSWORD, sslPwd);
        props.put(GlobalPropoties.SASL_MECHANISM, saslMechanism);
        props.put(GlobalPropoties.SASL_JAAS_CONFIG, saslJaasConfig);
        props.put(GlobalPropoties.SASL_ENDPOINT_IDENTIFICATION_ALGORITHM, GlobalPropoties.EMPTY_STRING);
        return AdminClient.create(props);
    }
}
