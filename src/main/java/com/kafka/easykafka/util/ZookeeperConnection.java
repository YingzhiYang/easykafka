package com.kafka.easykafka.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Objects;

import javax.annotation.PostConstruct;

import org.apache.kafka.common.security.JaasUtils;
import org.apache.kafka.common.utils.Time;
import org.apache.zookeeper.ZooKeeper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.ResourceUtils;

import com.kafka.easykafka.mapper.ClustersMapper;
import com.kafka.easykafka.model.Cluster;

import kafka.zk.AdminZkClient;
import kafka.zk.KafkaZkClient;
import kafka.zookeeper.ZooKeeperClient;
import scala.Int;

@Repository
public class ZookeeperConnection {

    private static ClustersMapper mapper;

    @Autowired
    public void setMapper(ClustersMapper mapper) {
        ZookeeperConnection.mapper = mapper;
    }

    private volatile static AdminZkClient adminZkClient;
    private volatile static ZooKeeper zooKeeper;

    /**
     * 预先获取到上传的文件信息
     * @throws FileNotFoundException 抛出异常
     */
    @PostConstruct
    public void init() throws FileNotFoundException {
        //String path = ResourceUtils.getURL("classpath:").getPath();
        //System.out.println(path);
        //后续应该做成可以上传的，或者接受输入路径的，目前先这样写死，
        //	eclipse和idea的方式不一样，下面的这种路径无法用到eclipse上
        //File file = new File(path,"static/configFiles/zk-client-jaas.conf");
        
//        File file = new File("D:/tmp/zk-client-jaas.conf");
//        System.setProperty("java.security.auth.login.config", file.getAbsolutePath());
        
        //fileProd是生产注意不要用混乱
        //File fileProd = new File(path,"static/configFiles/broker-prod-jaas.conf");
        //System.setProperty("java.security.auth.login.config", fileProd.getAbsolutePath());
        
        //写死在服务器上，暂时这样做,后续要从数据库取这个路径
//        File file = new File("zk-client-jaas.conf");
//        System.setProperty("java.security.auth.login.config", file.getAbsolutePath());
        //System.out.println(file.getAbsolutePath());
        //System.out.println(file.getPath());
        //System.out.println(System.getProperty("java.security.auth.login.config"));
    }

    /**
     * 根据clusterName获取到对应的zookeeper信息
     * @param clusterName clusterName
     * @return ZooKeeperClient
     */
    public static ZooKeeperClient buildZookeeperConnection(String clusterName){
    	String filePath=mapper.queryLoginConfig(clusterName);
    	File file = new File(filePath);
    	//System.out.println("file is uploaded:"+filePath);
        System.setProperty("java.security.auth.login.config", file.getAbsolutePath());
        Cluster cluster=mapper.queryZookeeperIPInfo(clusterName);
        if (Objects.isNull(cluster.getZkNodePath()) || cluster.getZkNodePath().equals("null") || cluster.getZkNodePath().isEmpty()) {
        	return new ZooKeeperClient(cluster.getZkIps().split(",")[0],30000, 30000, Int.MaxValue(), Time.SYSTEM,"","");
		}
        return new ZooKeeperClient(cluster.getZkIps().split(",")[0]+cluster.getZkNodePath(),30000, 30000, Int.MaxValue(), Time.SYSTEM,"","");
        
    }

    /**
     * 根据生成的ZooKeeperClient生成KafkaZkClient
     * @param clusterName clusterName
     * @return KafkaZkClient
     */
    private static KafkaZkClient buildKafkaZkClient(String clusterName){
        return new KafkaZkClient(buildZookeeperConnection(clusterName), JaasUtils.isZkSaslEnabled(), Time.SYSTEM);
    }

    /**
     * 根据生成的KafkaZkClient生成AdminZkClient
     * @param clusterName clusterName
     * @return KafkaZkClient
     */
    private static AdminZkClient buildAdminZkClient(String clusterName){
        return new AdminZkClient(buildKafkaZkClient(clusterName));
    }

    /**
     * ensure singleton
     * @param clusterName clusterName
     * @return AdminZkClient
     */
    public static AdminZkClient getAdminZkClient(String clusterName){
        if(Objects.isNull(adminZkClient)){
            synchronized (AdminZkClient.class){
                if(Objects.isNull(adminZkClient))
                    adminZkClient = ZookeeperConnection.buildAdminZkClient(clusterName);
            }
        }
        return adminZkClient;
    }

    /**
     * 获取当前使用的Zookeeper对象
     * @param clusterName clusterName
     * @return ZooKeeper
     */
    public static ZooKeeper getZooKeeper(String clusterName){
        if(Objects.isNull(zooKeeper)){
            synchronized (ZooKeeper.class){
                if(Objects.isNull(zooKeeper))
                    zooKeeper = ZookeeperConnection.buildKafkaZkClient(clusterName).currentZooKeeper();
            }
        }
        return zooKeeper;
    }
    
    /**
     * 重置Zk对象
     * @param clusterName
     */
    private static void setZookeeper(String clusterName) {
    	zooKeeper = ZookeeperConnection.buildKafkaZkClient(clusterName).currentZooKeeper();
	}

    /**
     * 刷新Zookeeper连接
     * @param clusterName
     */
    public static void refreshZookeeperConnection(String clusterName) {
    	System.out.println("Refresh Zookeeper Connection to " +clusterName);
    	buildAdminZkClient(clusterName);
        setZookeeper(clusterName);
	}
    
    
}
