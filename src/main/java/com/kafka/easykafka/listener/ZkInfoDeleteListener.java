package com.kafka.easykafka.listener;

import com.kafka.easykafka.mapper.ZkStatsMapper;
import com.kafka.easykafka.thread.ZookeeperExpiredInfoCheckRunnable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class ZkInfoDeleteListener implements ApplicationListener<ContextRefreshedEvent> {

    private ZkStatsMapper zkStatsMapper;

    @Autowired
    public ZkInfoDeleteListener(ZkStatsMapper zkStatsMapper) {
        this.zkStatsMapper = zkStatsMapper;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        Runnable runnable=new ZookeeperExpiredInfoCheckRunnable(zkStatsMapper);
        ScheduledExecutorService service= Executors.newSingleThreadScheduledExecutor();
        service.scheduleAtFixedRate(runnable,0,10, TimeUnit.DAYS);
    }
}
