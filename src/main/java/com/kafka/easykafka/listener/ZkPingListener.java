package com.kafka.easykafka.listener;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;

import com.kafka.easykafka.mapper.ZkStatsMapper;
import com.kafka.easykafka.service.FindClusterService;
import com.kafka.easykafka.thread.ZookeeperStatusCheckRunnable;

@Service
public class ZkPingListener implements ApplicationListener<ContextRefreshedEvent> {

    private final FindClusterService findClusterService;
    private ZkStatsMapper zkStatsMapper;

    @Autowired
    public ZkPingListener(FindClusterService findClusterService, ZkStatsMapper zkStatsMapper) {
        this.findClusterService = findClusterService;
        this.zkStatsMapper = zkStatsMapper;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if(findClusterService.queryClusterNum()>0){
           /* Thread check=new ZookeeperStatusCheckThread(findClusterService,zkStatsMapper);
            check.start();*/
            Runnable runnable=new ZookeeperStatusCheckRunnable(findClusterService,zkStatsMapper);
            ScheduledExecutorService service= Executors.newSingleThreadScheduledExecutor();
            service.scheduleAtFixedRate(runnable,0,5, TimeUnit.MINUTES);
        }

        //System.out.println("StartedListenerTest cluster count:"+findClusterService.queryClusterNum());
    }
}
