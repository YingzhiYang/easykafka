package com.kafka.easykafka.thread;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

import com.kafka.easykafka.mapper.ZkStatsMapper;
import com.kafka.easykafka.model.ZookeeperInfo;
import com.kafka.easykafka.model.ZookeeperSingleStatus;
import com.kafka.easykafka.service.FindClusterService;
import com.kafka.easykafka.util.Util;
import com.kafka.easykafka.util.ZkServerOperation;

@Deprecated
public class ZookeeperStatusCheckThread extends Thread{

    private FindClusterService findClusterService;
    private ZkStatsMapper zkStatsMapper;

    public ZookeeperStatusCheckThread(FindClusterService findClusterService, ZkStatsMapper zkStatsMapper) {
        this.findClusterService = findClusterService;
        this.zkStatsMapper = zkStatsMapper;
    }

    @Override
    public void run() {
        while (true) {
            try {
                zookeeperServerStatCheck();
                Thread.sleep(60000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            }
        }
    }

    private void zookeeperServerStatCheck() {
        List<ZookeeperInfo> zkCheckInfoList = findClusterService.queryZookeeperCheckStatusInfo();
        for (ZookeeperInfo zookeeperInfo : zkCheckInfoList) {
            String[] ips=zookeeperInfo.getZkIpsNotPort().split(",");
            for (String ip : ips) {
                ZookeeperSingleStatus status=new ZookeeperSingleStatus();
                try {
                    status.setIp(ip);
                    status.setClusterName(zookeeperInfo.getClusterName());
                    status.setPort(zookeeperInfo.getZkPort());
                    status.setRole(ZkServerOperation.zkServerRoleCheck(ip, zookeeperInfo.getZkPort()));
                    status.setStatus(ZkServerOperation.zkServerStatusCheck(ip,zookeeperInfo.getZkPort()));
                    status.setTimestamp(new Timestamp(System.currentTimeMillis()));
                    randomModulus(status);
                } catch (IOException e) {
                    status.setIp(ip);
                    status.setClusterName(zookeeperInfo.getClusterName());
                    status.setPort(zookeeperInfo.getZkPort());
                    status.setRole("");
                    status.setStatus(Util.ZERO);
                    status.setTimestamp(new Timestamp(System.currentTimeMillis()));
                    randomModulus(status);
                    e.printStackTrace();
                }
            }
        }
    }

    private void randomModulus(ZookeeperSingleStatus status) {
        if (System.currentTimeMillis()%2==1){
            zkStatsMapper.insertZkStats0(status);
        }else {
            zkStatsMapper.insertZkStats1(status);
        }
    }


}
