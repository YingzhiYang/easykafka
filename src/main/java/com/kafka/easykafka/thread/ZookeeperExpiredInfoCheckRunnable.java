package com.kafka.easykafka.thread;

import com.kafka.easykafka.mapper.ZkStatsMapper;

public class ZookeeperExpiredInfoCheckRunnable implements Runnable{

    private ZkStatsMapper zkStatsMapper;


    public ZookeeperExpiredInfoCheckRunnable(ZkStatsMapper zkStatsMapper) {
        this.zkStatsMapper = zkStatsMapper;
    }

    @Override
    public void run() {
        zkStatsMapper.deleteZkStats0();
        zkStatsMapper.deleteZkStats1();
    }
}
