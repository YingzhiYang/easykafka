package com.kafka.easykafka.service;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import com.kafka.easykafka.model.EKTopicAuthorization;

public interface AuthorizationDescriptionService {

    /**
     * 拿到所有的acl
     * @return
     */
    public ArrayList<EKTopicAuthorization> getAllAclDescription() throws ExecutionException, InterruptedException;

    /**
     * 根据principal拿到所有的acl
     * @return
     */
    public ArrayList<EKTopicAuthorization> getSingleAclDescription(String account) throws ExecutionException, InterruptedException;

    /**
     * 根据pattern去筛选allAuthorizations里面的内容
     * @param allAuthorizations
     * @param content
     * @return
     */
    public ArrayList<EKTopicAuthorization> getClickSearchedACL(ArrayList<EKTopicAuthorization> allAuthorizations, String content);
}
