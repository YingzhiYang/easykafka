package com.kafka.easykafka.service;

import com.kafka.easykafka.model.Cluster;
import com.kafka.easykafka.model.Message;
import com.kafka.easykafka.model.Topic;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

public interface TopicService {

    /**
     * 获取所有topic名字和partition数量
     *
     * @return
     */
    Set<String> getAllTopics(Cluster cluster);

    /**
     * 过滤list需要搜索的name
     * @param list
     * @param name
     * @return
     */
    List<String> getClickSearchedTopic(List<String> list, String name);

    /**
     * 每次添加一个topic
     * @param topic
     * @return
     */
    Message addATopic(Topic topic);

    /**
     * 每次添加多个topics
     * @param topic
     * @param topicList
     * @return
     */
    Message addMultipleTopics(Topic topic, List<String> topicList);

    /**
     * 删除单一topic
     * @param topicName
     * @param topicFullList
     * @return
     */
    Message deleteATopic(String topicName,List<String> topicFullList) throws ExecutionException, InterruptedException;

    /**
     * 删除多个topic
     * @param deleteTopicNames
     * @param topicFullList
     * @return
     */
    Message deleteMultipleTopic(String deleteTopicNames,List<String> topicFullList) throws ExecutionException, InterruptedException;

    /**
     * 根据topic名字拿到topic的运行时信息
     * @param topicName
     * @return
     */
    Message getTopicDescription(String topicName) throws ExecutionException, InterruptedException;

    /**
     * 根据topic名字拿到topic的授权信息
     * @param topicName
     * @return
     */
    Message getTopicAuthorizationDescription(String topicName) throws ExecutionException, InterruptedException;


}
