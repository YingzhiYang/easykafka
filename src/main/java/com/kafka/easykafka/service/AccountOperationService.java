package com.kafka.easykafka.service;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.zookeeper.KeeperException;

import com.kafka.easykafka.model.ScramAccount;

public interface AccountOperationService {


    /**
     * 创建账户
     *
     */
    public boolean createScramAccount(ScramAccount account) throws NoSuchAlgorithmException;

    /**
     * 生成随机数
     *
     * @return
     */
    public String generatePassword(Integer nums, Integer lowerCase, Integer capital, Integer length);


    /**
     * delete账户
     *
     * @return
     */
    public List<String> deleteScramAccount(ScramAccount account) throws NoSuchAlgorithmException, ExecutionException, InterruptedException, KeeperException;

}
