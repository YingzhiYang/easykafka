package com.kafka.easykafka.service;

import java.util.concurrent.ExecutionException;

import com.kafka.easykafka.model.TopicConfiguration;

public interface TopicConfigOperationService {
	/**
	 * 查找传入指定Topic的配置信息
	 * @param topicName topicName
	 * @return TopicConfiguration
	 * @throws ExecutionException ExecutionException
	 * @throws InterruptedException InterruptedException
	 */
    TopicConfiguration getTopicConfigurations(String topicName) throws ExecutionException, InterruptedException;
    
    /**
     * 给指定topic设置新的参数
     * @param topicName topicName
     * @param topicConfiguration topicConfiguration
     * @return
     * @throws ExecutionException  ExecutionException
     * @throws InterruptedException  InterruptedException
     */
    boolean setTopicConfigurations(String topicName,TopicConfiguration topicConfiguration) throws InterruptedException, ExecutionException;

}
