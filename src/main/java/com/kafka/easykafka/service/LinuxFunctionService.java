package com.kafka.easykafka.service;

import com.jcraft.jsch.JSchException;
import com.kafka.easykafka.model.Account;

import java.io.IOException;

@Deprecated
public interface LinuxFunctionService {

    public boolean createAccount(Account account) throws JSchException, IOException;

}
