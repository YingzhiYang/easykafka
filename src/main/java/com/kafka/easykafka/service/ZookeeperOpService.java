package com.kafka.easykafka.service;

import com.kafka.easykafka.model.ZookeeperSingleStatus;

import java.io.IOException;
import java.util.List;

public interface ZookeeperOpService {

    /**
     * 获取当前的Zookeeper集群的状态
     * @return
     */
    List<ZookeeperSingleStatus> getZookeeperListInfo(String clusterName) throws IOException;

    /**
     * ping当前ip和port
     * @return
     */
    boolean getZookeeperPing(String ip, Integer port) throws IOException;

}
