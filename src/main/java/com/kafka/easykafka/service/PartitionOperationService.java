package com.kafka.easykafka.service;

import java.util.concurrent.ExecutionException;

public interface PartitionOperationService {

    /**
     * 获取到指定topic的分区数
     * @param topicName topicName
     * @return 返回分区数
     */
    Integer getTopicPartitions(String topicName) throws ExecutionException, InterruptedException;

    /**
     * 设置指定topic的分区数
     * @param topicName topicName
     * @return boolean
     * @throws ExecutionException 
     * @throws InterruptedException 
     */
    boolean setTopicPartitions(String topicName, Integer partitions) throws InterruptedException, ExecutionException;

}
