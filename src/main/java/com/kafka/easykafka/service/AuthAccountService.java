package com.kafka.easykafka.service;

import com.kafka.easykafka.model.AuthorizationToAccount;
import com.kafka.easykafka.model.Group;
import com.kafka.easykafka.model.Message;

import java.util.concurrent.ExecutionException;

public interface AuthAccountService {

    /**
     * 添加权限给多个topics
     * @param authorizationToAccount 授权列表
     * @return Message
     */
    Message addAuthorizationAccount(AuthorizationToAccount authorizationToAccount);


    /**
     * 移除多个topics的权限
     * @param authorizationToAccount 授权列表
     * @return Message
     */
    Message removeAuthorizationAccount(AuthorizationToAccount authorizationToAccount);

    /**
     * 给指定的一个账户添加一个group权限
     * @param group Group#groupId,#accountName
     * @return Message
     */
    Message addGroupId(Group group) throws ExecutionException, InterruptedException;

    /**
     * 给指定的一个账户移除一个group权限
     * @param group Group#groupId,#accountName
     * @return Message
     */
    Message removeGroupId(Group group) throws ExecutionException, InterruptedException;
}
