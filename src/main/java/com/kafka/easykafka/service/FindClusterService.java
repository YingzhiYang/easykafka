package com.kafka.easykafka.service;

import com.kafka.easykafka.model.ZookeeperInfo;

import java.util.List;

public interface FindClusterService {

    /**
     * 查询表中有多少条cluster记录
     * @return
     */
    Integer queryClusterNum();


    /**
     * 查询出来表中所有cluster信息，这些信息包含了所有Zookeeper的ip，和Zookeeper的client port
     * @return
     */
    List<ZookeeperInfo> queryZookeeperCheckStatusInfo();


}
