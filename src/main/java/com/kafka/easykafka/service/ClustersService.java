package com.kafka.easykafka.service;

import com.kafka.easykafka.model.Cluster;

import java.util.List;
import java.util.Map;

public interface ClustersService {

    /**
     * 获取一个cluster的连接
     * @param kafkaUrl              Broker的连接地址。eg. localhost:9092
     * @param supName               超级用户
     * @param supPwd                超级用户密码
     * @param securityProtocol      安全协议
     * @param sslLocation           ssl的**.jks文件地址
     * @param sslPwd                ssl文件密码
     * @param saslMechanism         sasl认证机制
     * @return
     */
    boolean getClusterInstance(String kafkaUrl,String supName, String supPwd, String securityProtocol, String sslLocation, String sslPwd, String saslMechanism);

    /**
     * 添加一个集群
     * @param cluster
     * @return
     */
    int addCluster(Cluster cluster);
    int updateCluster(Cluster cluster);

    /**
     * 根据name找到一个cluster的所有存在数据库信息
     * @param name
     * @return
     */
    Cluster findCluster(String name);

    /**
     * 查找所有存在数据库的Cluster信息
     * @return
     */
    List<Cluster> findAllCluster();

    /**
     * 查找所有存在数据库的Cluster Name
     * @return
     */
    List<Map<String, Object>> findAllClusterName();

    /**
     * 移除一个存在数据库的Cluster
     * @return
     */
    int removeBroker(String name);


    /**
     * 从kafka服务器中查到所有的broker ip信息
     * @return
     */
    List<String> findAllClustersFromKafkaServer();


}
