package com.kafka.easykafka.mapper;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.kafka.easykafka.model.Cluster;
import com.kafka.easykafka.model.ZookeeperInfo;

@Repository
public interface ClustersMapper {
    /**
     * 向数据库插入一个cluster
     * @param cluster Cluster
     * @return int
     */
    int insertCluster(Cluster cluster);

    /**
     * 更新一个数据库中的cluster
     * @param cluster Cluster
     * @return int
     */
    int updateCluster(Cluster cluster);

    /**
     * 删除一个数据库中的cluster
     * @param name cluster name
     * @return int
     */
    int deleteCluster(String name);

    /**
     * 查询数据库中所有的cluster信息
     * @return List<Cluster>
     */
    List<Cluster> queryAllCluster();
    /**
     * 查询数据库中所有的cluster name
     * @return List<Map<String, Object>>
     */
    List<Map<String, Object>> queryAllClusterName();

    /**
     * 根据名字name查询一个数据库中的cluster信息
     * @param name clusterName
     * @return Cluster
     */
    Cluster queryCluster(String name);

    /**
     * 查询表中有多少条cluster记录
     * @return Integer
     */
    Integer queryClusterNum();

    /**
     * 查询出来表中所有cluster信息，这些信息包含了所有Zookeeper的ip，和Zookeeper的client port
     * @return List<ZookeeperInfo>
     */
    List<ZookeeperInfo> queryZookeeperCheckStatusInfo();

    /**
     * 查询出来表中指定的cluster信息，这些信息包含了所有Zookeeper的ip，和Zookeeper的client port
     * @return ZookeeperInfo
     */
    ZookeeperInfo queryZookeeperInfoForSpecifiedCluster(String clusterName);

    /**
     * 查询指定Cluster的ZK host信息
     * @param clusterName clusterName
     * @return String
     */
    Cluster queryZookeeperIPInfo(String clusterName);

    /**
     * 查询执行ClusterName配置的路径
     * @param clusterName
     * @return String:返回文件路径
     */
    String queryLoginConfig(String clusterName);
    
    
}
