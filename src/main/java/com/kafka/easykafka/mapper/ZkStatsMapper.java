package com.kafka.easykafka.mapper;

import com.kafka.easykafka.model.ZookeeperSingleStatus;
import org.springframework.stereotype.Repository;

@Repository
public interface ZkStatsMapper {

    /**
     * 向数据库表0插入一条Zookeeper数据
     * @param status
     * @return
     */
    Integer insertZkStats0(ZookeeperSingleStatus status);

    /**
     * 向数据库表1插入一条Zookeeper数据
     * @param status
     * @return
     */
    Integer insertZkStats1(ZookeeperSingleStatus status);

    /**
     * 向数据库表1删除7天前的数据
     * @return
     */
    Integer deleteZkStats1();
    /**
     * 向数据库表0删除7天前的数据
     * @return
     */
    Integer deleteZkStats0();


}
