package com.kafka.easykafka.exception;

import com.kafka.easykafka.model.Message;
import com.kafka.easykafka.util.GlobalPropoties;
import com.kafka.easykafka.util.Util;
import org.apache.zookeeper.KeeperException;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutionException;

@ControllerAdvice
@Repository
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Message globalExceptionHandlerMessage(Exception e){
        System.out.println("["+this.getClass().getSimpleName()+"] is working."); //已经生效
        return Message.fail(Util.ERR_CODE_400, Util.ERR_MSG_400).addContent(GlobalPropoties.EXCEPTION_FLAG,
                new GlobalExceptionMessage(
                        e.getStackTrace()[0].getClassName(),
                        e.getStackTrace()[0].getMethodName(),
                        e.getStackTrace()[0].getLineNumber(),
                        e.getMessage()));
    }

    @ExceptionHandler(ExecutionException.class)
    @ResponseBody
    public Message globalExecutionExceptionHandlerMessage(Exception e){
        System.out.println("["+this.getClass().getSimpleName()+"] is working."); //已经生效
        return Message.fail(Util.ERR_CODE_600, Util.ERR_MSG_600).addContent(GlobalPropoties.EXCEPTION_FLAG,
                new GlobalExceptionMessage(
                        e.getStackTrace()[0].getClassName(),
                        e.getStackTrace()[0].getMethodName(),
                        e.getStackTrace()[0].getLineNumber(),
                        e.getMessage()));
    }


    @ExceptionHandler(InterruptedException.class)
    @ResponseBody
    public Message globalInterruptedExceptionHandlerMessage(Exception e){
        System.out.println("["+this.getClass().getSimpleName()+"] is working."); //已经生效
        return Message.fail(Util.ERR_CODE_600, Util.ERR_MSG_600).addContent(GlobalPropoties.EXCEPTION_FLAG,
                new GlobalExceptionMessage(
                        e.getStackTrace()[0].getClassName(),
                        e.getStackTrace()[0].getMethodName(),
                        e.getStackTrace()[0].getLineNumber(),
                        e.getMessage()));
    }


    @ExceptionHandler(KeeperException.class)
    @ResponseBody
    public Message globalKeeperExceptionHandlerMessage(Exception e){
        System.out.println("["+this.getClass().getSimpleName()+"] is working."); //已经生效
        return Message.fail(Util.ERR_CODE_600, Util.ERR_MSG_600).addContent(GlobalPropoties.EXCEPTION_FLAG,
                new GlobalExceptionMessage(
                        e.getStackTrace()[0].getClassName(),
                        e.getStackTrace()[0].getMethodName(),
                        e.getStackTrace()[0].getLineNumber(),
                        e.getMessage()));
    }

    @ExceptionHandler(NoSuchAlgorithmException.class)
    @ResponseBody
    public Message globalNoSuchAlgorithmExceptionHandler(Exception e) {
    	System.out.println("["+this.getClass().getSimpleName()+"] is working."); //已经生效
        return Message.fail(Util.ERR_CODE_600, Util.ERR_MSG_600).addContent(GlobalPropoties.EXCEPTION_FLAG,
                new GlobalExceptionMessage(
                        e.getStackTrace()[0].getClassName(),
                        e.getStackTrace()[0].getMethodName(),
                        e.getStackTrace()[0].getLineNumber(),
                        e.getMessage()));
    }



}
