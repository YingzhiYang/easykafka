package com.kafka.easykafka.exception;

public class GlobalExceptionMessage {

    private String clazzName;
    private String methodName;
    private int lineNum;
    private String cause;

    public GlobalExceptionMessage(String clazzName, String methodName, int lineNum, String cause) {
        this.clazzName = clazzName;
        this.methodName = methodName;
        this.lineNum = lineNum;
        this.cause = cause;
    }


    @Override
    public String toString() {
        return "GlobalExceptionMessage{" +
                "clazzName='" + clazzName + '\'' +
                ", methodName='" + methodName + '\'' +
                ", lineNum=" + lineNum +
                ", cause='" + cause + '\'' +
                '}';
    }

    public String getClazzName() {
        return clazzName;
    }

    public void setClazzName(String clazzName) {
        this.clazzName = clazzName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public int getLineNum() {
        return lineNum;
    }

    public void setLineNum(int lineNum) {
        this.lineNum = lineNum;
    }


    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }
}
