package com.kafka.easykafka.serviceImpl;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.kafka.easykafka.model.EKTopicAuthorization;
import com.kafka.easykafka.service.AuthorizationDescriptionService;
import com.kafka.easykafka.util.KafkaOperationUtils;

@Service
public class AuthorizationDescriptionServiceImpl implements AuthorizationDescriptionService {

    @Override
    public ArrayList<EKTopicAuthorization> getAllAclDescription() throws ExecutionException, InterruptedException {
        return KafkaOperationUtils.getAllAclDescription();
    }

    @Override
    public ArrayList<EKTopicAuthorization> getSingleAclDescription(String account) throws ExecutionException, InterruptedException {
        return KafkaOperationUtils.getSingleAclDescription(account);
    }

    @Override
    public ArrayList<EKTopicAuthorization> getClickSearchedACL(ArrayList<EKTopicAuthorization> allAuthorizations, String content) {
        ArrayList<EKTopicAuthorization> results = new ArrayList<>();
        Pattern pattern = Pattern.compile(content, Pattern.CASE_INSENSITIVE);
        for (EKTopicAuthorization allAuthorization : allAuthorizations) {
            if (pattern.matcher(allAuthorization.getName()).find() ||
                    pattern.matcher(allAuthorization.getPrincipal()).find() ||
                    pattern.matcher(allAuthorization.getOperation()).find() ||
                    pattern.matcher(allAuthorization.getPatternType()).find() ||
                    pattern.matcher(allAuthorization.getPermission()).find() ||
                    pattern.matcher(allAuthorization.getResourceType()).find()) {

                results.add(allAuthorization);
            }
        }
        return results;
    }
}
