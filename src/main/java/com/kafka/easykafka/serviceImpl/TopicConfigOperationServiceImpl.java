package com.kafka.easykafka.serviceImpl;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import org.springframework.stereotype.Service;

import com.kafka.easykafka.model.TopicConfiguration;
import com.kafka.easykafka.service.TopicConfigOperationService;
import com.kafka.easykafka.util.GlobalPropoties;
import com.kafka.easykafka.util.KafkaOperationUtils;

@Service
public class TopicConfigOperationServiceImpl implements TopicConfigOperationService {
    @Override
    public TopicConfiguration getTopicConfigurations(String topicName) throws ExecutionException, InterruptedException {
        return analyzeTopicConfig(KafkaOperationUtils.getTopicConfiguration(topicName));
    }

    @Override
    public boolean setTopicConfigurations(String topicName,TopicConfiguration topicConfiguration) throws InterruptedException, ExecutionException {
        return KafkaOperationUtils.setTopicConfigurations(topicName, topicConfiguration);
    }

    private TopicConfiguration analyzeTopicConfig(Map<String, String> map) {
        if (Objects.isNull(map))
            return null;
        TopicConfiguration topicConfiguration=new TopicConfiguration();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            switch (entry.getKey()) {
                case GlobalPropoties.COMPRESSION_TYPE:
                    topicConfiguration.setCompressionType(entry.getValue());
                    break;
                case GlobalPropoties.RETENTION_MS:
                    topicConfiguration.setRetentionMs(Long.valueOf(entry.getValue()));
                    break;
                case GlobalPropoties.MAX_MESSAGE_BYTES:
                    topicConfiguration.setMaxMessageBytes(Integer.valueOf(entry.getValue()));
                    break;
                case GlobalPropoties.SEGMENT_INDEX_BYTES:
                    topicConfiguration.setSegmentIndexBytes(Integer.valueOf(entry.getValue()));
                    break;
                case GlobalPropoties.SEGMENT_BYTES:
                    topicConfiguration.setSegmentBytes(Integer.valueOf(entry.getValue()));
                    break;
                case GlobalPropoties.MIN_CLEANABLE_DIRTY_RATIO:
                    topicConfiguration.setMinCleanableDirtyRatio(Double.valueOf(entry.getValue()));
                    break;
                case GlobalPropoties.MIN_INSYNC_REPLICAS:
                    topicConfiguration.setMinInsyncReplicas(Integer.valueOf(entry.getValue()));
                    break;
                case GlobalPropoties.DELETE_RETENTION_MS:
                    topicConfiguration.setDeleteRetentionMs(Long.valueOf(entry.getValue()));
                    break;
                case GlobalPropoties.FLUSH_MESSAGES:
                    topicConfiguration.setFlushMessages(Long.valueOf(entry.getValue()));
                    break;
                case GlobalPropoties.PREALLOCATE:
                    topicConfiguration.setPreallocate(Boolean.valueOf(entry.getValue()));
                    break;
                case GlobalPropoties.RETENTION_BYTES:
                    topicConfiguration.setRetentionBytes(Long.valueOf(entry.getValue()));
                    break;
                case GlobalPropoties.FLUSH_MS:
                    topicConfiguration.setFlushMs(Long.valueOf(entry.getValue()));
                    break;
                case GlobalPropoties.FILE_DELETE_DELAY_MS:
                    topicConfiguration.setFileDeleteDelayMs(Long.valueOf(entry.getValue()));
                    break;
                case GlobalPropoties.SEGMENT_JITTER_MS:
                    topicConfiguration.setSegmentJitterMs(Long.valueOf(entry.getValue()));
                    break;
                case GlobalPropoties.INDEX_INTERVAL_BYTES:
                    topicConfiguration.setIndexIntervalBytes(Integer.valueOf(entry.getValue()));
                    break;
                case GlobalPropoties.SEGMENT_MS:
                    topicConfiguration.setSegmentMs(Long.valueOf(entry.getValue()));
                    break;
                case GlobalPropoties.UNCLEAN_LEADER_ELECTION_ENABLE:
                    topicConfiguration.setUncleanLeaderElectionEnable(Boolean.valueOf(entry.getValue()));
                    break;
                case GlobalPropoties.CLEANUP_POLICY:
                    topicConfiguration.setCleanupPolicy(entry.getValue());
                    break;
            }
        }
        return topicConfiguration;
    }

}
