package com.kafka.easykafka.serviceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSchException;
import com.kafka.easykafka.exception.GlobalExceptionMessage;
import com.kafka.easykafka.model.Account;
import com.kafka.easykafka.model.Message;
import com.kafka.easykafka.service.LinuxFunctionService;
import com.kafka.easykafka.util.GlobalPropoties;
import com.kafka.easykafka.util.Util;

@Deprecated
@Service
public class LinuxFunctionServiceImpl implements LinuxFunctionService {

    @Resource
    LinuxConnectionService service;

    @Override
    public boolean createAccount(Account account) throws JSchException, IOException {
        if(!service.getSession().isConnected()){
            service.connect();
        }
        ChannelExec channelExec = (ChannelExec) service.getSession().openChannel("exec"); //打开执行channel
        channelExec.setCommand(Util.generateAddAccount("/usr/local/kafka_2.12-2.1.0/bin/", account));
        channelExec.setInputStream(null);
        channelExec.setErrStream(System.err);
        channelExec.connect();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(channelExec.getInputStream(), StandardCharsets.UTF_8))) {
            String buf = "";
            while ((buf = reader.readLine()) != null) {
                if (buf.contains("Completed")) {
                    reader.close();
                    channelExec.disconnect();
                    service.closeSession();
                    return true;
                }
            }
        } finally {
            channelExec.disconnect();
            service.closeSession();
        }
        return false;
    }


    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Message LinuxFunctionServiceImplExceptionHandler(Exception e){
        return Message.fail(Util.ERR_CODE_400, Util.ERR_MSG_400).addContent(GlobalPropoties.EXCEPTION_FLAG,
                new GlobalExceptionMessage(
                        e.getStackTrace()[0].getClassName(),
                        e.getStackTrace()[0].getMethodName(),
                        e.getStackTrace()[0].getLineNumber(),
                        e.getMessage()));
    }
}
