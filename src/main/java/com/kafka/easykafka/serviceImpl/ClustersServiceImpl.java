package com.kafka.easykafka.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.annotation.Resource;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.DescribeClusterResult;
import org.apache.kafka.common.Node;
import org.springframework.stereotype.Service;

import com.kafka.easykafka.mapper.ClustersMapper;
import com.kafka.easykafka.model.Cluster;
import com.kafka.easykafka.service.ClustersService;
import com.kafka.easykafka.util.KafkaConnection;
import com.kafka.easykafka.util.Util;

@Service
public class ClustersServiceImpl implements ClustersService {

    @Resource
    private ClustersMapper clustersMapper;
    //private final KafkaConfiguration configuration;
   /* @Autowired
    public ClustersServiceImpl(KafkaConfiguration configuration){
        this.configuration=configuration;
    }*/


    @Override
    public boolean getClusterInstance(String kafkaUrl, String supName, String supPwd, String securityProtocol, String sslLocation, String sslPwd, String saslMechanism) {
        AdminClient adminClient;
        try {
            //System.out.println(GlobalPropoties.getSaslJaasConfigInfo(supName,supPwd));
            //adminClient=configuration.getKafkaConfiguration(kafkaUrl,securityProtocol,sslLocation,sslPwd,saslMechanism,GlobalPropoties.getSaslJaasConfigInfo(supName,supPwd)).getAdminClient();
            /*adminClient=configuration.getKafkaConfiguration("10.122.49.166:9092", "SASL_SSL",
                    "D:/Working/Guidance/Kafka/client_truststore.jks", "WSO2_sp440",
                    "SCRAM-SHA-512",
                    "org.apache.kafka.common.security.scram.ScramLoginModule required username='admin' password='BROKER_admin';").getAdminClient();*/
            adminClient = KafkaConnection.getAdminClient();

            //adminClient=configuration.getAdminClient(kafkaUrl,securityProtocol,sslLocation,sslPwd,saslMechanism,saslJaasConfig);
            return 0 != adminClient.describeCluster().nodes().get().size();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public List<String> findAllClustersFromKafkaServer() {
        try {
            DescribeClusterResult cluster = KafkaConnection.getAdminClient().describeCluster();
            List<String> list = new ArrayList<>();
            for (Node node : cluster.nodes().get()) {
                list.add(node.host());
            }
            return list;
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public int addCluster(Cluster cluster) {
        try {
            System.out.println(cluster.toString());
            return clustersMapper.insertCluster(cluster);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("[" + this.getClass().getName() + "]" + Util.FAIL_MSG_301);
            return Util.SQL_FAIL_FLAG;
        }
    }

    @Override
    public int updateCluster(Cluster cluster) {
        return 0;
    }

    @Override
    public Cluster findCluster(String name) {
        try {
            return clustersMapper.queryCluster(name);
        } catch (Exception e) {
            System.out.println("[" + this.getClass().getName() + "]" + Util.FAIL_MSG_303);
            return null;
        }
    }

    @Override
    public List<Cluster> findAllCluster() {
        return clustersMapper.queryAllCluster();
    }

    @Override
    public List<Map<String, Object>> findAllClusterName() {
        return clustersMapper.queryAllClusterName();
    }

    @Override
    public int removeBroker(String name) {
        System.out.println("remove");
        return 0;
    }




}
