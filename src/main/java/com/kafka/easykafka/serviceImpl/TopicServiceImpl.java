package com.kafka.easykafka.serviceImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.DeleteTopicsResult;
import org.apache.kafka.clients.admin.DescribeAclsResult;
import org.apache.kafka.clients.admin.DescribeTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.admin.TopicDescription;
import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.TopicPartitionInfo;
import org.apache.kafka.common.acl.AccessControlEntryFilter;
import org.apache.kafka.common.acl.AclBinding;
import org.apache.kafka.common.acl.AclBindingFilter;
import org.apache.kafka.common.resource.PatternType;
import org.apache.kafka.common.resource.ResourcePatternFilter;
import org.apache.kafka.common.resource.ResourceType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kafka.easykafka.exception.GlobalExceptionMessage;
import com.kafka.easykafka.model.Cluster;
import com.kafka.easykafka.model.EKPartitionLeader;
import com.kafka.easykafka.model.EKTopicAuthorization;
import com.kafka.easykafka.model.EKTopicDescription;
import com.kafka.easykafka.model.EKTopicPartition;
import com.kafka.easykafka.model.Message;
import com.kafka.easykafka.model.Topic;
import com.kafka.easykafka.service.TopicService;
import com.kafka.easykafka.util.GlobalPropoties;
import com.kafka.easykafka.util.KafkaConnection;
import com.kafka.easykafka.util.Util;

@Service
public class TopicServiceImpl implements TopicService {


    /*@Resource
    private KafkaConfiguration kafkaConfiguration;*/
    //private AdminClient adminClient;

    @Override
    public Set<String> getAllTopics(Cluster cluster) {
        //AdminClient adminClient = kafkaConfiguration.getKafkaConfiguration(cluster.getKfkIps(), cluster.getSecurityProtocol(), cluster.getJksPath(), cluster.getSslPws(), cluster.getSaslMechanism(), GlobalPropoties.getSaslJaasConfigInfo(cluster.getSupUser(), cluster.getSupPwd())).getAdminClient();
        try {
            return KafkaConnection.getAdminClient().listTopics().names().get();
            //页面上首先要列出，list，新建，修改和删除都在这里。
            // 然后点击每一个topic会进入topic详细页面，然后再在里面操作，这里也可以修改topic，还能查看message，
            // 这消息比较难处理，可以先暂停去弄权限的东西。
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public ArrayList<String> getClickSearchedTopic(List<String> list, String name) {
        ArrayList<String> results = new ArrayList<>();
        Pattern pattern = Pattern.compile(name, Pattern.CASE_INSENSITIVE);
        for (String s : list) {
            //Matcher matcher =pattern.matcher(s); matcher.find();
            if (pattern.matcher(s).find()) {
                results.add(s);
            }
        }
        return results;
    }

    @Override
    public Message addATopic(Topic topic) {

        try {

            NewTopic newTopic = new NewTopic(topic.getTopicName(), topic.getTopicPartition(), topic.getTopicReplicationFactor());
            Map<String, String> configMap = new HashMap<>();
            optionalParamCheck(topic, configMap);
            newTopic.configs(configMap);
            Collection<NewTopic> topicsList = new ArrayList<>();
            topicsList.add(newTopic);
            CreateTopicsResult result = KafkaConnection.getAdminClient().createTopics(topicsList);
            result.all().get();
            if (result.all().isDone()) {
                return Message.success();
            }
        } catch (Exception e) {
            if (e.getCause().getMessage().contains("already exists")) {
                return Message.fail(Util.ERR_CODE_601, Util.ERR_MSG_601);
            }
            return Message.fail(Util.ERR_CODE_600, Util.ERR_MSG_HEAD_600 + e.getCause().getMessage());
        }
        return Message.fail(Util.ERR_CODE_600, Util.ERR_MSG_600);
    }

    @Override
    public Message addMultipleTopics(Topic topic, List<String> topicList) {
        ArrayList<NewTopic> topicsCollection = new ArrayList<>();
        ArrayList<String> existsList = new ArrayList<>();
        ArrayList<String> addList = new ArrayList<>();
        try {
            String[] topics = Util.getStringList(topic.getTopicName());
            for (String topicName : topics) {
                if (Util.existsTopics(topicName, topicList)) {
                    existsList.add(topicName);
                } else {
                    NewTopic newTopic = new NewTopic(topicName, topic.getTopicPartition(), topic.getTopicReplicationFactor());
                    Map<String, String> configMap = new HashMap<>();
                    optionalParamCheck(topic, configMap);
                    newTopic.configs(configMap);
                    addList.add(topicName);
                    topicsCollection.add(newTopic);
                }
            }
            if (!existsList.isEmpty()) {
                return Message.fail(Util.ERR_CODE_601, Util.ERR_MSG_601).addContent(Util.ERR_EXD_601, existsList);
            }
            CreateTopicsResult result = KafkaConnection.getAdminClient().createTopics(topicsCollection);
            result.all().get();
            if (result.all().isDone()) {
                return Message.success().addContent(Util.TOPIC_RESULT, addList);
            }
        } catch (Exception e) {
            if (e.getCause().getMessage().contains("already exists")) {
                return Message.fail(Util.ERR_CODE_601, Util.ERR_MSG_601);
            }
            return Message.fail(Util.ERR_CODE_600, Util.ERR_MSG_HEAD_600 + e.getCause().getMessage());
        }
        return Message.fail(Util.ERR_CODE_600, Util.ERR_MSG_600);
    }

    @Override
    public Message deleteATopic(String topicName, List<String> topicFullList) throws ExecutionException, InterruptedException {
        if (!Util.existsTopics(topicName, topicFullList)) {
            return Message.fail(Util.ERR_CODE_602, Util.ERR_MSG_602);
        }
        DeleteTopicsResult result = KafkaConnection.getAdminClient().deleteTopics(Collections.singletonList(topicName));
        result.all().get();
        if (result.all().isDone()) {
            return Message.success();
        }else {
            return Message.fail(Util.ERR_CODE_600, Util.ERR_MSG_600);
        }

    }

    @Override
    public Message deleteMultipleTopic(String deleteTopicNames, List<String> topicFullList) throws ExecutionException, InterruptedException {
        ArrayList<String> failedList = new ArrayList<>();
        DeleteTopicsResult result = KafkaConnection.getAdminClient().deleteTopics(Arrays.asList(Util.getStringList(deleteTopicNames)));
        for (Map.Entry<String, KafkaFuture<Void>> entry : result.values().entrySet()) {
            KafkaFuture<Void> future = entry.getValue();
            future.get();
            if (!future.isDone()) {
                failedList.add(entry.getKey());
            }
        }
        if (failedList.isEmpty()) {
            return Message.success();
        } else {
            return Message.fail(Util.ERR_CODE_604, Util.ERR_MSG_604).addContent(Util.ERR_EXD_604, failedList);
        }
    }


    @Override
    public Message getTopicDescription(String topicName) throws ExecutionException, InterruptedException {
        List<String> list = new ArrayList<String>();
        list.add(topicName);
        DescribeTopicsResult topic = KafkaConnection.getAdminClient().describeTopics(list);

        EKTopicDescription topicDescription = new EKTopicDescription();

        Collection<EKTopicPartition> topicPartitions = new ArrayList<>();
        Map<String, TopicDescription> topicMap = topic.all().get();
        for (Map.Entry<String, TopicDescription> entry : topicMap.entrySet()) {
            topicDescription.setTopicName(entry.getValue().name());
            topicDescription.setPartitionNums(entry.getValue().partitions().size());
            for (TopicPartitionInfo info : entry.getValue().partitions()) {
                EKTopicPartition topicPartition = new EKTopicPartition();
                topicPartition.setPartitionId(info.partition());
                topicPartition.setLeader(new EKPartitionLeader(info.leader().id(), info.leader().host(), info.leader().port(), info.replicas().size()));
                topicPartitions.add(topicPartition);
            }
        }
        return Message.success().addContent(GlobalPropoties.TOPIC_PARTITIONS, topicPartitions);

    }

    @Override
    public Message getTopicAuthorizationDescription(String topicName) throws ExecutionException, InterruptedException {
        ResourcePatternFilter resourcePatternFilter = new ResourcePatternFilter(ResourceType.ANY, topicName, PatternType.ANY);
        AclBindingFilter aclBindingFilter=new AclBindingFilter(resourcePatternFilter, AccessControlEntryFilter.ANY);
        DescribeAclsResult result = KafkaConnection.getAdminClient().describeAcls(aclBindingFilter);
        Collection<EKTopicAuthorization> topicAuthorization=new ArrayList<>();
        Collection<AclBinding> gets = result.values().get();
        for (AclBinding get : gets) {
            EKTopicAuthorization authorization=new EKTopicAuthorization(
                    get.pattern().name(),
                    get.pattern().patternType().toString(),
                    get.pattern().resourceType().toString(),
                    get.entry().principal(),
                    get.entry().permissionType().toString(),
                    get.entry().operation().toString()
            );
            topicAuthorization.add(authorization);
        }
        return Message.success().addContent(GlobalPropoties.TOPIC_AUTHORIZATION,topicAuthorization);
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Message topicServiceImplExceptionHandler(Exception e) {
        return Message.fail(Util.ERR_CODE_600, Util.ERR_MSG_600).addContent(GlobalPropoties.EXCEPTION_FLAG,
                new GlobalExceptionMessage(
                        e.getStackTrace()[0].getClassName(),
                        e.getStackTrace()[0].getMethodName(),
                        e.getStackTrace()[0].getLineNumber(),
                        e.getMessage()));
    }


    /**
     * 检测创建的topic是不是具有下列一个或者多个属性
     *
     * @param topic
     * @param map
     */
    private void optionalParamCheck(Topic topic, Map<String, String> map) {
        if (Objects.nonNull(topic.getRetentionMs())) {
            map.put(GlobalPropoties.RETENTION_MS, topic.getRetentionMs().toString());
        }
        if (Objects.nonNull(topic.getMaxMessageBytes())) {
            map.put(GlobalPropoties.MAX_MESSAGE_BYTES, topic.getMaxMessageBytes().toString());
        }
        if (Objects.nonNull(topic.getSegmentIndexBytes())) {
            map.put(GlobalPropoties.SEGMENT_INDEX_BYTES, topic.getSegmentIndexBytes().toString());
        }
        if (Objects.nonNull(topic.getSegmentBytes())) {
            map.put(GlobalPropoties.SEGMENT_BYTES, topic.getSegmentBytes().toString());
        }
        if (Objects.nonNull(topic.getMinCleanableDirtyRatio())) {
            map.put(GlobalPropoties.MIN_CLEANABLE_DIRTY_RATIO, topic.getMinCleanableDirtyRatio().toString());
        }
        if (Objects.nonNull(topic.getMinInsyncReplicas())) {
            map.put(GlobalPropoties.MIN_INSYNC_REPLICAS, topic.getMinInsyncReplicas().toString());
        }
        if (Objects.nonNull(topic.getDeleteRetentionMs())) {
            map.put(GlobalPropoties.DELETE_RETENTION_MS, topic.getDeleteRetentionMs().toString());
        }
        if (Objects.nonNull(topic.getFlushMessages())) {
            map.put(GlobalPropoties.FLUSH_MESSAGES, topic.getFlushMessages().toString());
        }
        if (Objects.nonNull(topic.getPreallocate())) {
            map.put(GlobalPropoties.PREALLOCATE, topic.getPreallocate().toString());
        }
        if (Objects.nonNull(topic.getRetentionBytes())) {
            map.put(GlobalPropoties.RETENTION_BYTES, topic.getRetentionBytes().toString());
        }
        if (Objects.nonNull(topic.getFlushMs())) {
            map.put(GlobalPropoties.FLUSH_MS, topic.getFlushMs().toString());
        }
        if (Objects.nonNull(topic.getCleanupPolicy()) && !topic.getCompressionType().isEmpty()) {
            map.put(GlobalPropoties.CLEANUP_POLICY, topic.getCleanupPolicy());
        }
        if (Objects.nonNull(topic.getFileDeleteDelayMs())) {
            map.put(GlobalPropoties.FILE_DELETE_DELAY_MS, topic.getFileDeleteDelayMs().toString());
        }
        if (Objects.nonNull(topic.getSegmentJitterMs())) {
            map.put(GlobalPropoties.SEGMENT_JITTER_MS, topic.getSegmentJitterMs().toString());
        }
        if (Objects.nonNull(topic.getIndexIntervalBytes())) {
            map.put(GlobalPropoties.INDEX_INTERVAL_BYTES, topic.getIndexIntervalBytes().toString());
        }
        if (Objects.nonNull(topic.getCompressionType()) && !topic.getCompressionType().isEmpty()) {
            map.put(GlobalPropoties.COMPRESSION_TYPE, topic.getCompressionType());
        } 
        if (Objects.nonNull(topic.getSegmentMs())) {
            map.put(GlobalPropoties.SEGMENT_MS, topic.getSegmentMs().toString());
        }
        if (Objects.nonNull(topic.getUncleanLeaderElectionEnable())) {
            map.put(GlobalPropoties.UNCLEAN_LEADER_ELECTION_ENABLE, topic.getUncleanLeaderElectionEnable().toString());
        }
    }
}
