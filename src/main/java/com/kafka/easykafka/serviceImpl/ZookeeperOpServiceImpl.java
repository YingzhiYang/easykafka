package com.kafka.easykafka.serviceImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kafka.easykafka.mapper.ClustersMapper;
import com.kafka.easykafka.model.ZookeeperInfo;
import com.kafka.easykafka.model.ZookeeperSingleStatus;
import com.kafka.easykafka.service.ZookeeperOpService;
import com.kafka.easykafka.util.Util;
import com.kafka.easykafka.util.ZkServerOperation;

@Service
public class ZookeeperOpServiceImpl implements ZookeeperOpService {

    private ClustersMapper clustersMapper;

    @Autowired
    public ZookeeperOpServiceImpl(ClustersMapper clustersMapper) {
        this.clustersMapper = clustersMapper;
    }

    @Override
    public List<ZookeeperSingleStatus> getZookeeperListInfo(String clusterName) throws IOException {
        return getZkStatus(clustersMapper.queryZookeeperInfoForSpecifiedCluster(clusterName));
    }

    @Override
    public boolean getZookeeperPing(String ip, Integer port) throws IOException {
        return ZkServerOperation.zkServerStatusCheck(ip, port).equals(Util.ONE);
    }


    private List<ZookeeperSingleStatus> getZkStatus(ZookeeperInfo zkInfo) throws IOException {
        List<ZookeeperSingleStatus> list=new ArrayList<>();
        String[] ips=Util.getStringList(zkInfo.getZkIpsNotPort());
        for (String ip : ips) {
            ZookeeperSingleStatus zk=new ZookeeperSingleStatus();
            zk.setIp(ip);
            zk.setPort(zkInfo.getZkPort());
            zk.setClusterName(zkInfo.getClusterName());
            zk.setRole(ZkServerOperation.zkServerRoleCheck(ip,zkInfo.getZkPort()));
            zk.setStatus(ZkServerOperation.zkServerStatusCheck(ip,zkInfo.getZkPort()));
            list.add(zk);
        }
        return list;
    }


}
