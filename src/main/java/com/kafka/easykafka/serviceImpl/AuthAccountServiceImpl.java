package com.kafka.easykafka.serviceImpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.admin.CreateAclsResult;
import org.apache.kafka.clients.admin.DeleteAclsResult;
import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.acl.AclBinding;
import org.apache.kafka.common.acl.AclBindingFilter;
import org.springframework.stereotype.Service;

import com.kafka.easykafka.model.AuthorizationToAccount;
import com.kafka.easykafka.model.Group;
import com.kafka.easykafka.model.Message;
import com.kafka.easykafka.service.AuthAccountService;
import com.kafka.easykafka.util.KafkaOperationUtils;
import com.kafka.easykafka.util.Util;

@Service
public class AuthAccountServiceImpl implements AuthAccountService {

    @Override
    public Message addAuthorizationAccount(AuthorizationToAccount authorizationToAccount) {
        ArrayList<String> errorList = new ArrayList<>();
        CreateAclsResult aclResult = KafkaOperationUtils.addAuthorizationToAccount(authorizationToAccount,Util.getStringList(authorizationToAccount.getTopics()));
        for (Map.Entry<AclBinding, KafkaFuture<Void>> e : aclResult.values().entrySet()) {
            KafkaFuture<Void> future = e.getValue();
            try {
                future.get();
            } catch (Exception exception) {
                errorList.add(e.getKey().pattern().name());
            }
        }
        if (errorList.isEmpty()) {
            return Message.success();
        } else {
            return Message.fail(Util.ERR_CODE_605, Util.ERR_MSG_605).addContent(Util.ERR_EXD_605, errorList);
        }
    }


    @Override
    public Message removeAuthorizationAccount(AuthorizationToAccount authorizationToAccount) {
        ArrayList<String> errorList = new ArrayList<>();
        DeleteAclsResult aclResult = KafkaOperationUtils.removeAuthorizationFromAccount(authorizationToAccount,Util.getStringList(authorizationToAccount.getTopics()));
        for (Map.Entry<AclBindingFilter, KafkaFuture<DeleteAclsResult.FilterResults>> e : aclResult.values().entrySet()) {
            KafkaFuture<DeleteAclsResult.FilterResults> future = e.getValue();
            try {
                future.get();
            } catch (Exception exception) {
                errorList.add(e.getKey().patternFilter().name());
            }
        }
        if (errorList.isEmpty()) {
            return Message.success();
        } else {
            return Message.fail(Util.ERR_CODE_608, Util.ERR_MSG_608).addContent(Util.ERR_EXD_608, errorList);
        }
    }

    @Override
    public Message addGroupId(Group group) throws ExecutionException, InterruptedException {
        KafkaFuture<Void> result = KafkaOperationUtils.addGroupIdToAccount(group).all();
        result.get();
        if (result.isDone()) {
            return Message.success();
        }
        return Message.fail(Util.ERR_CODE_606, Util.ERR_MSG_606);
    }

    @Override
    public Message removeGroupId(Group group) throws ExecutionException, InterruptedException {
        KafkaFuture<Collection<AclBinding>> result = KafkaOperationUtils.removeGroupIdFromAccount(group).all();
        result.get();
        if (result.isDone()) {
            return Message.success();
        }
        return Message.fail(Util.ERR_CODE_607, Util.ERR_MSG_607);
    }
}
    /*private AccessControlEntry chooseAuth(AuthorizationToAccount authorizationToAccount, AccessControlEntry accessControlEntry) {
        switch (AuthorizationEnum.valueOf(authorizationToAccount.getAuthorization())) {
            case All:
                accessControlEntry = new AccessControlEntry("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.ALL, AclPermissionType.ALLOW);
                break;
            case Read:
                accessControlEntry = new AccessControlEntry("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.READ, AclPermissionType.ALLOW);
                break;
            case Write:
                accessControlEntry = new AccessControlEntry("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.WRITE, AclPermissionType.ALLOW);
                break;
            case Create:
                accessControlEntry = new AccessControlEntry("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.CREATE, AclPermissionType.ALLOW);
                break;
            case Delete:
                accessControlEntry = new AccessControlEntry("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.DELETE, AclPermissionType.ALLOW);
                break;
            case Alter:
                accessControlEntry = new AccessControlEntry("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.ALTER, AclPermissionType.ALLOW);
                break;
            case Describe:
                accessControlEntry = new AccessControlEntry("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.DESCRIBE, AclPermissionType.ALLOW);
                break;
            case Clusteraction:
                accessControlEntry = new AccessControlEntry("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.CLUSTER_ACTION, AclPermissionType.ALLOW);
                break;
        }
        return accessControlEntry;
    }

    private AccessControlEntryFilter chooseAuthFilter(AuthorizationToAccount authorizationToAccount, AccessControlEntryFilter accessControlEntryFilter) {
        switch (AuthorizationEnum.valueOf(authorizationToAccount.getAuthorization())) {
            case All:
                accessControlEntryFilter = new AccessControlEntryFilter("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.ALL, AclPermissionType.ALLOW);
                break;
            case Read:
                accessControlEntryFilter = new AccessControlEntryFilter("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.READ, AclPermissionType.ALLOW);
                break;
            case Write:
                accessControlEntryFilter = new AccessControlEntryFilter("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.WRITE, AclPermissionType.ALLOW);
                break;
            case Create:
                accessControlEntryFilter = new AccessControlEntryFilter("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.CREATE, AclPermissionType.ALLOW);
                break;
            case Delete:
                accessControlEntryFilter = new AccessControlEntryFilter("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.DELETE, AclPermissionType.ALLOW);
                break;
            case Alter:
                accessControlEntryFilter = new AccessControlEntryFilter("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.ALTER, AclPermissionType.ALLOW);
                break;
            case Describe:
                accessControlEntryFilter = new AccessControlEntryFilter("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.DESCRIBE, AclPermissionType.ALLOW);
                break;
            case Clusteraction:
                accessControlEntryFilter = new AccessControlEntryFilter("User:" + authorizationToAccount.getAccountName(), "*", AclOperation.CLUSTER_ACTION, AclPermissionType.ALLOW);
                break;
        }
        return accessControlEntryFilter;
    }*/



    /*LinuxConnectionService linuxConnectionService;

    @Autowired
    public void setLinuxConnectionService(LinuxConnectionService linuxConnectionService) {
        this.linuxConnectionService = linuxConnectionService;
    }

    @Override
    public Message addAuthorizationAccount(AuthorizationToAccount authorizationToAccount) {
        ArrayList<String> errorList=new ArrayList<>();
        try {
            String[] topics=Util.getStringList(authorizationToAccount.getTopics());
            for (String topicName : topics) {
                boolean flag=linuxConnectionService.executeAddAuthCommind(GlobalPropoties.KAFKA_BIN_PATH,authorizationToAccount.getAccountName(),topicName,"add",authorizationToAccount.getAuthorization(),GlobalPropoties.ZK_HOST);
                if(!flag){
                    errorList.add(topicName);
                }
            }
            if (errorList.isEmpty()){
                return Message.success();
            }else {
                return Message.fail(Util.ERR_CODE_701,Util.ERR_MSG_701).addContent(Util.ERR_EXD_701,errorList);
            }
        }catch (Exception e){
            if (!errorList.isEmpty()){
                return Message.fail(Util.ERR_CODE_700,Util.ERR_MSG_700).addContent(Util.ERR_EXD_605,errorList);
            }else {
                return Message.fail(Util.ERR_CODE_700,Util.ERR_MSG_700);
            }
        }finally {
            linuxConnectionService.closeSession();
        }
    }

    @Override
    public Message removeAuthorizationAccount(AuthorizationToAccount authorizationToAccount) {
        ArrayList<String> errorList=new ArrayList<>();
        try {
            String[] topics=Util.getStringList(authorizationToAccount.getTopics());
            for (String topicName : topics) {
                boolean flag=linuxConnectionService.executeRemoveAuthCommind(GlobalPropoties.KAFKA_BIN_PATH,authorizationToAccount.getAccountName(),topicName,"remove",authorizationToAccount.getAuthorization(),GlobalPropoties.ZK_HOST);
                if(!flag){
                    errorList.add(topicName);
                }
            }
            if (errorList.isEmpty()){
                return Message.success();
            }else {
                return Message.fail(Util.ERR_CODE_701,Util.ERR_MSG_701).addContent(Util.ERR_EXD_701,errorList);
            }
        }catch (Exception e){
            if (!errorList.isEmpty()){
                return Message.fail(Util.ERR_CODE_700,Util.ERR_MSG_700).addContent(Util.ERR_EXD_701,errorList);
            }else {
                return Message.fail(Util.ERR_CODE_700,Util.ERR_MSG_700);
            }
        }finally {
            linuxConnectionService.closeSession();
        }
    }*/

