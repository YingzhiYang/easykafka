package com.kafka.easykafka.serviceImpl;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.common.security.scram.ScramCredential;
import org.apache.kafka.common.security.scram.internals.ScramCredentialUtils;
import org.apache.kafka.common.security.scram.internals.ScramFormatter;
import org.apache.kafka.common.security.scram.internals.ScramMechanism;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooKeeper;
import org.springframework.stereotype.Service;

import com.kafka.easykafka.model.EKTopicAuthorization;
import com.kafka.easykafka.model.ScramAccount;
import com.kafka.easykafka.service.AccountOperationService;
import com.kafka.easykafka.util.GlobalPropoties;
import com.kafka.easykafka.util.KafkaOperationUtils;
import com.kafka.easykafka.util.Util;
import com.kafka.easykafka.util.ZookeeperConnection;

import kafka.server.ConfigType;
import kafka.zk.AdminZkClient;

@Service
public class AccountOperationServiceImpl implements AccountOperationService {

    @Override
    public boolean createScramAccount(ScramAccount account) throws NoSuchAlgorithmException {
        Properties properties=new Properties();
        ScramMechanism scramMechanism=ScramMechanism.valueOf(account.getSCRAM_MECHANISM());
        ScramCredential credential = new ScramFormatter(scramMechanism).generateCredential(account.getPassword(), scramMechanism.minIterations());
        properties.put(scramMechanism.mechanismName(), ScramCredentialUtils.credentialToString(credential));
        AdminZkClient adminZkClient=ZookeeperConnection.getAdminZkClient(account.getClusterName());
        adminZkClient.changeConfigs(ConfigType.User(),account.getAccount(),properties);
        return adminZkClient.fetchEntityConfig(ConfigType.User(), account.getAccount()).isEmpty();
    }

    @Override
    public List<String> deleteScramAccount(ScramAccount account) throws ExecutionException, InterruptedException, KeeperException {
        List<String> errorList=new ArrayList<>();
        //获取adminZkClient用于清空kafka账户配置
        AdminZkClient adminZkClient=ZookeeperConnection.getAdminZkClient(account.getClusterName());
        if(adminZkClient.fetchEntityConfig(ConfigType.User(),account.getAccount()).isEmpty()){
            return errorList;
        }
        //哪取所有该账户的权限信息
        ArrayList<EKTopicAuthorization> result = KafkaOperationUtils.getSingleAclDescription(account.getAccount());
        //循环删除
        for (EKTopicAuthorization authorization : result) {
            boolean checked=KafkaOperationUtils.removeAllAuthorizationFromAccount(authorization.getPrincipal().split(":")[1],authorization.getName());
            if(!checked){
                //如果发现有错的项目，放到errorlist里面
                errorList.add(authorization.getName());
            }
        }
        //errorList不是空的，返回error list
        if (!errorList.isEmpty()){
            return errorList;
        }
        //清空kafka账户配置
        adminZkClient.changeConfigs(ConfigType.User(),account.getAccount(),new Properties());
        //获取zooKeeper用于删除zk上账户的节点
        ZooKeeper zooKeeper= ZookeeperConnection.getZooKeeper(account.getClusterName());
        //删除节点，注意要把节点写完全
        zooKeeper.delete("/config/users/"+account.getAccount(), -1); //版本设置为-1是，不检测版本号
        //检测节点是否还存在
        if(!adminZkClient.fetchEntityConfig(ConfigType.User(),account.getAccount()).isEmpty()){
            errorList.add(GlobalPropoties.ZK_DEL_REPORT); //如果zk上节点没有删除，写入一个特殊标识
            return errorList;
        }
        //最终的error list应该是null的
        return errorList;
    }


    @Override
    public String generatePassword(Integer nums, Integer lowercase, Integer capital, Integer length) {
        Random random=new Random();
        StringBuilder sb=new StringBuilder();
        if (nums.equals(Util.ONE) & lowercase.equals(Util.ONE) & capital.equals(Util.ONE)){
            for (int i = 0; i < length; i++) {
                int number=random.nextInt(Util.passwordAllPool.length());
                sb.append(Util.passwordAllPool.charAt(number));
            }
        }else if (nums.equals(Util.ONE) & lowercase.equals(Util.ONE)){
            for (int i = 0; i < length; i++) {
                int number=random.nextInt(Util.passwordLowercaseNumPool.length());
                sb.append(Util.passwordLowercaseNumPool.charAt(number));
            }
        }else if (nums.equals(Util.ONE) & capital.equals(Util.ONE)){
            for (int i = 0; i < length; i++) {
                int number=random.nextInt(Util.passwordCapitalNumPool.length());
                sb.append(Util.passwordCapitalNumPool.charAt(number));
            }
        }else if (lowercase.equals(Util.ONE) & capital.equals(Util.ONE)){
            for (int i = 0; i < length; i++) {
                int number=random.nextInt(Util.passwordLowercaseCapitalPool.length());
                sb.append(Util.passwordLowercaseCapitalPool.charAt(number));
            }
        }else if (nums.equals(Util.ONE)){
            for (int i = 0; i < length; i++) {
                int number=random.nextInt(Util.passwordNumPool.length());
                sb.append(Util.passwordNumPool.charAt(number));
            }
        }else if (lowercase.equals(Util.ONE) ){
            for (int i = 0; i < length; i++) {
                int number=random.nextInt(Util.passwordLowercasePool.length());
                sb.append(Util.passwordLowercasePool.charAt(number));
            }
        }else if (capital.equals(Util.ONE)){
            for (int i = 0; i < length; i++) {
                int number=random.nextInt(Util.passwordCapitalPool.length());
                sb.append(Util.passwordCapitalPool.charAt(number));
            }
        }
        return sb.toString();
    }

}
