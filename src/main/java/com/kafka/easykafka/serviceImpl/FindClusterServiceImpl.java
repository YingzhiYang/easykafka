package com.kafka.easykafka.serviceImpl;

import com.kafka.easykafka.mapper.ClustersMapper;
import com.kafka.easykafka.model.ZookeeperInfo;
import com.kafka.easykafka.service.FindClusterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FindClusterServiceImpl implements FindClusterService {

    private ClustersMapper clustersMapper;

    @Autowired
    public FindClusterServiceImpl(ClustersMapper clustersMapper) {
        this.clustersMapper = clustersMapper;
    }

    @Override
    public Integer queryClusterNum() {
        return clustersMapper.queryClusterNum();
    }

    @Override
    public List<ZookeeperInfo> queryZookeeperCheckStatusInfo(){
        return clustersMapper.queryZookeeperCheckStatusInfo();
    }


}
