package com.kafka.easykafka.serviceImpl;

import java.util.concurrent.ExecutionException;

import org.springframework.stereotype.Service;

import com.kafka.easykafka.service.PartitionOperationService;
import com.kafka.easykafka.util.KafkaOperationUtils;

@Service
public class PartitionOperationServiceImpl implements PartitionOperationService {


    @Override
    public Integer getTopicPartitions(String topicName) throws ExecutionException, InterruptedException {
        return KafkaOperationUtils.getTopicPartitions(topicName);
    }

    @Override
    public boolean setTopicPartitions(String topicName, Integer partitions) throws InterruptedException, ExecutionException {
        return KafkaOperationUtils.increasePartitions(topicName, partitions);
    }
}
