package com.kafka.easykafka.serviceImpl;

import java.util.Properties;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.kafka.easykafka.model.LinuxConnect;

@Deprecated
@Service
public class LinuxConnectionService {

    //private ChannelExec channelExec;

    private Session session;
    public Session getSession() {
        return session;
    }

    @PostConstruct
    public void init(){
        this.connect();
    }

    public void connect(){
        JSch jsch = new JSch();
        try {
            session = jsch.getSession(LinuxConnect.getUserName(), LinuxConnect.getHost(), LinuxConnect.getPort());
            session.setPassword(LinuxConnect.getPassword()); // 设置密码
            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config); // 为Session对象设置properties
            session.setTimeout(60000000);// 设置timeout时间
            session.connect(); // 通过Session建立链接
        } catch (JSchException e) {
            e.printStackTrace();
        }
    }

    public void closeSession(){
        if (null != session) {
            session.disconnect();
        }
    }

 /*   public void closeSession(){
        if(null!=channelExec){
            channelExec.disconnect();
        }
        if (null != session) {
            session.disconnect();
        }
    }*/


    /*@Deprecated
    public boolean executeAddAuthCommind(String shallPath, String account, String topic,String operation, String auth,String zkHost){
        //System.out.println(Util.generateAuthCmd(shallPath, account, topic, operation, auth, zkHost));
        try {
            if (!session.isConnected()){
                this.connect();
            }
            channelExec = (ChannelExec) session.openChannel("exec"); //打开执行channel
            channelExec.setCommand(Util.generateAuthCmd(shallPath,account,topic,operation,auth,zkHost));
            channelExec.setInputStream(null);
            channelExec.setErrStream(System.err);
            channelExec.connect();
            BufferedReader reader = new BufferedReader(new InputStreamReader(channelExec.getInputStream(), Charset.forName("UTF-8")));
            String buf = null;
            while ((buf = reader.readLine()) != null) {
                if(buf.contains(Util.generateAuthFlag(account,auth))){
                    reader.close();
                    return true;
                }
            }
            return false;
        } catch (JSchException | IOException e) {
            return false;
        }
    }

    @Deprecated
    public boolean executeRemoveAuthCommind(String shallPath, String account, String topic,String operation, String auth,String zkHost){
        //System.out.println(Util.generateAuthCmdRemove(shallPath, account, topic, operation, auth, zkHost));
        try {
            if (!session.isConnected()){
                this.connect();
            }
            channelExec = (ChannelExec) session.openChannel("exec"); //打开执行channel
            channelExec.setCommand(Util.generateAuthCmdRemove(shallPath,account,topic,operation,auth,zkHost));
            channelExec.setInputStream(null);
            channelExec.setErrStream(System.err);
            channelExec.connect();
            BufferedReader reader = new BufferedReader(new InputStreamReader(channelExec.getInputStream(), Charset.forName("UTF-8")));
            String buf = null;
            while ((buf = reader.readLine()) != null) {
                if(buf.contains(Util.generateAuthFlag(account,auth))){
                    reader.close();
                    return true;
                }
            }
            return false;
        } catch (JSchException | IOException e) {
            return false;
        }
    }*/
}
