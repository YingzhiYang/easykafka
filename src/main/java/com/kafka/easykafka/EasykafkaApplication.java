package com.kafka.easykafka;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages = "com.kafka.easykafka")
@MapperScan("com.kafka.easykafka.mapper") //告诉springboot使用mybatis的时候去哪扫描
public class EasykafkaApplication {
    public static void main(String[] args) {
        SpringApplication.run(EasykafkaApplication.class, args);
    }
}
